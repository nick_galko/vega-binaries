#include "app.h"
#include "windows.h"

#ifdef _DEBUG
#include <crtdbg.h>
#define _CRTDBG_MAP_ALLOC // enable generation of debug heap alloc map
#define new new( _NORMAL_BLOCK, __FILE__, __LINE__) // redefine "new" to get file names in output
#endif

int main(_In_ HINSTANCE hInstance, _In_ HINSTANCE hPrevInstance, _In_ LPSTR lpCmdLine, _In_ int nCmdShow)

{
#ifdef _DEBUG
	_CrtSetReportMode(_CRT_WARN, _CRTDBG_MODE_FILE); // enable file output
	_CrtSetReportFile(_CRT_WARN, _CRTDBG_FILE_STDOUT); // set file to stdout
	_CrtMemState _ms;
	_CrtMemCheckpoint(&_ms); // now forget about objects created before
#endif
	using namespace vega;
	GameApp app;//Engine start
	app.Go();
#ifdef _DEBUG
	_CrtMemDumpAllObjectsSince(&_ms); // dump leaks
#endif
	return 0;
}

int __stdcall WinMain(_In_ HINSTANCE hInstance, _In_ HINSTANCE hPrevInstance, _In_ LPSTR lpCmdLine, _In_ int nCmdShow){
	return main(hInstance, hPrevInstance, lpCmdLine, nCmdShow);
}