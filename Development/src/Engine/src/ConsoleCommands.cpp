#include "EnginePrivate.h"
#include "EngineScriptingEx.h"

namespace vega
{
	/**
	*/
	namespace LowLevelConsoleCommands
	{
		// !@Low Level Pause command
		void Pause(bool _st)
		{
			if (_st)
				GetEngine()->audio->pauseAllSounds();
			else
				GetEngine()->audio->resumeAllSounds();
			
			GetEngine()->SetPause(_st);
		}
	}
	/**
	*/
	namespace ConsoleCommands
	{
		// !@Loading Level command
		void LoadLevel(const MyGUI::UString & _key, const MyGUI::UString & _value) {

			std::string path = _value + ".level";
			if (_key == "loadlevel")
				if (!_value.empty())
					GetEngine()->sceneManager->LoadLevel(path.c_str(), true);

		}
		// !@Loading Level command
		void PlayV(const MyGUI::UString & _key, const MyGUI::UString & _value) {
			GetEngine()->render->PlayVideo(_value);
		}
		// !@Runing script
		void RunScript(const MyGUI::UString & _key, const MyGUI::UString & _value) {
			std::string path = _value + ".lua";
			if (_key == "runscript")
				if (!_value.empty())
					GetEngine()->mScript->RunScript(path.c_str(), true);
		}
		// !@Help command
		void Help(const MyGUI::UString & _key, const MyGUI::UString & _value) {
			GetEngine()->console->GetListCommands();
		}
		// !@EngineShoutdown
		void EngineShoutdown(const MyGUI::UString & _key, const MyGUI::UString & _value) {
			GetEngine()->EngineShoutdown();
		}
		// !@EngineShoutdown
		void CleanConsole(const MyGUI::UString & _key, const MyGUI::UString & _value) {
			GetEngine()->console->CleanConsole();
		}
		// !@R_AA command
		void R_AA(const MyGUI::UString & _key, const MyGUI::UString & _value) {
			int status = 0;
			int currentSt = GetEngine()->mEngineConfig->mAntiAlliasing;
			if (!_value.empty()){
				MyGUI::utility::parseComplex(_value, status);

				switch (status)
				{
				case 0:
					if (currentSt == 1){
						GetEngine()->render->PostEffectSetStatus("SMAA", 0);
						Debug("[AA]Deactivated SMAA-Engine");
					}
					if (currentSt == 2){
						GetEngine()->render->PostEffectSetStatus("SSAA", 0);
						Debug("[AA]Deactivated SSAA-Engine");
					}
					if (currentSt == 3){
						GetEngine()->render->PostEffectSetStatus("FXAA", 0);
						Debug("[AA]Deactivated SSAA-Engine");
					}
					GetEngine()->mEngineConfig->mAntiAlliasing = (EngineConfig::AntiAlliasing)0;
					break;
				case 1:
					Debug("[AA]Activated SMAA-Engine");
					GetEngine()->render->PostEffectSetStatus("SMAA", 1);
					GetEngine()->mEngineConfig->mAntiAlliasing = (EngineConfig::AntiAlliasing)1;
					break;
				case 2:
					Debug("[AA]Activated SSAA-Engine");
					GetEngine()->render->PostEffectSetStatus("SSAA", 1);
					GetEngine()->mEngineConfig->mAntiAlliasing = (EngineConfig::AntiAlliasing)2;
					break;
				case 3:
					Debug("[AA]Activated FXAA-Engine");
					GetEngine()->render->PostEffectSetStatus("FXAA", 1);
					GetEngine()->mEngineConfig->mAntiAlliasing = (EngineConfig::AntiAlliasing)3;
					break;
				}
			}
		}
		// !@R_HDR command
		void R_HDR(const MyGUI::UString & _key, const MyGUI::UString & _value) {
			bool status = false;
			if (!_value.empty()){
				MyGUI::utility::parseComplex(_value, status);
				GetEngine()->render->PostEffectSetStatus("HDR", status);
			}
		}
		// !@R_SSAO command
		void R_SSAO(const MyGUI::UString & _key, const MyGUI::UString & _value) {
			bool status = false;
			if (!_value.empty()){
				MyGUI::utility::parseComplex(_value, status);
				GetEngine()->render->PostEffectSetStatus("SSAO", status);
			}
		}
		// !@E_Pause command
		void E_Pause(const MyGUI::UString & _key, const MyGUI::UString & _value) {
			bool status = false;
			if (!_value.empty()){
				MyGUI::utility::parseComplex(_value, status);
				using namespace LowLevelConsoleCommands;
				Pause(status);
			}
		}
		// !@ReloadCfg command
		void ReloadCfg(const MyGUI::UString & _key, const MyGUI::UString & _value){
			GetEngine()->AttemptConfig();
		}
	}
}