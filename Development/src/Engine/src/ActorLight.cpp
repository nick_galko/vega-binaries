// Last Update:15.01.13
// BugFix:������� ������� ��������� ���� �����,������� ������� ���������� ��������� ����������
#include "EnginePrivate.h"

namespace vega
{
	ActorLight::ActorLight(std::string _mName, _ltype _type, Ogre::Vector3 _pos, Ogre::Vector3 _rot)
	{
		mName = "ActorLight_" + _mName;

		mLight = GetEngine()->mGSceneMgr->createLight(mName);
		mNode = GetEngine()->mGSceneMgr->getRootSceneNode()->createChildSceneNode(mName);

		attach(this);

		//������ ����������� ���������
		setType(_type);
		setPosition(_pos);
		setDirection(_rot.normalisedCopy());
		setCastShadows(true);
	}

	void ActorLight::setShadowFarClipDistance(float _d)	{
		mLight->setShadowFarClipDistance(_d);
	}

	void ActorLight::attach(Actor *_mParent)	{
		_mParent->getNode()->attachObject(mLight);
	}

	void ActorLight::setType(_ltype _type)
	{
		if (mLightType != _type)
		{
			mLightType = (_type);
			mLight->setType(Ogre::Light::LightTypes(_type));
		}
	}

	void ActorLight::setVisible(bool _status)	{
		mLight->setVisible(_status);
	}

	void ActorLight::setCastShadows(bool _status)	{
		mLight->setCastShadows(_status);
	}

	void ActorLight::setPosition(const Ogre::Vector3& _pos)	{
		mLight->setPosition(_pos);
	}

	void ActorLight::setDirection(const Ogre::Vector3& _quat)	{
		mLight->setDirection(_quat);
	}

	void ActorLight::setDiffuse(float _r, float _g, float _b)	{
		mLight->setDiffuseColour(_r, _g, _b);
	}

	void ActorLight::setSpecular(float _r, float _g, float _b)	{
		mLight->setSpecularColour(_r, _g, _b);
	}

	void ActorLight::setScale(const Ogre::Vector3& _scale)	{
		if (mNode)
			mNode->setScale(_scale);
	}

	void ActorLight::setPowerScale(float _range, float _constant, float _linear, float _quadratic)	{
		mLight->setAttenuation(_range, _constant, _linear, _quadratic);
	}

	const Ogre::Vector3& ActorLight::getDirection(){
		return mLight->getDirection();
	}

	const Ogre::ColourValue& ActorLight::getDiffuse(){
		return mLight->getDiffuseColour();
	}

	const Ogre::ColourValue& ActorLight::getSpecular(){
		return mLight->getSpecularColour();
	}

	void ActorLight::setDiffuse(const Ogre::ColourValue& _col){
		return mLight->setDiffuseColour(_col);
	}

	void ActorLight::setSpecular(const Ogre::ColourValue& _col){
		return mLight->setSpecularColour(_col);
	}
}