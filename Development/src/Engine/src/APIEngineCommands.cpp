#include "EnginePrivate.h"
//API
#include "APIEngineCommands.h"

namespace vega
{
	namespace API{
		//-------------------------------------------------------------------------------------
		void RunScript(const char*_a, bool _debug)	{
			GetEngine()->RunScript(_a, _debug);
		}
		//-------------------------------------------------------------------------------------
		void SetPause(bool _a)	{
			GetEngine()->SetPause(_a);
		}
		//-------------------------------------------------------------------------------------
		void RunConsoleCommand(const char*_str) {
			GetEngine()->RunConsoleCommand(_str);
		}
		//-------------------------------------------------------------------------------------
		void PlayVideo(const char*_str, bool _abort) {
			if (!GetEngine()){
				Warning("[API::PlayVideo]Engine is not initiliased");
				return;
			}
			VideoPlayer*vplayer = GetEngine()->render->GetPlayer();
			if (vplayer)
				vplayer->playVideo(_str, _abort);
			else
				Warning("[API::PlayVideo]VideoPlayer is not initiliased");
		}
		//-------------------------------------------------------------------------------------
		bool IsPlayingVideo() {
			if (!GetEngine()){
				Warning("[API::PlayVideo]Engine is not initiliased");
				return false;
			}
			VideoPlayer*vplayer = GetEngine()->render->GetPlayer();
			return vplayer->isPlaying();
		}
		//-------------------------------------------------------------------------------------
		void LoadLevel(const char*_name, bool _new) {
			GetEngine()->sceneManager->LoadLevel(_name, _new);
		}
		//-------------------------------------------------------------------------------------
		void EngineShoutdown() {
			GetEngine()->EngineShoutdown();
		}
		//-------------------------------------------------------------------------------------
		void AttemptConfig() {
			GetEngine()->AttemptConfig();
		}
		//-------------------------------------------------------------------------------------
		void SetMouseLock(bool _status) {
			GetEngine()->input->SetMouseLock(_status);
		}
	}
}