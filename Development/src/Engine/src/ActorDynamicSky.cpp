#include "EnginePrivate.h"
#include "ActorDynamicSky.h"
#include "../SkyX/SkyX.h"

namespace vega
{
	//-------------------------------------------------------------------------------------
	ActorDynamicSky::ActorDynamicSky()
		:mSkyX(nullptr), mBC(nullptr)
	{
		mBC = new BasicController();
		mSkyX = new SkyX(GetEngine()->mGSceneMgr, GetEngine()->mGCamera, GetEngine()->mGRoot, GetEngine()->mGWindow, mBC);
		mSkyX->create();
		AddCloudLayer();

		GetEngine()->sceneManager->_ContainActorDynamicSky(this);
	}
	//-------------------------------------------------------------------------------------
	ActorDynamicSky::~ActorDynamicSky()	{
		SAFE_DELETE(mSkyX);//mBc ������� �� ����,�.� ��� ��������� � mSkyX
	}
	//-------------------------------------------------------------------------------------
	void ActorDynamicSky::Update(float evt){
		mSkyX->update(evt);
	}
	//-------------------------------------------------------------------------------------
	void ActorDynamicSky::AddCloudLayer(){
		mSkyX->getCloudsManager()->add(CloudLayer::Options());
	}
}