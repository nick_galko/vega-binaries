/*=====================================================================================
��������:inputkeys.cpp
����������:����� �������������� ������� ������
������:1.1
�����:������� ������

������� �����������:
1)������� �� ������ ����� ������� �� ~
2)������ ����� ��� ���������,��� 2 ������ ������,������� ������� �������������� ������
======================================================================================*/
#include "EnginePrivate.h"

namespace vega
{
	//Note:������ �� ���� �������� ��������,������ ���������� �� ���/���� �������� ������
	Input::Input()
		:mInputManager(nullptr)
		, mKeyboard(nullptr)
		, mMouse(nullptr)
		, InputMode(InputMODE_GAME)//Note:���������� ����� ��� �������� ������
		, mInitialised(false)
	{
	}

	//-------------------------------------------------------------------------------------
	void Input::frameRenderingQueued()
	{
		if (mInitialised)
		{
			windowResized(GetEngine()->mGWindow);
			//Need to capture/update each device
			mKeyboard->capture();
			mMouse->capture();
		}
	}
	//-------------------------------------------------------------------------------------
	void Input::createFrameListener()
	{
		if (!mInitialised)
		{
			Debug("*** Initializing OIS ***");
			OIS::ParamList pl;
			size_t windowHnd = 0;
			std::ostringstream windowHndStr;

			GetEngine()->mGWindow->getCustomAttribute("WINDOW", &windowHnd);
			windowHndStr << windowHnd;
			pl.insert(std::make_pair(std::string("WINDOW"), windowHndStr.str()));

			mInputManager = OIS::InputManager::createInputSystem(pl);

			mKeyboard = static_cast<OIS::Keyboard*>(mInputManager->createInputObject(OIS::OISKeyboard, true));
			mMouse = static_cast<OIS::Mouse*>(mInputManager->createInputObject(OIS::OISMouse, true));

			mMouse->setEventCallback(this);
			mKeyboard->setEventCallback(this);
			mInitialised = true;
		}
	}//-------------------------------------------------------------------------------------
	void Input::createFrameListener(size_t handle)
	{
		if (!mInitialised)
		{
			Debug("*** Initializing OIS for editor ***");
			mInputManager = OIS::InputManager::createInputSystem(handle);
			mKeyboard = static_cast<OIS::Keyboard*>(mInputManager->createInputObject(OIS::OISKeyboard, false));
			mMouse = static_cast<OIS::Mouse*>(mInputManager->createInputObject(OIS::OISMouse, false));

			mMouse->setEventCallback(this);
			mKeyboard->setEventCallback(this);
			mInitialised = true;
		}
	}
	//-------------------------------------------------------------------------------------
	void Input::windowResized(Ogre::RenderWindow* _window)
	{
		unsigned int width, height, depth;
		int left, top;
		_window->getMetrics(width, height, depth, left, top);

		const OIS::MouseState &ms = mMouse->getMouseState();
		ms.width = width;
		ms.height = height;
	}
	//-------------------------------------------------------------------------------------
	//Unattach OIS before window shutdown (very important under Linux)
	void Input::windowClosed(Ogre::RenderWindow* _window)
	{
		if (mInitialised)
		{
			//Only close for window that created OIS (the main window in these demos)
			if (_window == GetEngine()->mGWindow)
			{
				if (mInputManager)
				{
					mInputManager->destroyInputObject(mMouse);
					mInputManager->destroyInputObject(mKeyboard);

					OIS::InputManager::destroyInputSystem(mInputManager);
					mInputManager = 0;
				}
			}
			mInitialised = false;
		}
	}
	//-------------------------------------------------------------------------------------
	bool Input::keyPressed(const OIS::KeyEvent &_arg)
	{
		if (mInitialised)
		{
			switch (_arg.key) {
			case OIS::KC_SYSRQ:   // take a screenshot
				GetEngine()->mGWindow->writeContentsToTimestampedFile("UserData//screenshot", ".jpg");
				break;

				//Console
			case OIS::KC_GRAVE:
#ifndef __ANDROID__
				bool status = !GetEngine()->console->GetVisible();
				GetEngine()->console->SetVisible(status);

				(status) ? setInputMode(InputMODE_GUIMK) : setInputModeHowPreventInputMode();//���� ������� ������� �������� ������ ���� � �����,����� ������� �����

				setMouseVisible(status);
#endif
				break;
			}

			//��������� �������,��� � � ���� ������������ �� ����������,������ � ����� � ���������
			switch (InputMode){
			case InputMODE_GUIK:
			case InputMODE_GUIMK:
#ifndef __ANDROID__
				MyGUI::InputManager::getInstance().injectKeyPress(MyGUI::KeyCode::Enum(_arg.key), _arg.text);
#endif
				break;
			default:
				GetEngine()->sceneManager->InjectKeyDown(_arg.key);
			}
		}
		return true;
	}

	//-------------------------------------------------------------------------------------
	bool Input::keyReleased(const OIS::KeyEvent &_arg)
	{
		if (mInitialised)
		{
			//��������� �������,��� � � ���� ������������ �� ����������,������ � ����� � ���������
			switch (InputMode){
			case InputMODE_GUIK:
			case InputMODE_GUIMK:
#ifndef __ANDROID__
				MyGUI::InputManager::getInstance().injectKeyRelease(MyGUI::KeyCode::Enum(_arg.key));
#endif
				break;
			default:
				GetEngine()->sceneManager->InjectKeyUp(_arg.key);
			}
		}
		return true;
	}
	//-------------------------------------------------------------------------------------

	bool Input::mousePressed(const OIS::MouseEvent& evt, OIS::MouseButtonID id)
	{
		if (mInitialised)
		{
			switch (InputMode){
			case InputMODE_GUIMK:
			case InputMODE_GUIM:
#ifndef __ANDROID__
				MyGUI::InputManager::getInstance().injectMousePress(evt.state.X.abs, evt.state.Y.abs, MyGUI::MouseButton::Enum(id));
#endif
				break;
			default:
				GetEngine()->sceneManager->InjectMouseDown(id);
			}
		}
		return true;
	}

	//-------------------------------------------------------------------------------------
	bool Input::mouseMoved(const OIS::MouseEvent &_arg)
	{
		if (mInitialised)
		{
			switch (InputMode){
			case InputMODE_GUIMK:
			case InputMODE_GUIM:
#ifndef __ANDROID__
				MyGUI::InputManager::getInstance().injectMouseMove(_arg.state.X.abs, _arg.state.Y.abs, _arg.state.Z.abs);
#endif
				break;
			default:
				GetEngine()->sceneManager->InjectMouseMove(Ogre::Vector2(_arg.state.X.rel, _arg.state.Y.rel));
			}
		}
		return true;
	}
	//-------------------------------------------------------------------------------------
	bool Input::mouseReleased(const OIS::MouseEvent &_arg, OIS::MouseButtonID id)
	{
		if (mInitialised)
		{
			switch (InputMode){
			case InputMODE_GUIMK:
			case InputMODE_GUIK:
			case InputMODE_GUIM:
#ifndef __ANDROID__
				MyGUI::InputManager::getInstance().injectMouseRelease(_arg.state.X.abs, _arg.state.Y.abs, MyGUI::MouseButton::Enum(id));
#endif
				break;
			default:
				GetEngine()->sceneManager->InjectMouseUp(id);
			}
		}
		return true;
	}
	//-------------------------------------------------------------------------------------
	void Input::setInputMode(InputFor mode){
		preventInputMode = InputMode;
		InputMode = mode;
	}
	//-------------------------------------------------------------------------------------
	void Input::setMouseVisible(bool mode){
#ifndef __ANDROID__
		MyGUI::PointerManager::getInstance().setVisible(mode);
#endif
	}
	//-------------------------------------------------------------------------------------
	bool Input::getMouseVisible(){
#ifndef __ANDROID__
		return MyGUI::PointerManager::getInstance().isVisible();
#else
		return 0;
#endif
	}
	//-------------------------------------------------------------------------------------
	void Input::setInputModeHowPreventInputMode() {
		InputFor temp = InputMode;
		if (InputMode != preventInputMode){
			InputMode = preventInputMode;
			preventInputMode = temp;
		}
	}
	//-------------------------------------------------------------------------------------
	void Input::SetMouseLock(bool _status) {
		if (_status)
			mMouse->capture();
		mMouse->setBuffered(_status);
		mMouseLocked = _status;
	}
}