#include "EnginePrivate.h"
#include "ActorSkyBox.h"

namespace vega
{
	/**
	*/
	ActorSkyBox::ActorSkyBox(const char* _material)
	{
		if(!GetEngine()->mGSceneMgr->isSkyBoxEnabled())
			GetEngine()->mGSceneMgr->setSkyBox(true, _material);
	}
	/**
	*/
	ActorSkyBox::~ActorSkyBox()
	{
		if(GetEngine()->mGSceneMgr->isSkyBoxEnabled())
			GetEngine()->mGSceneMgr->destroySceneNode(GetEngine()->mGSceneMgr->getSkyBoxNode());
	}
}