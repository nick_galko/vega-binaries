/*=====================================================================================
��������:EngineBase.cpp
����������:�������� ������� ������
������:1.0
LAST UPDATE:21.06.12
======================================================================================*/
#include "EnginePrivate.h"

//-----------------------------------------------------------------------------
// Depends.
//-----------------------------------------------------------------------------
#include "OgreStaticPluginLoader.h"

namespace vega
{
	ENGINE_API EngineGlobals engine;
	EngineGlobals*GetEngine(){
		return &engine;
	}
	//-------------------------------------------------------------------------------------
	EngineBase::EngineBase(void)
	{}
	//-------------------------------------------------------------------------------------
	EngineBase::~EngineBase(void)
	{
		GetEngine()->EngineShoutdown();
	}
	//-------------------------------------------------------------------------------------
	void EngineBase::Go(void)	{
		GetEngine()->Go();
	}
	//-------------------------------------------------------------------------------------
	void EngineBase::LoadEngineModule(const char *_moduleName)	{
		GetEngine()->plugins->LoadEngineModule((_moduleName));
	}
	//-------------------------------------------------------------------------------------
	void EngineBase::SetGame(iGame* _game)	{
		GetEngine()->SetGame(_game);
	}
}