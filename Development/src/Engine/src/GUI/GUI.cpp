#include "EnginePrivate.h"


namespace vega
{
#ifndef __ANDROID__
	GUI::GUI()
		:mPlatform(nullptr)
		, mGUI(nullptr)
	{
		//GUI package
		Ogre::ResourceGroupManager::getSingleton().addResourceLocation("..\\Engine\\Content\\GUIBase.npk","Zip",Ogre::ResourceGroupManager::AUTODETECT_RESOURCE_GROUP_NAME,true);
	}
	/*
	*/
	void GUI::Initialize()
	{
		mPlatform = new MyGUI::OgrePlatform();
		mPlatform->initialise(GetEngine()->mGWindow, GetEngine()->mGSceneMgr, Ogre::ResourceGroupManager::AUTODETECT_RESOURCE_GROUP_NAME);
		mGUI = new MyGUI::Gui();
		mGUI->initialise("MyGUI_Core.xml");
		//������ ������ ���������
		MyGUI::PointerManager::getInstance().setVisible(false);
	}
	/*
	*/
	bool GUI::frameRenderingQueued(float _evt)	{
		return 1;
	}
#else
	GUI::GUI()
	{}
	/*
	*/
	void GUI::Initialize()
	{}
	/*
	*/
	bool GUI::frameRenderingQueued(float _evt)	{
		return 1;
	}
#endif
}