#include "EnginePrivate.h"
#include "Console.h"
#include "EngineScriptingEx.h"
#include "../../Core/inc/OgreStaticPluginLoader.h"
#include "Raycast.h"
#include "Updater.h"

namespace vega
{
	//-------------------------------------------------------------------------------------
	EngineGlobals::EngineGlobals()
		:window(nullptr)
		, plugins(nullptr)
		, input(nullptr)
		, console(nullptr)
		, render(nullptr)
		, audio(nullptr)
		, gui(nullptr)
		, sceneManager(nullptr)
		, physics(nullptr)
		, mOgrePluginLoader(nullptr)
		, updater(nullptr)
		, mShutDown(false)
	{}
	//-------------------------------------------------------------------------------------
	EngineGlobals::~EngineGlobals()	{
		Debug("EngineGlobals::~EngineGlobals");
		Release();
	}
	//-------------------------------------------------------------------------------------
	void EngineGlobals::PreInitSystems()
	{
		using namespace vega;
		WriteInfoAboutBuild();
		Debug("[PreInit]Updater");
		updater = new Updater();
		Debug("[PreInit]CollisionTools");
		mRaycast = new CollisionTools(this);
		Debug("[PreInit]Plugins");
		plugins = new EnginePlugins();
		Debug("[PreInit]loading Plugins");
		_LoadEngineModules();
		Debug("[PreInit]Window");
		window = new Window();
		Debug("[PreInit]Input");
		input = new Input();
		Debug("[PreInit]Console");
		console = new Console();
		Debug("[PreInit]Render");
		render = new  Render(this);
		Debug("[PreInit]GUI");
		gui = new GUI();
		Debug("[PreInit]LevelManager");
		sceneManager = new SceneManager();
		Debug("[PreInit]Attempt Config");
		AttemptConfig();
	}
	//-------------------------------------------------------------------------------------
	void EngineGlobals::Initialize()
	{
		Debug("[StartupSystems]physics");
		if (physics)
			physics->initialize();
		else
			Warning("[StartupSystems]physics not Initialize");
		Debug("[StartupSystems]render");
		if (render)
			render->Initialize();
		else
			Warning("[StartupSystems]render not Initialize");
		Debug("[StartupSystems]gui");
		if (gui)
			gui->Initialize();
		else
			Warning("[StartupSystems]gui not Initialize");

		if (console)
			console->Initialize();
		else
			Warning("[StartupSystems]console not Initialize");

		if (mScript){
			_RenderAddScriptFunctionaluty(mScript);
			_EngineAddScriptFunctionaluty(mScript);
		}
		else
			Warning("[StartupSystems]scripting not Initialize");

		Debug("[StartupSystems]iGame");
		if (game)
			game->Initialize();
		else
			Debug("[StartupSystems]iGame not Initialize");

		//��������� ���������,��� �� ��� ������ ������
		mEngineState = ES_PLAY;
	}
	//-------------------------------------------------------------------------------------
	void EngineGlobals::SetGame(iGame*_game){
		game = _game;
	}
	//-------------------------------------------------------------------------------------
	void EngineGlobals::Update(float _time)
	{
		if (input)
			input->frameRenderingQueued();
		else
			Debug("[StartupSystems]input not updated");

		if (mEngineState == ES_PLAY)
		{
			if (audio)
				audio->updateSounds();
			else
				Debug("[StartupSystems]audio not updated");

			if (physics)
				physics->update(_time);
			else
				Debug("[StartupSystems]physics not updated");

			if (render)
				render->Update(_time);
			else
				Debug("[StartupSystems]render not updated");

			//Updating Camera
			if (sceneManager)
				sceneManager->Update(_time);
			else
				Debug("[StartupSystems]levelloader not updated");

			if (game)
				game->Update(_time);
			else
				Debug("[StartupSystems]iGame not updated");
		}

		if (gui)
			gui->frameRenderingQueued(_time);
		else
			Debug("[StartupSystems]gui not updated");
	}
	//-------------------------------------------------------------------------------------
	void EngineGlobals::StartupOgre()
	{
		Debug("EngineGlobals::StartupOgre");
		// Init Ogre
		mGRoot = new Ogre::Root(Ogre::StringUtil::BLANK,
			"..\\Engine\\Configs\\BaseRender.ini",
			"..\\Engine\\Logs\\Log.log");
		SetLoggingLevel(3);//Disables ogreLog trash messages
		mOgrePluginLoader = new StaticPluginLoader(mGRoot);
		mOgrePluginLoader->load();
	}
	//-------------------------------------------------------------------------------------
	void EngineGlobals::EngineShoutdown()	{
		Debug("EngineGlobals::EngineShoutdown");
		mShutDown = (!(isLaunched()));
	//	Release();
	}
	//-------------------------------------------------------------------------------------
	void EngineGlobals::Release()	{
		Debug("EngineGlobals::Release");
		SAFE_DELETE(game);
		SAFE_DELETE(sceneManager);
		SAFE_DELETE(gui);
		SAFE_DELETE(audio);
		SAFE_DELETE(physics);
		SAFE_DELETE(render);
		SAFE_DELETE(input);
		SAFE_DELETE(mRaycast);
		SAFE_DELETE(window);
		SAFE_DELETE(plugins);
		SAFE_DELETE(mOgrePluginLoader);
	}
	//-------------------------------------------------------------------------------------
	void EngineGlobals::LoadResources(void){
		Debug("EngineGlobals::LoadResources");
		Ogre::ResourceBackgroundQueue & RBQ = Ogre::ResourceBackgroundQueue::getSingleton();
		RBQ.initialiseAllResourceGroups();
	}
	//-------------------------------------------------------------------------------------
	void EngineGlobals::Go(void)
	{
		if (!Setup())
			return;

		mGRoot->startRendering();
	}
	//-------------------------------------------------------------------------------------
	void EngineGlobals::CreateViewports(void)
	{
		// Create one viewport, entire window
		mGViewport = mGWindow->addViewport(mGCamera);
		mGViewport->setBackgroundColour(Ogre::ColourValue(0, 0, 0));

		// Alter the camera aspect ratio to match the viewport
		mGCamera->setAspectRatio(
			float(mGViewport->getActualWidth()) / float(mGViewport->getActualHeight()));
	}
	//-------------------------------------------------------------------------------------
	void EngineGlobals::PrecacheResources(std::string _mResourcesCfg)
	{
		//������� �������
		std::string path = "..//Engine//Content//";
		// Adding Archives
		Ogre::ResourceGroupManager::getSingleton().addResourceLocation(path, "FileSystem", "Engine", true);
		Ogre::StringVectorPtr gameArchives = Ogre::ResourceGroupManager::getSingleton().findResourceNames("Engine", "*.npk", false);
		for (unsigned int i = 0; i < gameArchives->size(); i++)
			Ogre::ResourceGroupManager::getSingleton().addResourceLocation(path + (*gameArchives)[i], "Zip", "Engine", true);

		if (mEngineConfig->PrecacheResources)
		{
			Debug("[Engine-Resource]-Loading resources from PrecacheResources");


			// ����� � ����������� �������
			path = "..//Game//Content//";
			// Adding Archives
			Ogre::ResourceGroupManager::getSingleton().addResourceLocation(path, "FileSystem", "Game", true);
			gameArchives = Ogre::ResourceGroupManager::getSingleton().findResourceNames("Game", "*.npk", false);
			for (unsigned int i = 0; i < gameArchives->size(); i++)
				Ogre::ResourceGroupManager::getSingleton().addResourceLocation(path + (*gameArchives)[i], "Zip", "Game", true, true);
		}
		TODO("������� �������� ������������� �������");
		//if (!_mResourcesCfg.empty())
		//{
		//	Debug("[Engine-Resource]-Loading resources from config");
		//	// Load resource paths from config file
		//	Ogre::ConfigFile cf;
		//	cf.load(_mResourcesCfg);
		//	// Go through all sections & settings in the file
		//	Ogre::ConfigFile::SectionIterator seci = cf.getSectionIterator();
		//	Ogre::String secName, typeName, archName;
		//	while (seci.hasMoreElements())
		//	{
		//		secName = seci.peekNextKey();
		//		Ogre::ConfigFile::SettingsMultiMap *settings = seci.getNext();
		//		Ogre::ConfigFile::SettingsMultiMap::iterator i;
		//		for (i = settings->begin(); i != settings->end(); ++i)
		//		{
		//			typeName = i->first;
		//			archName = i->second;
		//			Ogre::ResourceGroupManager::getSingleton().addResourceLocation(
		//				archName, typeName, secName, true);
		//		}
		//	}
		//}
		//else
		//	Warning("[Engine]The file:%s a list for resources not exist", _mResourcesCfg);
		Ogre::ResourceGroupManager::getSingleton().addResourceLocation("..//Engine//Scripts//", "FileSystem", "Engine", true);
		Ogre::ResourceGroupManager::getSingleton().addResourceLocation("..//Game//Scripts//", "FileSystem", "Game", true);
		LoadResources();
	}
	//-------------------------------------------------------------------------------------
	void EngineGlobals::SetupResources()
	{
		Debug("[Engine-Resource]-Loading resources from setupResources");

		std::string path = "..//Engine//Shaders//";
		Ogre::ResourceGroupManager::getSingleton().addResourceLocation(path, "FileSystem", "Shaders", true);
		path = "..//Engine//Shaders//programs//Cg//";
		Ogre::ResourceGroupManager::getSingleton().addResourceLocation(path, "FileSystem", "Shaders", true);
		path = "..//Engine//Shaders//programs//GLSL//";
		Ogre::ResourceGroupManager::getSingleton().addResourceLocation(path, "FileSystem", "Shaders", true);
		path = "..//Engine//Shaders//programs//GLSL150//";
		Ogre::ResourceGroupManager::getSingleton().addResourceLocation(path, "FileSystem", "Shaders", true);
		path = "..//Engine//Shaders//programs//GLSL400//";
		Ogre::ResourceGroupManager::getSingleton().addResourceLocation(path, "FileSystem", "Shaders", true);
		path = "..//Engine//Shaders//programs//GLSLES//";
		Ogre::ResourceGroupManager::getSingleton().addResourceLocation(path, "FileSystem", "Shaders", true);
		path = "..//Engine//Shaders//programs//HLSL//";
		Ogre::ResourceGroupManager::getSingleton().addResourceLocation(path, "FileSystem", "Shaders", true);
		path = "..//Engine//Shaders//programs//";
		Ogre::ResourceGroupManager::getSingleton().addResourceLocation(path, "FileSystem", "Shaders", true);
		// Adding Archives
		Ogre::StringVectorPtr gameArchives = Ogre::ResourceGroupManager::getSingleton().findResourceNames("Shaders", "*.shaders", false);
		for (unsigned int i = 0; i < gameArchives->size(); i++)
			Ogre::ResourceGroupManager::getSingleton().addResourceLocation(path + (*gameArchives)[i], "Zip", "Shaders", true);

	}
	//-------------------------------------------------------------------------------------
	bool EngineGlobals::Configure(void)
	{
		const char* platform;
		TODO("Replace this correctly")
#ifdef _WIN64
			platform = "VEGAEngine(W64) (http:\\vegaengine.com)";
#elif _WIN32
			platform = "VEGAEngine(W32) (http:\\vegaengine.com)";
#else
			platform = "VEGAEngine(Unknown) (http:\\vegaengine.com)";
#endif
		if (mGRoot->restoreConfig() || mGRoot->showConfigDialog())
		{
			mGWindow = mGRoot->initialise(true, platform);
			return true;
		}

		return false;
	}
	//-------------------------------------------------------------------------------------
	void EngineGlobals::ChooseSceneManager(void)	{
		mGSceneMgr = mGRoot->createSceneManager("OctreeSceneManager");
	}
	//-------------------------------------------------------------------------------------
	void EngineGlobals::CreateCamera(void)
	{
		mGCamera = mGSceneMgr->createCamera("PlayerCam");
		mGCamera->setPosition(Ogre::Vector3(0, 0, 0));
		mGCamera->lookAt(Ogre::Vector3(0, 0, 0));
		mGCamera->setNearClipDistance(5);
	}
	//-------------------------------------------------------------------------------------
	void EngineGlobals::CreateFrameListener(void)
	{
		window->Listener();
		input->createFrameListener();
		GetEngine()->mGRoot->addFrameListener(updater);
	}
	//-------------------------------------------------------------------------------------
	bool EngineGlobals::Setup(void)
	{
		StartupOgre();
		SetupResources();
		bool carryOn = Configure();
		if (!carryOn) return false;
		ChooseSceneManager();
		CreateCamera();
		CreateViewports();
		Ogre::TextureManager::getSingleton().setDefaultNumMipmaps(5);
		PreInitSystems();
		StartupSystems();
		if (game)
			game->CreateScene();
		CreateFrameListener();
	
		return true;
	}
	//-------------------------------------------------------------------------------------
	void EngineGlobals::StartupSystems()	{
		Initialize();
		PrecacheResources("..\\Engine\\Configs\\resources.ini");
	}
	//-------------------------------------------------------------------------------------
	bool EngineGlobals::Update(const Ogre::FrameEvent& evt)
	{
		if (mGWindow->isClosed())
			return false;

		if (isLaunched())
			return false;

		Update(evt.timeSinceLastFrame);

		return true;
	}
	//-------------------------------------------------------------------------------------
	void EngineGlobals::RunScript(const char*_a, bool _debug)	{
		mScript->RunScript(_a, _debug);
	}
	//-------------------------------------------------------------------------------------
	void EngineGlobals::SetPause(bool _a)
	{
		TODO("���������� ��� � ���� �������");
		if (_a)
			mEngineState = ES_PAUSE;
		else mEngineState = ES_PLAY;
	}
	//-------------------------------------------------------------------------------------
	void EngineGlobals::RunConsoleCommand(const char*_str) {
		TODO("Make this");
	}
	//-------------------------------------------------------------------------------------
	void EngineGlobals::AttemptConfig() {
		mGSceneMgr->setShadowFarDistance(mEngineConfig->ShadowFarDistance);
		mGSceneMgr->setShadowTextureSize(mEngineConfig->ShadowTextureSize);
		mGCamera->setFarClipDistance(mEngineConfig->FarClipDistance);
	}
}