#include "Engineprivate.h"
#include "camera/iCameraBase.h"

namespace vega
{
	/**
	*/
	iCameraBase::iCameraBase()
		:mTopSpeed(150), mVelocity(Ogre::Vector3::ZERO),
		mGoingForward(false), mGoingBack(false), mGoingLeft(false),
		mGoingRight(false), mGoingUp(false), mGoingDown(false),
		mFastMove(false), mOrbiting(false), mZooming(false)
	{
		GetEngine()->sceneManager->AddActorCameraToList(this);
	}
	/**
	*/
	iCameraBase::~iCameraBase(){
		GetEngine()->sceneManager->DeleteCameraFromList(this);
		TODO("Crash");
	}
	/**
	*/
	void iCameraBase::Activate(){
		GetEngine()->sceneManager->SetActiveCamera(this);
	}
}