#include "EnginePrivate.h"
#include "camera\CCSSphericCameraMode.h"

namespace vega{
	bool SphericCameraMode::init()
	{
		CameraMode::init();

		mCameraCS->setFixedYawAxis(true, mFixedAxis);
		mCameraCS->setAutoTrackingTarget(true);

		if (mCameraCS->hasCameraTarget())
		{
			mLastTargetPosition = mCameraCS->getCameraTargetPosition();
			mLastTargetOrientation = mCameraCS->getCameraTargetOrientation();

			reset();
		}
		else
		{
			mLastTargetPosition = Ogre::Vector3::ZERO;
			mLastTargetOrientation = Ogre::Quaternion::IDENTITY;
		}

		mTimeSinceLastChange = 0;

		return true;
	}

	void SphericCameraMode::update(const float &timeSinceLastFrame)
	{
		instantUpdate();

		if (mAutoResetTime > 0)
		{
			if (mCameraCS->getCameraTargetPosition() == mLastTargetPosition
				&& mCameraCS->getCameraTargetOrientation() == mLastTargetOrientation)
			{
				mTimeSinceLastChange += timeSinceLastFrame;
				if (mTimeSinceLastChange >= mAutoResetTime)
				{
					mTimeSinceLastChange = 0;

					reset(false);
				}
			}
			else
			{
				mTimeSinceLastChange = 0;
				mResseting = false;
			}

			mLastTargetPosition = mCameraCS->getCameraTargetPosition();
			mLastTargetOrientation = mCameraCS->getCameraTargetOrientation();
		}

		if (mResseting)
		{
			Ogre::Vector3 cameraTargetPosition = mCameraCS->getCameraTargetPosition();

			Ogre::Vector3 cameraDirection = (mCameraPosition - cameraTargetPosition) - mOffset;

			Ogre::Vector3 derivedRelativePositionToCameraTarget =
				mCameraCS->getCameraTargetOrientation() * mRelativePositionToCameraTarget;

			Ogre::Radian diffRot = derivedRelativePositionToCameraTarget.angleBetween(cameraDirection);
			float diffDist = cameraDirection.length() - mRelativePositionToCameraTarget.length();
			if ((diffRot.valueRadians() > 0 && diffRot < mLastRessetingDiff)
				|| (mResetDistance && diffDist != 0))
			{
				mLastRessetingDiff = diffRot;

				Ogre::Vector3 rotNormal = derivedRelativePositionToCameraTarget.crossProduct(cameraDirection);
				rotNormal.normalise();

				Ogre::Radian deltaAngle = Ogre::Radian(timeSinceLastFrame * mResetRotationFactor * diffRot.valueRadians());
				Ogre::Quaternion q = Ogre::Quaternion(
					deltaAngle
					, -rotNormal);

				Ogre::Vector3 newCameraDirection = q * cameraDirection;

				if (mResetDistance && diffDist != 0)
				{
					Ogre::Vector3 newCameraDirectionNormalised = newCameraDirection.normalisedCopy();
					float distToBeReduced = 0;
					if (diffRot.valueRadians() > 0)
					{
						distToBeReduced = (deltaAngle.valueRadians() / diffRot.valueRadians()) * newCameraDirection.length();
					}

					if (diffDist > 0)
					{
						newCameraDirection = newCameraDirection
							- (newCameraDirectionNormalised * distToBeReduced);

						float newDiffDist = newCameraDirection.length() - mRelativePositionToCameraTarget.length();
						if (newDiffDist < 0)
						{
							newCameraDirection = newCameraDirectionNormalised * mRelativePositionToCameraTarget.length();
						}
					}
					else
					{
						newCameraDirection = newCameraDirection
							+ (newCameraDirectionNormalised * distToBeReduced);

						float newDiffDist = newCameraDirection.length() - mRelativePositionToCameraTarget.length();
						if (newDiffDist > 0)
						{
							newCameraDirection = newCameraDirectionNormalised * mRelativePositionToCameraTarget.length();
						}
					}
				}

				mCameraPosition = (cameraTargetPosition + newCameraDirection) + mOffset;
			}
			else
			{
				mResseting = false;
				mLastRessetingDiff = Ogre::Radian(2.0f * Ogre::Math::PI);
			}
		}
	}

	void SphericCameraMode::instantUpdate()
	{
		Ogre::Vector3 cameraTargetPosition = mCameraCS->getCameraTargetPosition();

		Ogre::Vector3 cameraDirection = (mCameraPosition - cameraTargetPosition) - mOffset;

		float distance = std::min<float>(mOuterSphereRadious, std::max<float>(mInnerSphereRadious, cameraDirection.length()));

		cameraDirection.normalise();

		mCameraPosition = (cameraTargetPosition + (cameraDirection * distance)) + mOffset;
	}

	void SphericCameraMode::reset(bool instant)
	{
		if (instant)
		{
			mCameraPosition = mCameraCS->getCameraTargetPosition()
				+ (mCameraCS->getCameraTargetOrientation() * mRelativePositionToCameraTarget)
				+ mOffset;

			mResseting = false;
		}
		else
		{
			mResseting = true;
		}

		instantUpdate();
	}

	void SphericCameraMode::setOuterSphereRadious(float outerSphereRadious)
	{
		mOuterSphereRadious =
			std::max<float>(mRelativePositionToCameraTarget.length(),
			std::max<float>(outerSphereRadious, mInnerSphereRadious));
	}

	void SphericCameraMode::setInnerSphereRadious(float innerSphereRadious)
	{
		mInnerSphereRadious =
			std::min<float>(mRelativePositionToCameraTarget.length(),
			std::min<float>(innerSphereRadious, mOuterSphereRadious));
	}
}