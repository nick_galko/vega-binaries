#include "EnginePrivate.h"
#include "camera\CCSOrbitalCameraMode.h"

namespace vega{
	bool OrbitalCameraMode::init()
	{
		ChaseFreeYawAxisCameraMode::init();

		mCameraCS->setFixedYawAxis(false);
		mCameraCS->setAutoTrackingTarget(false);

		if (mResetToInitialPosition)
		{
			mRotHorizontal = mInitialRotHorizontal;
			mRotVertical = mInitialRotVertical;
			mZoom = mInitialZoom;
		}

		return true;
	}

	void OrbitalCameraMode::update(const float &timeSinceLastFrame)
	{
		ChaseFreeYawAxisCameraMode::update(timeSinceLastFrame);

		mRotVertical += mRotVerticalDisplacement * timeSinceLastFrame * mRotationFactor;
		mRotHorizontal += mRotHorizontalDisplacement * timeSinceLastFrame * mRotationFactor;
		mZoom += mZoomDisplacement * timeSinceLastFrame * mZoomFactor;

		Ogre::Quaternion offsetVertical(mRotVertical, Ogre::Vector3::UNIT_X);
		Ogre::Quaternion offsetHorizontal(mRotHorizontal, Ogre::Vector3::UNIT_Y);

		mCameraOrientation = mCameraOrientation * offsetHorizontal * offsetVertical;
		mCameraPosition += mCameraOrientation * Ogre::Vector3(0, 0, mZoom);

		mRotVerticalDisplacement = 0;
		mRotHorizontalDisplacement = 0;
		mZoomDisplacement = 0;

		if (mCollisionsEnabled)
			mCameraPosition = collisionDelegate(mCameraCS->getCameraTargetPosition(), mCameraPosition);
	}

	void OrbitalCameraMode::reset()
	{
		mRotHorizontal = mInitialRotHorizontal;
		mRotVertical = mInitialRotVertical;
		mZoom = mInitialZoom;
	}
}