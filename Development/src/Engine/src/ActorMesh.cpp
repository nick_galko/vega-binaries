#include "EnginePrivate.h"
using namespace Ogre;


namespace vega
{

	ActorMesh::ActorMesh(const char *_fileName, const char*_name, const char* material, int _mCollisionModel,
		float _mass, Ogre::Vector3 _mPosition, Ogre::Quaternion _mRotation, Ogre::Vector3 _scale,
		Ogre::Vector3 _mLinearVelocity)
		:pmEntity(nullptr)
	{
		mName = _name;//for Actor Base
		mNode = nullptr;//for Actor Base
		mEntity = nullptr;//for Actor Base
		mFileName = _fileName;
		mPosition = _mPosition;
		mRotation = _mRotation;
		mScale = _scale;
		mMaterialName = material;
		mLinearVelocity = _mLinearVelocity;
		mMass = _mass;
		mCollisionModel = CollisionModel(_mCollisionModel);
		_createNode(mNode, mPosition, mRotation, mScale);
		_createMesh();
		setCastShadows(true);
		setMaterialName(material);
	}
	//-------------------------------------------------------------------------------------
	ActorMesh::~ActorMesh()
	{
		BUG("���� ������ ��������,������ �� �������� ���� ������� ����� � ���������,��������� ��� ��������� �������")
	//	if (!pmEntity->isBackgroundLoaded())
	//		SAFE_DELETE(phBody);
	}

	//-------------------------------------------------------------------------------------
	void ActorMesh::_createMesh()
	{
		TODO("������� �������� ��������")
			// ���������� ���� ����
#if OGRE_THREAD_SUPPORT 2
			_LoadInBackGround();
#else
			_LoadDirectly();
#endif
		// ������� ��������� entity
		mEntity = GetEngine()->mGSceneMgr->createEntity(mName + "_Ent", mFileName);
		mNode->attachObject(mEntity);
		// ������� ������ �������,�� ���� ������ ������ ���� ���������,�.� 
		calculateSizeUsingAxisAlignedBox();
		//������� ������
		_switchCollision();
	}
	//-------------------------------------------------------------------------------------
	void ActorMesh::setMaterialName(const char* material)
	{
		mMaterialName = material;
		if (!(mMaterialName == "AutoDetect"))
			mEntity->setMaterialName(mMaterialName);
	}
	//-------------------------------------------------------------------------------------
	ActorMesh::ActorMesh(const char *_fileName, const char*_name)
		:ActorMesh(_fileName, _name, "DefMat",
		ACTORMESH_COLLISION_STATIC, 10.0f, Ogre::Vector3(0, 0, 0), Ogre::Quaternion(1, 0, 0, 0),
		Ogre::Vector3(1, 1, 1), Ogre::Vector3(0, 0, 0))
	{}
	//-------------------------------------------------------------------------------------
	ActorMesh::ActorMesh(const char *_fileName, const char*_name, const char* _material)
		: ActorMesh(_fileName, _name, _material,
		ACTORMESH_COLLISION_STATIC, 10.0f, Ogre::Vector3(0, 0, 0), Ogre::Quaternion(1, 0, 0, 0),
		Ogre::Vector3(1, 1, 1), Ogre::Vector3(0, 0, 0))
	{}
	//-------------------------------------------------------------------------------------
	void ActorMesh::_LoadInBackGround(){
		//Background loading
		// ���������� ���� ����
		bool exist = Ogre::MeshManager::getSingleton().resourceExists(mFileName);
		if (!exist)
		{
			int ticket = ResourceBackgroundQueue::getSingleton().load(MeshManager::getSingleton().getResourceType(), mFileName, ResourceGroupManager::AUTODETECT_RESOURCE_GROUP_NAME);

			while (!(ResourceBackgroundQueue::getSingleton().isProcessComplete(ticket)))
			{
				OGRE_THREAD_SLEEP(0);
				Root::getSingleton().getWorkQueue()->processResponses();
			}
		}
		pmEntity = MeshManager::getSingleton().getByName(mFileName).getPointer();
		pmEntity->setBackgroundLoaded(true);
	}
	//-------------------------------------------------------------------------------------
	void ActorMesh::_LoadDirectly(){
		pmEntity = Ogre::MeshManager::getSingleton().getByName(mFileName).getPointer();
		if (!pmEntity)
			pmEntity = MeshManager::getSingleton().load(mFileName,
			ResourceGroupManager::AUTODETECT_RESOURCE_GROUP_NAME).getPointer();
	}
}