#include "EnginePrivate.h"



//Modules
#include "../../BulletPhysics/inc/VBulletPhysics.h"
#pragma comment(lib,"BulletPhysics.lib")

#include "../../AudioAL/inc/AudioAL.h"
#pragma comment(lib,"AudioAL.lib")



namespace vega
{
	/**
	*/
	void EngineGlobals::_LoadEngineModules()
	{
#ifdef ENGINE_DLL_LOADING_PLUGINS
		// a config file we'll use to retrieve some settings
		Ogre::ConfigFile cfgFile;
		cfgFile.load("..\\Engine\\Configs\\Modules.ini");

		Ogre::ConfigFile::SectionIterator seci = cfgFile.getSectionIterator();
		while (seci.hasMoreElements())
		{
			std::string secName = seci.peekNextKey();
			Ogre::ConfigFile::SettingsMultiMap *settings = seci.getNext();
			Ogre::ConfigFile::SettingsMultiMap::iterator i;
			for (i = settings->begin(); i != settings->end(); ++i)
			{
				std::string name = i->first;
				std::string libraryName = i->second;//BUG:� ��������� ��� �� ����� �������� � ��������,

				const char* finalStr = libraryName.c_str();
				if (name == "Module")//������ ����������� ��������
					LoadEngineModule(finalStr);

			}
		}
#else
		physics = new VBulletPhysics(this);
		audio = new SoundManager();
#endif
	}




}