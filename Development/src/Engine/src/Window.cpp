#include "EnginePrivate.h"



namespace vega
{
	Window::~Window()
	{
		//Remove ourself as a Window listener
		Ogre::WindowEventUtilities::removeWindowEventListener(GetEngine()->mGWindow, this);
		windowClosed(GetEngine()->mGWindow);
	}

	void Window::Listener()
	{
		//Register as a Window listener
		Ogre::WindowEventUtilities::addWindowEventListener(GetEngine()->mGWindow, this);
	}
}