#include "EnginePrivate.h"
#include "Updater.h"

namespace vega
{
	bool Updater::frameRenderingQueued(const Ogre::FrameEvent& evt){
		mUpdatedTime = evt.timeSinceLastFrame;
		return GetEngine()->Update(evt);
	}
}