#include "EnginePrivate.h"
#include "EmitterParticle.h"


using namespace Ogre;

namespace vega
{
	/**
	*/
	EmitterParticle::EmitterParticle(const char *name,const char *_temp)
		:ps(nullptr)
	{
		ParticleSystem::setDefaultNonVisibleUpdateTimeout(5);  // set nonvisible timeout
		// create some nice fireworks and place it at the origin
		ps = GetEngine()->mGSceneMgr->createParticleSystem(name, _temp);
		attach(nullptr);
	}
	/**
	*/
	void EmitterParticle::attach(Actor *_mParent)
	{
		if(!_mParent)
			GetEngine()->mGSceneMgr->getRootSceneNode()->attachObject(ps);
		else
			_mParent->getNode()->attachObject(ps);
	}
	/**
	*/
	EmitterParticle::~EmitterParticle()
	{

	}
}