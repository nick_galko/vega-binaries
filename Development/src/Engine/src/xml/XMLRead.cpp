#include "EnginePrivate.h"
#include "xml/XMLRead.h"


namespace vega
{
	Ogre::String XMLRead::getAttrib(rapidxml::xml_node<>* XMLNode, const Ogre::String &attrib, const Ogre::String &defaultValue)
	{
		if(XMLNode->first_attribute(attrib.c_str()))
			return XMLNode->first_attribute(attrib.c_str())->value();
		else
			return defaultValue;
	}

	float XMLRead::getAttribReal(rapidxml::xml_node<>* XMLNode, const Ogre::String &attrib, float defaultValue)
	{
		if(XMLNode->first_attribute(attrib.c_str()))
			return Ogre::StringConverter::parseReal(XMLNode->first_attribute(attrib.c_str())->value());
		else
			return defaultValue;
	}

	bool XMLRead::getAttribBool(rapidxml::xml_node<>* XMLNode, const Ogre::String &attrib, bool defaultValue)
	{
		if(!XMLNode->first_attribute(attrib.c_str()))
			return defaultValue;

		if(Ogre::String(XMLNode->first_attribute(attrib.c_str())->value()) == "true")
			return true;

		return false;
	}

	Ogre::Vector3 XMLRead::parseVector3(rapidxml::xml_node<>* XMLNode)
	{
		return Ogre::Vector3(
			Ogre::StringConverter::parseReal(XMLNode->first_attribute("x")->value()),
			Ogre::StringConverter::parseReal(XMLNode->first_attribute("y")->value()),
			Ogre::StringConverter::parseReal(XMLNode->first_attribute("z")->value())
			);
	}

	Ogre::Quaternion XMLRead::parseQuaternion(rapidxml::xml_node<>* XMLNode)
	{
		//! @todo Fix this crap!

		Ogre::Quaternion orientation;

		if(XMLNode->first_attribute("qx"))
		{
			orientation.x = Ogre::StringConverter::parseReal(XMLNode->first_attribute("qx")->value());
			orientation.y = Ogre::StringConverter::parseReal(XMLNode->first_attribute("qy")->value());
			orientation.z = Ogre::StringConverter::parseReal(XMLNode->first_attribute("qz")->value());
			orientation.w = Ogre::StringConverter::parseReal(XMLNode->first_attribute("qw")->value());
		}
		if(XMLNode->first_attribute("qw"))
		{
			orientation.w = Ogre::StringConverter::parseReal(XMLNode->first_attribute("qw")->value());
			orientation.x = Ogre::StringConverter::parseReal(XMLNode->first_attribute("qx")->value());
			orientation.y = Ogre::StringConverter::parseReal(XMLNode->first_attribute("qy")->value());
			orientation.z = Ogre::StringConverter::parseReal(XMLNode->first_attribute("qz")->value());
		}
		else if(XMLNode->first_attribute("axisX"))
		{
			Ogre::Vector3 axis;
			axis.x = Ogre::StringConverter::parseReal(XMLNode->first_attribute("axisX")->value());
			axis.y = Ogre::StringConverter::parseReal(XMLNode->first_attribute("axisY")->value());
			axis.z = Ogre::StringConverter::parseReal(XMLNode->first_attribute("axisZ")->value());
			float angle = Ogre::StringConverter::parseReal(XMLNode->first_attribute("angle")->value());;
			orientation.FromAngleAxis(Ogre::Angle(angle), axis);
		}
		else if(XMLNode->first_attribute("angleX"))
		{
			Ogre::Vector3 axis;
			axis.x = Ogre::StringConverter::parseReal(XMLNode->first_attribute("angleX")->value());
			axis.y = Ogre::StringConverter::parseReal(XMLNode->first_attribute("angleY")->value());
			axis.z = Ogre::StringConverter::parseReal(XMLNode->first_attribute("angleZ")->value());
			//orientation.FromAxes(&axis);
			//orientation.F
		}
		else if(XMLNode->first_attribute("x"))
		{
			orientation.x = Ogre::StringConverter::parseReal(XMLNode->first_attribute("x")->value());
			orientation.y = Ogre::StringConverter::parseReal(XMLNode->first_attribute("y")->value());
			orientation.z = Ogre::StringConverter::parseReal(XMLNode->first_attribute("z")->value());
			orientation.w = Ogre::StringConverter::parseReal(XMLNode->first_attribute("w")->value());
		}
		else if(XMLNode->first_attribute("w"))
		{
			orientation.w = Ogre::StringConverter::parseReal(XMLNode->first_attribute("w")->value());
			orientation.x = Ogre::StringConverter::parseReal(XMLNode->first_attribute("x")->value());
			orientation.y = Ogre::StringConverter::parseReal(XMLNode->first_attribute("y")->value());
			orientation.z = Ogre::StringConverter::parseReal(XMLNode->first_attribute("z")->value());
		}

		return orientation;
	}

	Ogre::ColourValue XMLRead::parseColour(rapidxml::xml_node<>* XMLNode)
	{
		return Ogre::ColourValue(
			Ogre::StringConverter::parseReal(XMLNode->first_attribute("r")->value()),
			Ogre::StringConverter::parseReal(XMLNode->first_attribute("g")->value()),
			Ogre::StringConverter::parseReal(XMLNode->first_attribute("b")->value()),
			XMLNode->first_attribute("a") != NULL ? Ogre::StringConverter::parseReal(XMLNode->first_attribute("a")->value()) : 1
			);
	}
}