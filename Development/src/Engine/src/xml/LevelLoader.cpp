#include "EnginePrivate.h"
#include "..\..\inc\xml\LevelLoader.h"
//API
#include "APIEngineCommands.h"

/*TODO:
CLEAN
�������� ��������� �������� �����--����
*/
namespace vega
{
	/**
	*/

	LevelLoader::LevelLoader() : mSceneMgr(0), mTerrainGroup(0) 	{
		mTerrainGlobalOptions = OGRE_NEW Ogre::TerrainGlobalOptions();
	}


	LevelLoader::~LevelLoader()
	{
		if(mTerrainGroup)
		{
			OGRE_DELETE mTerrainGroup;
		}

		OGRE_DELETE mTerrainGlobalOptions;
	}

	void LevelLoader::parseDotScene(const Ogre::String &SceneName, Ogre::SceneNode *pAttachNode, const Ogre::String &sPrependNode)
	{
		// set up shared object values
		m_sGroupName = Ogre::ResourceGroupManager::AUTODETECT_RESOURCE_GROUP_NAME;
		mSceneMgr = GetEngine()->mGSceneMgr;
		m_sPrependNode = sPrependNode;
		staticObjects.clear();
		dynamicObjects.clear();

		rapidxml::xml_document<> XMLDoc;    // character type defaults to char

		rapidxml::xml_node<>* XMLRoot;



		 VFile file;
		char*scene = (char*)file.LoadFileHowCChar(SceneName.c_str());
		XMLDoc.parse<0>(scene);

		// Grab the scene node
		XMLRoot = XMLDoc.first_node("scene");

		// Validate the File
		if( getAttrib(XMLRoot, "formatVersion", "") == "")
		{
			Ogre::LogManager::getSingleton().logMessage( "[LevelLoader] Error: Invalid .scene File. Missing <scene>" );
			delete scene;
			return;
		}

		// figure out where to attach any nodes we create
		mAttachNode = pAttachNode;
		if(!mAttachNode)
			mAttachNode = mSceneMgr->getRootSceneNode();

		// Process the scene
		processScene(XMLRoot);

		delete scene;
	}

	void LevelLoader::processScene(rapidxml::xml_node<>* XMLRoot)
	{
		rapidxml::xml_node<>* pElement;

		// Process environment (?)
		pElement = XMLRoot->first_node("environment");
		if(pElement)
			processEnvironment(pElement);

		// Process nodes (?)
		pElement = XMLRoot->first_node("nodes");
		if(pElement)
			processNodes(pElement);

		// Process scripts
		pElement = XMLRoot->first_node("scripts");
		if(pElement)
			processScripts(pElement);

		// Process light (?)
		//pElement = XMLRoot->first_node("light");
		//if(pElement)
		//    processLight(pElement);

		// Process camera (?)
		pElement = XMLRoot->first_node("camera");
		if(pElement)
			processCamera(pElement);

		// Process terrain (?)
		pElement = XMLRoot->first_node("terrain");
		if(pElement)
			processTerrain(pElement);
	}

	void LevelLoader::processNodes(rapidxml::xml_node<>* XMLNode)
	{
		rapidxml::xml_node<>* pElement;

		// Process node (*)
		pElement = XMLNode->first_node("node");
		while(pElement)
		{
			processNode(pElement);
			pElement = pElement->next_sibling("node");
		}
		// Process asset 
		pElement = XMLNode->first_node("asset");
		while(pElement)
		{
			AssetParse(pElement);
			pElement = pElement->next_sibling("asset");
		}

		// Process position (?)
		pElement = XMLNode->first_node("position");
		if(pElement)
		{
			mAttachNode->setPosition(parseVector3(pElement));
			mAttachNode->setInitialState();
		}

		// Process rotation (?)
		pElement = XMLNode->first_node("rotation");
		if(pElement)
		{
			mAttachNode->setOrientation(parseQuaternion(pElement));
			mAttachNode->setInitialState();
		}

		// Process scale (?)
		pElement = XMLNode->first_node("scale");
		if(pElement)
		{
			mAttachNode->setScale(parseVector3(pElement));
			mAttachNode->setInitialState();
		}
	}

	void LevelLoader::processScripts(rapidxml::xml_node<>* XMLNode)
	{
		std::string script = getAttrib(XMLNode, "script", "");
		if (!script.empty())
			API::RunScript(script.c_str(), true);
	}

	void LevelLoader::processEnvironment(rapidxml::xml_node<>* XMLNode)
	{
		rapidxml::xml_node<>* pElement;

		// Process camera (?)
		pElement = XMLNode->first_node("camera");
		if(pElement)
			processCamera(pElement);

		// Process fog (?)
		pElement = XMLNode->first_node("fog");
		if(pElement)
			processFog(pElement);

		// Process skyBox (?)
		pElement = XMLNode->first_node("skyBox");
		if(pElement)
			processSkyBox(pElement);

		// Process skyDome (?)
		pElement = XMLNode->first_node("skyDome");
		if(pElement)
			processSkyDome(pElement);

		// Process skyPlane (?)
		pElement = XMLNode->first_node("skyPlane");
		if(pElement)
			processSkyPlane(pElement);

		// Process clipping (?)
		pElement = XMLNode->first_node("clipping");
		if(pElement)
			processClipping(pElement);

		// Process colourAmbient (?)
		pElement = XMLNode->first_node("colourAmbient");
		if(pElement)
			mSceneMgr->setAmbientLight(parseColour(pElement));

		// Process colourBackground (?)
		//! @todo Set the background colour of all viewports (RenderWindow has to be provided then)
		pElement = XMLNode->first_node("colourBackground");
		if(pElement)
			;//mSceneMgr->set(parseColour(pElement));

		}

	void LevelLoader::processTerrain(rapidxml::xml_node<>* XMLNode)
	{
		float worldSize = getAttribReal(XMLNode, "worldSize");
		int mapSize = Ogre::StringConverter::parseInt(XMLNode->first_attribute("mapSize")->value());
//	bool colourmapEnabled = getAttribBool(XMLNode, "colourmapEnabled");
	//	int colourMapTextureSize = Ogre::StringConverter::parseInt(XMLNode->first_attribute("colourMapTextureSize")->value());
		int compositeMapDistance = Ogre::StringConverter::parseInt(XMLNode->first_attribute("tuningCompositeMapDistance")->value());
		int maxPixelError = Ogre::StringConverter::parseInt(XMLNode->first_attribute("tuningMaxPixelError")->value());

		Ogre::Vector3 lightdir(0, -0.3, 0.75);
		lightdir.normalise();
		Ogre::Light* l = mSceneMgr->createLight("tstLight");
		l->setType(Ogre::Light::LT_DIRECTIONAL);
		l->setDirection(lightdir);
		l->setDiffuseColour(Ogre::ColourValue(1.0, 1.0, 1.0));
		l->setSpecularColour(Ogre::ColourValue(0.4, 0.4, 0.4));
		mSceneMgr->setAmbientLight(Ogre::ColourValue(0.6, 0.6, 0.6));

		mTerrainGlobalOptions->setMaxPixelError((float)maxPixelError);
		mTerrainGlobalOptions->setCompositeMapDistance((float)compositeMapDistance);
		mTerrainGlobalOptions->setLightMapDirection(lightdir);
		mTerrainGlobalOptions->setCompositeMapAmbient(mSceneMgr->getAmbientLight());
		mTerrainGlobalOptions->setCompositeMapDiffuse(l->getDiffuseColour());

		mTerrainGroup = OGRE_NEW Ogre::TerrainGroup(mSceneMgr, Ogre::Terrain::ALIGN_X_Z, mapSize, worldSize);
		mTerrainGroup->setOrigin(Ogre::Vector3::ZERO);

		mTerrainGroup->setResourceGroup("General");

		rapidxml::xml_node<>* pElement;
		rapidxml::xml_node<>* pPageElement;

		// Process terrain pages (*)
		pElement = XMLNode->first_node("terrainPages");
		if(pElement)
		{
			pPageElement = pElement->first_node("terrainPage");
			while(pPageElement)
			{
				processTerrainPage(pPageElement);
				pPageElement = pPageElement->next_sibling("terrainPage");
			}
		}
		mTerrainGroup->loadAllTerrains(true);

		mTerrainGroup->freeTemporaryResources();
		//mTerrain->setPosition(mTerrainPosition);
	}

	void LevelLoader::processTerrainPage(rapidxml::xml_node<>* XMLNode)
	{
		Ogre::String name = getAttrib(XMLNode, "name");
		int pageX = Ogre::StringConverter::parseInt(XMLNode->first_attribute("pageX")->value());
		int pageY = Ogre::StringConverter::parseInt(XMLNode->first_attribute("pageY")->value());

		if (Ogre::ResourceGroupManager::getSingleton().resourceExists(mTerrainGroup->getResourceGroup(), name))
		{
			mTerrainGroup->defineTerrain(pageX, pageY, name);
		}
	}


	void LevelLoader::ProcessLight(rapidxml::xml_node<>* XMLNode, Ogre::SceneNode *pParent)
	{
		/*
		// Process attributes
		Ogre::String name = getAttrib(XMLNode, "name");
		Ogre::String id = getAttrib(XMLNode, "id");

		// Create the light
		Ogre::Light *pLight = mSceneMgr->createLight(name);
		if(pParent)
			pParent->attachObject(pLight);

		Ogre::String sValue = getAttrib(XMLNode, "type");
		if(sValue == "point")
			pLight->setType(Ogre::Light::LT_POINT);
		else if(sValue == "directional")
			pLight->setType(Ogre::Light::LT_DIRECTIONAL);
		else if(sValue == "spot")
			pLight->setType(Ogre::Light::LT_SPOTLIGHT);
		else if(sValue == "radPoint")
			pLight->setType(Ogre::Light::LT_POINT);

		pLight->setVisible(getAttribBool(XMLNode, "visible", true));
		pLight->setCastShadows(getAttribBool(XMLNode, "castShadows", true));

		rapidxml::xml_node<>* pElement;

		// Process position (?)
		pElement = XMLNode->first_node("position");
		if(pElement)
			pLight->setPosition(parseVector3(pElement));

		// Process normal (?)
		pElement = XMLNode->first_node("normal");
		if(pElement)
			pLight->setDirection(parseVector3(pElement));

		pElement = XMLNode->first_node("directionVector");
		if(pElement)
		{
			pLight->setDirection(parseVector3(pElement));
			mLightDirection = parseVector3(pElement);
		}

		// Process colourDiffuse (?)
		pElement = XMLNode->first_node("colourDiffuse");
		if(pElement)
			pLight->setDiffuseColour(parseColour(pElement));

		// Process colourSpecular (?)
		pElement = XMLNode->first_node("colourSpecular");
		if(pElement)
			pLight->setSpecularColour(parseColour(pElement));

		if(sValue != "directional")
		{
			// Process lightRange (?)
			pElement = XMLNode->first_node("lightRange");
			if(pElement)
				processLightRange(pElement, pLight);

			// Process lightAttenuation (?)
			pElement = XMLNode->first_node("lightAttenuation");
			if(pElement)
				processLightAttenuation(pElement, pLight);
		}
		// Process userDataReference (?)
		pElement = XMLNode->first_node("userDataReference");
		if(pElement)
			;//processUserDataReference(pElement, pLight);*/

	
	}

	void LevelLoader::processCamera(rapidxml::xml_node<>* XMLNode, Ogre::SceneNode *pParent)
	{
		// Process attributes
		Ogre::String name = getAttrib(XMLNode, "name");
		Ogre::String id = getAttrib(XMLNode, "id");
//	float fov = getAttribReal(XMLNode, "fov", 45);
//	float aspectRatio = getAttribReal(XMLNode, "aspectRatio", 1.3333);
		Ogre::String projectionType = getAttrib(XMLNode, "projectionType", "perspective");

		// Create the camera
		Ogre::Camera *pCamera = mSceneMgr->createCamera(name);

		//TODO: make a flag or attribute indicating whether or not the camera should be attached to any parent node.
		//if(pParent)
		//    pParent->attachObject(pCamera);

		// Set the field-of-view
		//! @todo Is this always in degrees?
		//pCamera->setFOVy(Ogre::Degree(fov));

		// Set the aspect ratio
		//pCamera->setAspectRatio(aspectRatio);

		// Set the projection type
		if(projectionType == "perspective")
			pCamera->setProjectionType(Ogre::PT_PERSPECTIVE);
		else if(projectionType == "orthographic")
			pCamera->setProjectionType(Ogre::PT_ORTHOGRAPHIC);

		rapidxml::xml_node<>* pElement;

		// Process clipping (?)
		pElement = XMLNode->first_node("clipping");
		if(pElement)
		{
			float nearDist = getAttribReal(pElement, "near");
			pCamera->setNearClipDistance(nearDist);

			float farDist =  getAttribReal(pElement, "far");
			pCamera->setFarClipDistance(farDist);
		}

		// Process position (?)
		pElement = XMLNode->first_node("position");
		if(pElement)
			pCamera->setPosition(parseVector3(pElement));

		// Process rotation (?)
		pElement = XMLNode->first_node("rotation");
		if(pElement)
			pCamera->setOrientation(parseQuaternion(pElement));

		// Process normal (?)
		pElement = XMLNode->first_node("normal");
		if(pElement)
			;//!< @todo What to do with this element?

		// Process lookTarget (?)
		pElement = XMLNode->first_node("lookTarget");
		if(pElement)
			;//!< @todo Implement the camera look target

		// Process trackTarget (?)
		pElement = XMLNode->first_node("trackTarget");
		if(pElement)
			;//!< @todo Implement the camera track target

		// Process userDataReference (?)
		pElement = XMLNode->first_node("userDataReference");
		if(pElement)
			;//!< @todo Implement the camera user data reference

		// construct a scenenode is no parent
		if(!pParent)
		{
			Ogre::SceneNode* pNode = mAttachNode->createChildSceneNode(name);
			pNode->setPosition(pCamera->getPosition());
			pNode->setOrientation(pCamera->getOrientation());
			pNode->scale(1,1,1);
		}
	}

	void LevelLoader::processNode(rapidxml::xml_node<>* XMLNode, Ogre::SceneNode *pParent)
	{
		// Construct the node's name
		Ogre::String name = m_sPrependNode + getAttrib(XMLNode, "name");

		// Create the scene node
		Ogre::SceneNode *pNode;
		if(name.empty())
		{
			// Let Ogre choose the name
			if(pParent)
				pNode = pParent->createChildSceneNode();
			else
				pNode = mAttachNode->createChildSceneNode();
		}
		else
		{
			// Provide the name
			if(pParent)
				pNode = pParent->createChildSceneNode(name);
			else
				pNode = mAttachNode->createChildSceneNode(name);
		}

		// Process other attributes
		Ogre::String id = getAttrib(XMLNode, "id");
//	bool isTarget = getAttribBool(XMLNode, "isTarget");

		rapidxml::xml_node<>* pElement;

		// Process position (?)
		pElement = XMLNode->first_node("position");
		if(pElement)
		{
			pNode->setPosition(parseVector3(pElement));
			pNode->setInitialState();
		}

		// Process rotation (?)
		pElement = XMLNode->first_node("rotation");
		if(pElement)
		{
			pNode->setOrientation(parseQuaternion(pElement));
			pNode->setInitialState();
		}

		// Process scale (?)
		pElement = XMLNode->first_node("scale");
		if(pElement)
		{
			pNode->setScale(parseVector3(pElement));
			pNode->setInitialState();
		}

		// Process lookTarget (?)
		pElement = XMLNode->first_node("lookTarget");
		if(pElement)
			processLookTarget(pElement, pNode);

		// Process trackTarget (?)
		pElement = XMLNode->first_node("trackTarget");
		if(pElement)
			processTrackTarget(pElement, pNode);

		// Process node (*)
		pElement = XMLNode->first_node("node");
		while(pElement)
		{
			processNode(pElement, pNode);
			pElement = pElement->next_sibling("node");
		}

		// Process entity (*)
		pElement = XMLNode->first_node("entity");
		while(pElement)
		{
			processEntity(pElement, pNode);
			pElement = pElement->next_sibling("entity");
		}

		

		// Process camera (*)
		pElement = XMLNode->first_node("camera");
		while(pElement)
		{
			processCamera(pElement, pNode);
			pElement = pElement->next_sibling("camera");
		}

		// Process particleSystem (*)
		pElement = XMLNode->first_node("particleSystem");
		while(pElement)
		{
			processParticleSystem(pElement, pNode);
			pElement = pElement->next_sibling("particleSystem");
		}

		// Process billboardSet (*)
		pElement = XMLNode->first_node("billboardSet");
		while(pElement)
		{
			processBillboardSet(pElement, pNode);
			pElement = pElement->next_sibling("billboardSet");
		}

		// Process plane (*)
		pElement = XMLNode->first_node("plane");
		while(pElement)
		{
			processPlane(pElement, pNode);
			pElement = pElement->next_sibling("plane");
		}
	}





	LevelLoader::XMLData::XMLData()
	{}


	void LevelLoader::AssetParse(rapidxml::xml_node<>* XMLNode)
	{
		Ogre::Vector3 mPosition=Ogre::Vector3::ZERO;
		Ogre::Vector3 mLinearVelocity=Ogre::Vector3::ZERO;
		Ogre::Vector3 mScale=Ogre::Vector3::ZERO;
		Ogre::Quaternion mRotation=Ogre::Quaternion::ZERO;		
		bool castShadows = getAttribBool(XMLNode, "castShadows", true);
		Ogre::String meshFile="";
		Ogre::String materialFile="";
		float mCollisionModel=0.0f;
		float mMass=10.0f;


		// Construct the node's name
		Ogre::String name = m_sPrependNode + getAttrib(XMLNode, "name");

		if(name.empty())		
			Error("NameIs Empty");
		

		// Process other attributes
		Ogre::String id = getAttrib(XMLNode, "id");

		rapidxml::xml_node<>* pElement;

		// Process position 
		pElement = XMLNode->first_node("position");
		if(pElement)
			mPosition=parseVector3(pElement);
		// Process linearvelocity 
		pElement = XMLNode->first_node("linearvelocity");
		if(pElement)
			mLinearVelocity=parseVector3(pElement);


		// Process rotation
		pElement = XMLNode->first_node("rotation");
		if(pElement)
			mRotation=parseQuaternion(pElement);

		// Process scale
		pElement = XMLNode->first_node("scale");
		if(pElement)
			mScale=parseVector3(pElement);

		pElement = XMLNode->first_node("ActorMesh");
		if(pElement)
		{
			// Process attributes			
			meshFile = getAttrib(pElement, "meshFile","error.mesh");
			materialFile = getAttrib(pElement, "material");
			mCollisionModel=getAttribReal(pElement, "collision",0);
			mMass=getAttribReal(pElement, "mass",10);
			ActorMesh *mesh=new ActorMesh(meshFile.c_str(),name.c_str(),materialFile.c_str(),mCollisionModel,mMass,mPosition,mRotation,mScale,mLinearVelocity);
			if(!materialFile.empty())
				mesh->setMaterialName(materialFile.c_str());
			if(castShadows)
				mesh->setCastShadows(castShadows);
		}

		// Process light (*)
		
		Ogre::Vector3 mRotationVec=Ogre::Vector3(0.0f,-1.0f,0.0f);		
		pElement = XMLNode->first_node("ActorLight");
		if(pElement)
		{
		int mType=getAttribReal(pElement, "type",0);
		
		// Process rotation
		pElement = XMLNode->first_node("rotationVec");
		if(pElement)
			mRotationVec=parseVector3(pElement);

	

			
			ActorLight *light=new ActorLight(name.c_str(),(ActorLight::_ltype)mType,mPosition,mRotationVec);
			if(mScale!=Ogre::Vector3::ZERO)
				light->setScale(mScale);
		}
		
	}






	void LevelLoader::processLookTarget(rapidxml::xml_node<>* XMLNode, Ogre::SceneNode *pParent)
	{
		//! @todo Is this correct? Cause I don't have a clue actually

		// Process attributes
		Ogre::String nodeName = getAttrib(XMLNode, "nodeName");

		Ogre::Node::TransformSpace relativeTo = Ogre::Node::TS_PARENT;
		Ogre::String sValue = getAttrib(XMLNode, "relativeTo");
		if(sValue == "local")
			relativeTo = Ogre::Node::TS_LOCAL;
		else if(sValue == "parent")
			relativeTo = Ogre::Node::TS_PARENT;
		else if(sValue == "world")
			relativeTo = Ogre::Node::TS_WORLD;

		rapidxml::xml_node<>* pElement;

		// Process position (?)
		Ogre::Vector3 position;
		pElement = XMLNode->first_node("position");
		if(pElement)
			position = parseVector3(pElement);

		// Process localDirection (?)
		Ogre::Vector3 localDirection = Ogre::Vector3::NEGATIVE_UNIT_Z;
		pElement = XMLNode->first_node("localDirection");
		if(pElement)
			localDirection = parseVector3(pElement);

		// Setup the look target
		try
		{
			if(!nodeName.empty())
			{
				Ogre::SceneNode *pLookNode = mSceneMgr->getSceneNode(nodeName);
				position = pLookNode->_getDerivedPosition();
			}

			pParent->lookAt(position, relativeTo, localDirection);
		}
		catch(Ogre::Exception &/*e*/)
		{
			Ogre::LogManager::getSingleton().logMessage("[LevelLoader] Error processing a look target!");
		}
	}

	void LevelLoader::processTrackTarget(rapidxml::xml_node<>* XMLNode, Ogre::SceneNode *pParent)
	{
		// Process attributes
		Ogre::String nodeName = getAttrib(XMLNode, "nodeName");

		rapidxml::xml_node<>* pElement;

		// Process localDirection (?)
		Ogre::Vector3 localDirection = Ogre::Vector3::NEGATIVE_UNIT_Z;
		pElement = XMLNode->first_node("localDirection");
		if(pElement)
			localDirection = parseVector3(pElement);

		// Process offset (?)
		Ogre::Vector3 offset = Ogre::Vector3::ZERO;
		pElement = XMLNode->first_node("offset");
		if(pElement)
			offset = parseVector3(pElement);

		// Setup the track target
		try
		{
			Ogre::SceneNode *pTrackNode = mSceneMgr->getSceneNode(nodeName);
			pParent->setAutoTracking(true, pTrackNode, localDirection, offset);
		}
		catch(Ogre::Exception &/*e*/)
		{
			Ogre::LogManager::getSingleton().logMessage("[LevelLoader] Error processing a track target!");
		}
	}

	void LevelLoader::processEntity(rapidxml::xml_node<>* XMLNode, Ogre::SceneNode *pParent)
	{
		// Process attributes
		Ogre::String name = getAttrib(XMLNode, "name");
		Ogre::String id = getAttrib(XMLNode, "id");
		Ogre::String meshFile = getAttrib(XMLNode, "meshFile");
		Ogre::String materialFile = getAttrib(XMLNode, "materialFile");
		bool isStatic = getAttribBool(XMLNode, "static", false);;
		bool castShadows = getAttribBool(XMLNode, "castShadows", true);

		// TEMP: Maintain a list of static and dynamic objects
		if(isStatic)
			staticObjects.push_back(name);
		else
			dynamicObjects.push_back(name);

		rapidxml::xml_node<>* pElement;

		// Process vertexBuffer (?)
		pElement = XMLNode->first_node("vertexBuffer");
		if(pElement)
			;//processVertexBuffer(pElement);

		// Process indexBuffer (?)
		pElement = XMLNode->first_node("indexBuffer");
		if(pElement)
			;//processIndexBuffer(pElement);

		// Create the entity
		Ogre::Entity *pEntity = 0;
		try
		{
			Ogre::MeshManager::getSingleton().load(meshFile, m_sGroupName);
			pEntity = mSceneMgr->createEntity(name, meshFile);
			pEntity->setCastShadows(castShadows);
			pParent->attachObject(pEntity);

			if(!materialFile.empty())
				pEntity->setMaterialName(materialFile);
		}
		catch(Ogre::Exception &/*e*/)
		{
			Ogre::LogManager::getSingleton().logMessage("[LevelLoader] Error loading an entity!");
		}
	}

	void LevelLoader::processParticleSystem(rapidxml::xml_node<>* XMLNode, Ogre::SceneNode *pParent)
	{
		// Process attributes
		Ogre::String name = getAttrib(XMLNode, "name");
		Ogre::String id = getAttrib(XMLNode, "id");
		Ogre::String file = getAttrib(XMLNode, "file");

		// Create the particle system
		try
		{
			Ogre::ParticleSystem *pParticles = mSceneMgr->createParticleSystem(name, file);
			pParent->attachObject(pParticles);
		}
		catch(Ogre::Exception &/*e*/)
		{
			Ogre::LogManager::getSingleton().logMessage("[LevelLoader] Error creating a particle system!");
		}
	}

	void LevelLoader::processBillboardSet(rapidxml::xml_node<>* XMLNode, Ogre::SceneNode *pParent)
	{
		//! @todo Implement this
	}

	void LevelLoader::processPlane(rapidxml::xml_node<>* XMLNode, Ogre::SceneNode *pParent)
	{
		Ogre::String name = getAttrib(XMLNode, "name");
		float distance = getAttribReal(XMLNode, "distance");
		float width = getAttribReal(XMLNode, "width");
		float height = getAttribReal(XMLNode, "height");
		int xSegments = Ogre::StringConverter::parseInt(getAttrib(XMLNode, "xSegments"));
		int ySegments = Ogre::StringConverter::parseInt(getAttrib(XMLNode, "ySegments"));
		int numTexCoordSets = Ogre::StringConverter::parseInt(getAttrib(XMLNode, "numTexCoordSets"));
		float uTile = getAttribReal(XMLNode, "uTile");
		float vTile = getAttribReal(XMLNode, "vTile");
		Ogre::String material = getAttrib(XMLNode, "material");
		bool hasNormals = getAttribBool(XMLNode, "hasNormals");
		Ogre::Vector3 normal = parseVector3(XMLNode->first_node("normal"));
		Ogre::Vector3 up = parseVector3(XMLNode->first_node("upVector"));

		Ogre::Plane plane(normal, distance);
		Ogre::MeshPtr res = Ogre::MeshManager::getSingletonPtr()->createPlane(
			name + "mesh", "General", plane, width, height, xSegments, ySegments, hasNormals,
			numTexCoordSets, uTile, vTile, up);
		Ogre::Entity* ent = mSceneMgr->createEntity(name, name + "mesh");

		ent->setMaterialName(material);

		pParent->attachObject(ent);
	}

	void LevelLoader::processFog(rapidxml::xml_node<>* XMLNode)
	{
		// Process attributes
		float expDensity = getAttribReal(XMLNode, "density", 0.001);
		float linearStart = getAttribReal(XMLNode, "start", 0.0);
		float linearEnd = getAttribReal(XMLNode, "end", 1.0);

		Ogre::FogMode mode = Ogre::FOG_NONE;
		Ogre::String sMode = getAttrib(XMLNode, "mode");
		if(sMode == "none")
			mode = Ogre::FOG_NONE;
		else if(sMode == "exp")
			mode = Ogre::FOG_EXP;
		else if(sMode == "exp2")
			mode = Ogre::FOG_EXP2;
		else if(sMode == "linear")
			mode = Ogre::FOG_LINEAR;
		else
			mode = (Ogre::FogMode)Ogre::StringConverter::parseInt(sMode);

		rapidxml::xml_node<>* pElement;

		// Process colourDiffuse (?)
		Ogre::ColourValue colourDiffuse = Ogre::ColourValue::White;
		pElement = XMLNode->first_node("colour");
		if(pElement)
			colourDiffuse = parseColour(pElement);

		// Setup the fog
		mSceneMgr->setFog(mode, colourDiffuse, expDensity, linearStart, linearEnd);
	}

	void LevelLoader::processSkyBox(rapidxml::xml_node<>* XMLNode)
	{
		// Process attributes
		Ogre::String material = getAttrib(XMLNode, "material", "BaseWhite");
		float distance = getAttribReal(XMLNode, "distance", 5000);
		bool drawFirst = getAttribBool(XMLNode, "drawFirst", true);
		bool active = getAttribBool(XMLNode, "active", false);
		if(!active)
			return;

		rapidxml::xml_node<>* pElement;

		// Process rotation (?)
		Ogre::Quaternion rotation = Ogre::Quaternion::IDENTITY;
		pElement = XMLNode->first_node("rotation");
		if(pElement)
			rotation = parseQuaternion(pElement);

		// Setup the sky box
		mSceneMgr->setSkyBox(true, material, distance, drawFirst, rotation, m_sGroupName);
	}

	void LevelLoader::processSkyDome(rapidxml::xml_node<>* XMLNode)
	{
		// Process attributes
		Ogre::String material = XMLNode->first_attribute("material")->value();
		float curvature = getAttribReal(XMLNode, "curvature", 10);
		float tiling = getAttribReal(XMLNode, "tiling", 8);
		float distance = getAttribReal(XMLNode, "distance", 4000);
		bool drawFirst = getAttribBool(XMLNode, "drawFirst", true);
		bool active = getAttribBool(XMLNode, "active", false);
		if(!active)
			return;

		rapidxml::xml_node<>* pElement;

		// Process rotation (?)
		Ogre::Quaternion rotation = Ogre::Quaternion::IDENTITY;
		pElement = XMLNode->first_node("rotation");
		if(pElement)
			rotation = parseQuaternion(pElement);

		// Setup the sky dome
		mSceneMgr->setSkyDome(true, material, curvature, tiling, distance, drawFirst, rotation, 16, 16, -1, m_sGroupName);
	}

	void LevelLoader::processSkyPlane(rapidxml::xml_node<>* XMLNode)
	{
		// Process attributes
		Ogre::String material = getAttrib(XMLNode, "material");
		float planeX = getAttribReal(XMLNode, "planeX", 0);
		float planeY = getAttribReal(XMLNode, "planeY", -1);
		float planeZ = getAttribReal(XMLNode, "planeX", 0);
		float planeD = getAttribReal(XMLNode, "planeD", 5000);
		float scale = getAttribReal(XMLNode, "scale", 1000);
		float bow = getAttribReal(XMLNode, "bow", 0);
		float tiling = getAttribReal(XMLNode, "tiling", 10);
		bool drawFirst = getAttribBool(XMLNode, "drawFirst", true);

		// Setup the sky plane
		Ogre::Plane plane;
		plane.normal = Ogre::Vector3(planeX, planeY, planeZ);
		plane.d = planeD;
		mSceneMgr->setSkyPlane(true, plane, material, scale, tiling, drawFirst, bow, 1, 1, m_sGroupName);
	}

	void LevelLoader::processClipping(rapidxml::xml_node<>* XMLNode)
	{
		//! @todo Implement this

		// Process attributes
//	float fNear = getAttribReal(XMLNode, "near", 0);
//	float fFar = getAttribReal(XMLNode, "far", 1);
	}

	void LevelLoader::processLightRange(rapidxml::xml_node<>* XMLNode, Ogre::Light *pLight)
	{
		// Process attributes
		float inner = getAttribReal(XMLNode, "inner");
		float outer = getAttribReal(XMLNode, "outer");
		float falloff = getAttribReal(XMLNode, "falloff", 1.0);

		// Setup the light range
		pLight->setSpotlightRange(Ogre::Angle(inner), Ogre::Angle(outer), falloff);
	}

	void LevelLoader::processLightAttenuation(rapidxml::xml_node<>* XMLNode, Ogre::Light *pLight)
	{
		// Process attributes
		float range = getAttribReal(XMLNode, "range");
		float constant = getAttribReal(XMLNode, "constant");
		float linear = getAttribReal(XMLNode, "linear");
		float quadratic = getAttribReal(XMLNode, "quadratic");

		// Setup the light attenuation
		pLight->setAttenuation(range, constant, linear, quadratic);
	}





	Ogre::String LevelLoader::getProperty(const Ogre::String &ndNm, const Ogre::String &prop)
	{
		for ( unsigned int i = 0 ; i < nodeProperties.size(); i++ )
		{
			if ( nodeProperties[i].mNodeName == ndNm && nodeProperties[i].mPropertyNm == prop )
			{
				return nodeProperties[i].mValueName;
			}
		}

		return "";
	}


}