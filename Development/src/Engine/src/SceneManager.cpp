#include "EnginePrivate.h"

#include "xml/levelloader.h"

#include "camera/CCSCameraControlSystem.h"
#include "camera/CCSBasicCameraModes.h"
#include "camera/CCSFreeCameraMode.h"
#include "camera/CCSOrbitalCameraMode.h"
#include "camera/CCSSphericCameraMode.h"
#include "camera/iCameraBase.h"
#include "HydraxRttListener.h"
#include "ActorDynamicSky.h"
using namespace Ogre;

namespace vega
{
	//-----------------------------------------------------------------------------
	SceneManager::SceneManager()
		:levelloader(nullptr)
		, mCameraCS(nullptr)
		, mCurrentCamera(nullptr)
		, mHRTTL(nullptr)
		, mWater(nullptr)
		, mDSky(nullptr)
	{
		levelloader = new LevelLoader();
		// Create the camera system using the previously created ogre camera.
		mCameraCS = new CameraControlSystem(GetEngine()->mGSceneMgr, "CameraControlSystem", GetEngine()->mGCamera);
		_CreateCameras();
		_InitForSkyWaterDepends();
	}
	//-----------------------------------------------------------------------------
	SceneManager::~SceneManager()	{
		CleanScene();
		SAFE_DELETE(mHRTTL);
	}
	//-----------------------------------------------------------------------------
	void SceneManager::_CreateCameras()
	{
		// -------------------------------------------------------------------------------------
		// Register a "Fixed" camera mode. In this mode the camera position and orientation
		// never change.

		FixedCameraMode* camMode1 = new FixedCameraMode(mCameraCS);
		//mCameraCS->registerCameraMode("Fixed (2)",camMode1);
		mCameraCS->registerCameraMode("Fixed", camMode1);
		camMode1->setCameraPosition(Ogre::Vector3(-500, 0, -500));
		float roll = 0; float yaw = 225; float pitch = 10;
		camMode1->setCameraOrientation(Quaternion(Radian(Degree(roll)), Vector3::UNIT_Z)
			* Quaternion(Radian(Degree(yaw)), Vector3::UNIT_Y)
			* Quaternion(Radian(Degree(pitch)), Vector3::UNIT_X));

		// -------------------------------------------------------------------------------------
		// Register a "FixedTracking" camera mode. In this mode the camera position is fixed
		// and the camera always points to the target.

		FixedTrackingCameraMode*
			camMode2 = new FixedTrackingCameraMode(mCameraCS);
		mCameraCS->registerCameraMode("FixedTracking", camMode2);
		camMode2->setCameraPosition(Ogre::Vector3(500, 0, -100));

		// -------------------------------------------------------------------------------------
		// Register a "Chase" camera mode with default tightness (0.01). In
		// this mode the camera follows the target. The second parameter is the relative position
		// to the target. The orientation of the camera is fixed by a yaw axis (UNIT_Y by default).

		ChaseCameraMode* camMode3 = new ChaseCameraMode(mCameraCS, Ogre::Vector3(0, 0, -200));
		//mCameraCS->registerCameraMode("Chase(0.01 tightness)",camMode3);
		mCameraCS->registerCameraMode("Chase", camMode3);

		// -------------------------------------------------------------------------------------
		// Register a "ChaseFreeYawAxis" camera mode with max tightness. This mode is
		// similar to "Chase" camera mode but the camera orientation is not fixed by
		// a yaw axis. The camera orientation will be the same as the target.

		camMode3 = new ChaseFreeYawAxisCameraMode(mCameraCS, Ogre::Vector3(0, 0, -200)
			, Ogre::Radian(0), Ogre::Radian(Ogre::Degree(180)), Ogre::Radian(0));
		mCameraCS->registerCameraMode("ChaseFreeYawAxis", camMode3);
		camMode3->setCameraTightness(0.05);

		// -------------------------------------------------------------------------------------
		// Register a "FirstPerson" camera mode.

		//FirstPersonCameraMode* camMode4 = new FirstPersonCameraMode(mCameraCS,Ogre::Vector3(0,17,-16)
		FirstPersonCameraMode* camMode4 = new FirstPersonCameraMode(mCameraCS, Ogre::Vector3(0, 6, -20)
			, Ogre::Radian(0), Ogre::Radian(Ogre::Degree(180)), Ogre::Radian(0));
		mCameraCS->registerCameraMode("FirstPerson", camMode4);
		camMode4->setCharacterVisible(false);

		// -------------------------------------------------------------------------------------
		// Register a "PlaneBinded" camera mode. In this mode the camera is constrained to the
		// limits of a plane. The camera always points to the target, perpendicularly to the plane.

		Ogre::Plane* mPlane = new Ogre::Plane(Ogre::Vector3::UNIT_Z, Ogre::Vector3(0, 0, -200));
		PlaneBindedCameraMode* camMode5 = new PlaneBindedCameraMode(mCameraCS, *mPlane);
		mCameraCS->registerCameraMode("PlaneBinded (XY)", camMode5);

		// -------------------------------------------------------------------------------------
		// Register another "PlaneBinded" camera mode using a top point of view.

		mPlane = new Ogre::Plane(Ogre::Vector3::UNIT_Y, Ogre::Vector3(0, 1000, 0));
		camMode5 = new PlaneBindedCameraMode(mCameraCS, *mPlane, Ogre::Vector3::UNIT_Z);
		mCameraCS->registerCameraMode("PlaneBinded (XZ)", camMode5);

		// -------------------------------------------------------------------------------------
		// Register a "ThroughTarget" camera mode. In this mode the camera points to a given
		// position (the "focus") throuh the target. The camera orientation is fixed by a yaw axis.

		ThroughTargetCameraMode* camMode6 = new ThroughTargetCameraMode(mCameraCS, 400);
		mCameraCS->registerCameraMode("ThroughTarget", camMode6);
		//camMode6->setCameraFocusPosition(atheneNode->_getDerivedPosition() - Ogre::Vector3(0,100,0));
		//       camMode6->setCameraFocusPosition(atheneNode->_getDerivedPosition() + Ogre::Vector3(0,100,0));

		// -------------------------------------------------------------------------------------
		// Register a "ClosestToTarget" camera mode. In this camera mode the position of the
		// camera is chosen to be the closest to the target of a given list. The camera
		// orientation is fixed by a yaw axis.

		ClosestToTargetCameraMode* camMode7 = new ClosestToTargetCameraMode(mCameraCS);
		mCameraCS->registerCameraMode("ClosestToTarget", camMode7);

		Ogre::Vector3* camPos1 = new Ogre::Vector3(-400, 0, -400);
		Ogre::Vector3* camPos2 = new Ogre::Vector3(-400, 0, 1400);
		Ogre::Vector3* camPos3 = new Ogre::Vector3(1400, 0, 1400);

		camMode7->addCameraPosition(*camPos1);
		camMode7->addCameraPosition(*camPos2);
		camMode7->addCameraPosition(*camPos3);

		// -------------------------------------------------------------------------------------
		// Register an "Attached" camera mode. In this mode the camera node is attached to the
		// target node as a child.

		AttachedCameraMode* camMode8 = new AttachedCameraMode(mCameraCS, Ogre::Vector3(200, 0, 0)
			, Ogre::Radian(0), Ogre::Radian(Ogre::Degree(90)), Ogre::Radian(0));
		//mCameraCS->registerCameraMode("Attached (lateral)",camMode8);
		mCameraCS->registerCameraMode("Attached", camMode8);

		// -------------------------------------------------------------------------------------
		// Register a "Free" camera mode. In this mode the camera is controlled by the user.
		// The camera orientation is fixed to a yaw axis.

		yaw = 225; pitch = -10;


		// -------------------------------------------------------------------------------------
		// Register a "FixedDirection" camera mode. In this mode the
		// target is always seen from the same point of view.

		FixedDirectionCameraMode* camMode10 = new FixedDirectionCameraMode(mCameraCS, Ogre::Vector3(-1, -1, -1), 1000);
		mCameraCS->registerCameraMode("Fixed direction", camMode10);
		camMode10->setCameraTightness(0.01);

		// -------------------------------------------------------------------------------------
		// Register an "Orbital" camera mode. This is basically an attached camera mode where the user
		// can mofify the camera position. If the scene focus is seen as the center of a sphere, the camera rotates arount it.
		// The last parameter indicates if the camera should be reset to its initial position when this mode is selected.

		OrbitalCameraMode* camMode12 = new OrbitalCameraMode(mCameraCS, 200, Ogre::Radian(0), Ogre::Radian(0), false);
		mCameraCS->registerCameraMode("Orbital", camMode12);
		camMode12->setZoomFactor(100);
		camMode12->setRotationFactor(50);
		// -------------------------------------------------------------------------------------
		// Register a RTS camera mode.
		//

		RTSCameraMode* camMode13 = new RTSCameraMode(mCameraCS,
			Ogre::Vector3(500, 1300, 1000)
			, Ogre::Vector3::NEGATIVE_UNIT_Z
			, Ogre::Vector3::NEGATIVE_UNIT_X
			, Ogre::Radian(Ogre::Degree(70))
			, 0, 1000);
		mCameraCS->registerCameraMode("RTS", camMode13);
		camMode13->setMoveFactor(20);

		// -------------------------------------------------------------------------------------
		// Register the custom "Dummy" camera mode defined previously. It basically goes forward
		// and backward constantly
#if 0
		DummyCameraMode* camMode14 = new DummyCameraMode(mCameraCS, 400);
		mCameraCS->registerCameraMode("Dummy",(CameraControlSystem::CameraMode*)camMode14);

		// -------------------------------------------------------------------------------------
		// Register an "OrbitalWithMouse" camera mode. 

		OrbitalWithMouseCameraMode* camMode15 = new OrbitalWithMouseCameraMode(mCameraCS, OIS::MouseButtonID::MB_Left, 200);
		mCameraCS->registerCameraMode("Orbital + mouse",camMode15);
		camMode15->setZoomFactor(3);		
		camMode15->setRotationFactor(10);
#endif
		// -------------------------------------------------------------------------------------
		// Register a spheric camera mode. 

		Ogre::Vector3 relativePositionToCameraTarget(0, 50, -300);

		// THIS IS NOT NECESSARY. just needed for the demo showing integer values in the sliders
		relativePositionToCameraTarget = relativePositionToCameraTarget.normalisedCopy() * 300;

		SphericCameraMode* camMode16 = new SphericCameraMode(mCameraCS, relativePositionToCameraTarget, 700);
		// outer radious = inner radious = relativePositionToCameraTarget.length() for a perfect sphere
		camMode16->setHeightOffset(50);
		mCameraCS->registerCameraMode("Spherical", camMode16);
	}
	//-----------------------------------------------------------------------------
	void SceneManager::_UpdateCameraMove(float _time)
	{
		for (int i = 0; i < mSceneCameras.GetSize(); i++){
			if (mSceneCameras[i] == mCurrentCamera)
				mSceneCameras[i]->Update(_time);
		}
	}
	//-----------------------------------------------------------------------------
	void SceneManager::CleanScene(){
		mSceneActors.Erase();
	}
	//-----------------------------------------------------------------------------
	void SceneManager::LoadLevel(const char* _mFileName, bool _newLoading)
	{
		if (_newLoading)
			CleanScene();

		levelloader->parseDotScene(_mFileName);
	}
	//-----------------------------------------------------------------------------
	void SceneManager::Update(float _time){
		mCameraCS->update(_time);
		_UpdateCameraMove(_time);
		_UpdateSkyWater(_time);
	}
	//-----------------------------------------------------------------------------
	void SceneManager::ManualStop()
	{
		for (int i = 0; i < mSceneCameras.GetSize(); i++){
			if (mSceneCameras[i] == mCurrentCamera)
				mSceneCameras[i]->ManualStop();
		}
	}
	//-----------------------------------------------------------------------------
	void SceneManager::InjectMouseMove(const Ogre::Vector2/*OIS::MouseEvent*/& evt)
	{
			for (int i = 0; i < mSceneCameras.GetSize(); i++){
				if (mSceneCameras[i] == mCurrentCamera)
					mSceneCameras[i]->InjectMouseMove(evt);
			}
	}
	//-----------------------------------------------------------------------------
	void SceneManager::InjectMouseDown(int id)
	{
		for (int i = 0; i < mSceneCameras.GetSize(); i++){
			if (mSceneCameras[i] == mCurrentCamera)
				mSceneCameras[i]->InjectMouseDown(id);
		}
	}
	//-----------------------------------------------------------------------------
	void SceneManager::InjectMouseUp(int _id)
	{
		OIS::MouseButtonID id = (OIS::MouseButtonID)_id;
		for (int i = 0; i < mSceneCameras.GetSize(); i++){
			if (mSceneCameras[i] == mCurrentCamera)
				mSceneCameras[i]->InjectMouseUp(id);
		}
	}
	//-----------------------------------------------------------------------------
	void  SceneManager::InjectKeyDown(const int evt)
	{
		for (int i = 0; i < mSceneCameras.GetSize(); i++){
			if (mSceneCameras[i] == mCurrentCamera)
				mSceneCameras[i]->InjectKeyDown(evt);
		}
	}
	//-----------------------------------------------------------------------------
	void  SceneManager::InjectKeyUp(const int evt)
	{
		for (int i = 0; i < mSceneCameras.GetSize(); i++){
			if (mSceneCameras[i] == mCurrentCamera)
				mSceneCameras[i]->InjectKeyUp(evt);
		}
	}
	//-----------------------------------------------------------------------------
	void  SceneManager::SetAmbientLight(float r, float g, float b){
		GetEngine()->mGSceneMgr->setAmbientLight(Ogre::ColourValue(r, g, b));
	}
	//-----------------------------------------------------------------------------
	void SceneManager::AddActorToList(Actor* _actor)	{
		mSceneActors.AddElement(&_actor);
	}
	//-----------------------------------------------------------------------------
	void SceneManager::DeleteActor(Actor*_actor)	{
		mSceneActors.DeleteElement(&_actor, true);//with calling destructor of Actor
	}
	//-----------------------------------------------------------------------------
	void SceneManager::DeleteActorFromList(Actor*_actor)	{
		mSceneActors.DeleteElement(&_actor, false);//Just delete from list,without calling destructor of Actor
	}
	//-----------------------------------------------------------------------------
	void SceneManager::AddActorCameraToList(iCameraBase* _actor)	{
		mSceneCameras.AddElement(&_actor);
	}
	//-----------------------------------------------------------------------------
	void SceneManager::DeleteCameraFromList(iCameraBase*_actor)	{
		mSceneCameras.DeleteElement(&_actor, true);//with calling destructor of Actor
	}
	//-----------------------------------------------------------------------------
	void SceneManager::DeleteCameraActor(iCameraBase*_actor)	{
		mSceneCameras.DeleteElement(&_actor, false);//Just delete from list,without calling destructor of Actor
	}
	//-----------------------------------------------------------------------------
	void SceneManager::SetActiveCamera(iCameraBase*_actor)	{
		mCurrentCamera = _actor;//Just delete from list,without calling destructor of Actor
	}
	//-----------------------------------------------------------------------------
	void SceneManager::_ContainActorDynamicSky(ActorDynamicSky*_actor)	{
		mDSky = _actor;
		mHRTTL->SetSky(_actor->GetSkyX());
	}
	//-----------------------------------------------------------------------------
	void SceneManager::_ContainActorWater(ActorWater*_actor)	{
		mWater = _actor;
		mHRTTL->SetWater(_actor->GetHydrax());
		mWater->GetHydrax()->getRttManager()->addRttListener(mHRTTL);
	}
	//-----------------------------------------------------------------------------
	void SceneManager::_UpdateSkyWater(float _time)	{
		if (mWater)
			mWater->Update(_time);
		if (mDSky)
			mDSky->Update(_time);
	}
	//-----------------------------------------------------------------------------
	void SceneManager::_InitForSkyWaterDepends()	{
		mHRTTL = new HydraxRttListener();
	}
}