#include "EnginePrivate.h"
#include "ActorSkyDome.h"

namespace vega
{
	/**
	*/
	ActorSkyDome::ActorSkyDome(const char* _material)
	{
		if(!GetEngine()->mGSceneMgr->isSkyDomeEnabled())
			GetEngine()->mGSceneMgr->setSkyDome(true, _material);
	}
	/**
	*/
	ActorSkyDome::~ActorSkyDome()
	{
		if(GetEngine()->mGSceneMgr->isSkyDomeEnabled())
			GetEngine()->mGSceneMgr->destroySceneNode(GetEngine()->mGSceneMgr->getSkyDomeNode());
	}
}