#include "EnginePrivate.h"
namespace vega
{
	namespace API{
		ENGINE_API void RunScript(const char*_a, bool _debug);
		ENGINE_API void SetPause(bool _a);
		ENGINE_API void RunConsoleCommand(const char*_str);
		ENGINE_API void PlayVideo(const char*_str, bool _abort);
		ENGINE_API bool IsPlayingVideo();
		ENGINE_API void LoadLevel(const char*_name, bool _new);
		ENGINE_API void EngineShoutdown();
		ENGINE_API void AttemptConfig();
		ENGINE_API void SetMouseLock(bool _lock);
	}
}