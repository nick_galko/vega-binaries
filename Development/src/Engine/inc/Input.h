#pragma once

namespace vega
{

	class ENGINE_API Input :public OIS::MouseListener, public OIS::KeyListener
	{
	public:

		enum InputFor
		{
			InputMODE_GAME = 0,
			InputMODE_GUIK = 1,
			InputMODE_GUIM = 2,
			InputMODE_GUIMK = 3,
		};
		InputFor InputMode, preventInputMode;
	public:
		Input();

		void frameRenderingQueued(void);
		void createFrameListener(void);
		void createFrameListener(size_t handle);
		bool keyPressed(const OIS::KeyEvent &arg);
		void windowResized(Ogre::RenderWindow* _window);
		void windowClosed(Ogre::RenderWindow* _window);
		bool mousePressed( const OIS::MouseEvent& evt, OIS::MouseButtonID id);

		bool mouseMoved( const OIS::MouseEvent &_arg );

		bool mouseReleased(const OIS::MouseEvent &_arg,OIS::MouseButtonID);
		bool keyReleased(const OIS::KeyEvent &_arg);

		void setInputMode(InputFor mode);
		void setMouseVisible(bool mode);
		bool getMouseVisible();
		void setInputModeHowPreventInputMode();
		void SetMouseLock(bool _status);
		__inline InputFor getInputMode(){ return InputMode; }
		__inline bool GetMouseLock(){ return mMouseLocked; }
		__inline bool GetKeyBoardLock(){ return mKeyBoardLocked; }
	private:
		bool mInitialised;
		OIS::Keyboard*mKeyboard;
		OIS::Mouse*mMouse;
		OIS::InputManager*mInputManager;
		bool mMouseLocked, mKeyBoardLocked;
	};
}