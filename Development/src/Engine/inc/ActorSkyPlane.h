#pragma once

namespace vega
{
	class ENGINE_API ActorSkyPlane:public Actor
	{
	public:
		ActorSkyPlane(const char* _material="DefMat");
		ActorSkyPlane(const char* _material, const Ogre::Vector4 &_vec);
		ActorSkyPlane(const char* _material,float _a,float _b,float _c,float _d);
		~ActorSkyPlane();
	};
}