#pragma once
#ifndef ENGINEBASE_H
#define ENGINEBASE_H



namespace vega
{
	//-----------------------------------------------------------------------------
	// Base engine class.
	//-----------------------------------------------------------------------------
	class ENGINE_API EngineBase 
	{
	public:
		EngineBase(void);
		virtual ~EngineBase(void);
		void Go(void);
	protected:
		void SetGame(iGame* _game);
		// !@Proxy function
		void LoadEngineModule(const char *_moduleName);
	};


}

#endif//ENGINEBASE_H