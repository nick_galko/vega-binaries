#pragma once

namespace vega
{
	class ENGINE_API ActorMesh:public Actor
	{
	public:
		// !@Cretion new actor mesh
		ActorMesh(const char *_fileName, const char*_name, const char* _material,
			int _mCollisionModel,float _mass,Ogre::Vector3 _mPosition,
			Ogre::Quaternion _mRotation,
			Ogre::Vector3 _scale,
			Ogre::Vector3 _mLinearVelocity);
		// !@Cretion new actor mesh
		ActorMesh(const char *_fileName, const char*_name);
		// !@Cretion new actor mesh
		ActorMesh(const char *_fileName, const char*_name, const char* _material);
		// !@Destruction Entity
		virtual ~ActorMesh();
		// !@Set Material for Mesh
		void setMaterialName(const char* _material = "DefMat");
		// !@Loading Mesh in background
		void _LoadInBackGround();
		// !@Loading Mesh Directly
		void _LoadDirectly();
	private:
		// !@Creation Mesh
		void _createMesh();
	private:
		Ogre::Mesh* pmEntity;
	};
}