#ifndef CONSOLECOMMANDS_H
#define CONSOLECOMMANDS_H

#include "VideoPlayer.h"

namespace vega
{
	namespace ConsoleCommands
	{
		// !@Loading Level command
		void LoadLevel(const MyGUI::UString & _key, const MyGUI::UString & _value);
		//������� ��� ������������
		// !@Loading Level command
		void PlayV(const MyGUI::UString & _key, const MyGUI::UString & _value);
		// !@Help command
		void Help(const MyGUI::UString & _key, const MyGUI::UString & _value);
		// !@EngineShoutdown
		void EngineShoutdown(const MyGUI::UString & _key, const MyGUI::UString & _value) ;
		// !@EngineShoutdown
		void CleanConsole(const MyGUI::UString & _key, const MyGUI::UString & _value);
		void RunScript(const MyGUI::UString & _key, const MyGUI::UString & _value);
		void R_AA(const MyGUI::UString & _key, const MyGUI::UString & _value);
		void R_HDR(const MyGUI::UString & _key, const MyGUI::UString & _value);
		void R_SSAO(const MyGUI::UString & _key, const MyGUI::UString & _value);
		void E_Pause(const MyGUI::UString & _key, const MyGUI::UString & _value);
		void ReloadCfg(const MyGUI::UString & _key, const MyGUI::UString & _value);
	}
}
#endif