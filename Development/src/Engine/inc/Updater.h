#pragma once

namespace vega
{
	class Updater : public Ogre::FrameListener
	{
	public:
		Updater()
			:mUpdatedTime(0.0f)
		{}
		~Updater(){}
		// !@Update Cicle
		virtual bool frameRenderingQueued(const Ogre::FrameEvent& evt);
	public:
		float mUpdatedTime;
	};
}