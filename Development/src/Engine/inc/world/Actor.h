#ifndef ACTOR_H
#define ACTOR_H


namespace vega
{
	struct ENGINE_API Actor:public BaseActorInterface
	{
		Actor();
		virtual ~Actor();
		
		// !@Get Entity Name
		std::string getName();
		// !@Get File Name
		std::string getFileName();
		// !@Set Position of Actor
		void setPosition(const Ogre::Vector3& _pos);
		// !@Set Position of Actor
		void setPosition(float _x,float _y,float _z);
		// !@Get Position of Actor
		Ogre::Vector3& getPosition();
		// !@Set Material
		void setMaterialName(const char* _material);
		// !@Set Orintation
		void setOrientation(const Ogre::Quaternion& _quat);
		// !@Set Scale
		void setScale(const Ogre::Vector3& _scale);
		// !@Set Scale
		void setScale(float _x,float _y,float _z);
		// !@Set Casting Shadows
		void setCastShadows(bool _status);
		// !@Set Linear Velocity
		void setLinearVelocity(const Ogre::Vector3 &_mLinearVelocity);
		// !@Set Linear Velocity
		void setLinearVelocity(float _x,float _y,float _z);
		// !@Set Mass
		void setMass(float _mass);
		// !@Set Collision Model
		void setCollisionModel(int _mCollisionModel);
		// !@Calculate real mesh size using AxisAlignedBox(AABB)
		void calculateSizeUsingAxisAlignedBox();
		// !@Set Yaw Angle
		void setYaw(const Ogre::Radian& _rot);

		// !@Get SceneNode
		Ogre::SceneNode* const getNode(){
			return mNode;
		};
		// !@Get Entity
		Ogre::Entity* const getEntity(){
			return mEntity;
		}
	protected:
		// !@CreateNode
		void _createNode(Ogre::SceneNode* _parentNode = NULL, Ogre::Vector3 _mPosition = Ogre::Vector3(0, 0, 0), Ogre::Quaternion _mRotation = Ogre::Quaternion(1, 0, 0, 0), Ogre::Vector3 _mScale = Ogre::Vector3(1, 1, 1));
		// !@_switchCollision
		void _switchCollision();
	protected:
		Ogre::StaticGeometry *mStaticGeometry;
		IBody*phBody;
	};
}
#endif//ACTOR_H