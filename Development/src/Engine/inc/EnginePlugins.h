#pragma once


namespace vega
{

	class EnginePlugins
	{	
	public:
		// Loads Engine Module from dll
		void LoadEngineModule(const char *modulename);
		// Plug-In dynamic libraries, platform-independent
		std::vector<class DynLib*> modules;

	};
}