#ifndef _OrbitalCameraMode_H_
#define _OrbitalCameraMode_H_


#include "CCSCameraControlSystem.h"
#include "CCSBasicCameraModes.h"

namespace vega
{
	/**
	 * This is basically an attached camera mode where the user
	 * can mofify the camera position. If the scene focus is seen
	 * as the center of a sphere, the camera rotates arount it
	 */
    class OrbitalCameraMode : public ChaseFreeYawAxisCameraMode
	{
	public:

		OrbitalCameraMode(CameraControlSystem* cam, float initialZoom = 1
			, Ogre::Radian initialHorizontalRotation = Ogre::Radian(0), Ogre::Radian initialVerticalRotation = Ogre::Radian(0)
			, bool resetToInitialPosition = true, float collisionmargin = 0.1f) 
			: ChaseFreeYawAxisCameraMode(cam, Ogre::Vector3::ZERO, Ogre::Quaternion::IDENTITY, collisionmargin)
			, mZoomFactor(1)
            , mRotationFactor(0.13)
			, mInitialRotHorizontal(initialHorizontalRotation)
			, mInitialRotVertical(initialVerticalRotation)
			, mInitialZoom(initialZoom)			
			, mZoom(initialZoom)
			, mRotHorizontal(0)
			, mRotVertical(0)
			, mZoomDisplacement(0)
			, mResetToInitialPosition(resetToInitialPosition)
        {
			this->setCameraTightness(1);
		};

        virtual ~OrbitalCameraMode(){};

        virtual bool init();

        virtual void update(const float &timeSinceLastFrame);

        // Specific methods

		/**
		 * @brief Set the zooming speed factor
		 * 
		 * @param unitsPerSecond the units the camera will be zoomed in/out per second
		 */
        inline virtual void setZoomFactor(float unitsPerSecond){ mZoomFactor = unitsPerSecond; }

		/**
		 * @brief Set the rotating speed factor
		 * 
		 * @param radiansPerSecond the radians the camera will be rotated per second
		 */
		inline virtual void setRotationFactor(float radiansPerSecond){ mRotationFactor = radiansPerSecond; }

        /**
		 * @brief Tell the camera to look right
		 * 
		 * @param val percentage of the speed factor [-1,1] (use negative values to look left)
		 */
        inline virtual void yaw(float val = 1){ mRotHorizontalDisplacement += Ogre::Degree(-val); }

        /**
		 * @brief Get the current yaw value
		 * 
		 * @return the amount of rotation
		 */
        inline virtual Ogre::Radian getYaw(){ return -mRotHorizontal; }

        /**
		 * @brief Set the yaw value
		 * 
		 * @param val the amount of rotation (use negative values to look left)
		 */
        inline virtual void setYaw(Ogre::Radian val){ mRotHorizontal = mInitialRotHorizontal - val; }

        /**
		 * @brief Tell the camera to look down
		 * 
		 * @param val percentage of the speed factor [-1,1] (use negative values to look up)
		 */
        inline virtual void pitch(float val = 1){ mRotVerticalDisplacement += Ogre::Degree(-val); }

        /**
		 * @brief Get the current pitch value
		 * 
		 * @return the amount of rotation
		 */
        inline virtual Ogre::Radian getPitch(){ return -mRotVertical; }

        /**
		 * @brief Set the pitch value
		 * 
		 * @param val the amount of rotation (use negative values to look up)
		 */
        inline virtual void setPitch(Ogre::Radian val){ mRotVertical = mInitialRotVertical - val; }

		/**
		 * @brief Tell the camera to zoom out
		 * 
		 * @param val percentage of the speed factor [-1,1] (use negative values to zoom in)
		 */
		inline virtual void zoom(float val = 1){ mZoomDisplacement += val; }

        /**
		 * @brief Get the current zoom value
		 * 
		 * @return the amount of zoom
		 */
        inline virtual float getZoom(){ return mZoom; }

        /**
		 * @brief Set the zoom value
		 * 
		 * @param val the amount of zoom (use negative values to zoom in)
		 */
        inline virtual void setZoom(float val){ mZoom = val; }

		virtual void reset();
		
    protected:
        
        float mZoomFactor;
        float mRotationFactor;
        Ogre::Radian mRotHorizontal;
	    Ogre::Radian mRotVertical;
		float mZoom;
		Ogre::Radian mInitialRotHorizontal;
	    Ogre::Radian mInitialRotVertical;
		float mInitialZoom;
		Ogre::Radian mRotHorizontalDisplacement;
	    Ogre::Radian mRotVerticalDisplacement;
		float mZoomDisplacement;
		bool mResetToInitialPosition;
	};

}

#endif
