#ifndef _SphericCameraMode_H_
#define _SphericCameraMode_H_


#include "CCSCameraControlSystem.h"

namespace vega
{
	/**
	 * The camera position is constrained to the extent of a sphere centered in the player (Zelda-style). 
	 */
    class SphericCameraMode : public CameraControlSystem::CameraMode//, public CameraControlSystem::CollidableCamera
	{
	public:

		/**
		 * @param relativePositionToCameraTarget Default camera position with respecto to camera target
		 * @param autoResetTime If greater than zero the camera will be resetted to its initial position after the amount of seconds specified.
		 * @param resetDistance true for correcting the camera distance to the target when resetting. If false, the camera will maintaing the current distance to target.
		 * @param resetRotationFactor Speed factor for correcting the camera rotation when resetting.
		 * @param offset Offset applied to the center of the sphere.  
		 */
        SphericCameraMode(CameraControlSystem* cam
			, const Ogre::Vector3 &relativePositionToCameraTarget
			, float outerSphereRadious, float innerSphereRadious = 0.0f
			, float autoResetTime = 3.0f
			, bool resetDistance = true
			, float resetRotationFactor = 1.0f
			, const Ogre::Vector3 &offset = Ogre::Vector3::ZERO
			, const Ogre::Vector3 &fixedAxis = Ogre::Vector3::UNIT_Y) 
            : CameraControlSystem::CameraMode(cam)
            //, CameraControlSystem::CollidableCamera(cam, margin)
			, mRelativePositionToCameraTarget(relativePositionToCameraTarget)
			, mOuterSphereRadious(outerSphereRadious), mInnerSphereRadious(innerSphereRadious)
			, mAutoResetTime(autoResetTime)
			, mResetDistance(resetDistance)
			, mResetRotationFactor(resetRotationFactor)
			, mOffset(offset)
			, mFixedAxis(fixedAxis)
			, mResseting(false)
			, mLastRessetingDiff(Ogre::Radian(2.0f * Ogre::Math::PI))
        {
			/*this->collisionDelegate = newCollisionDelegate(this
				, &CollidableCamera::DefaultCollisionDetectionFunction);*/
			if( innerSphereRadious > outerSphereRadious){ throw new Ogre::Exception(1001,"Inner radious greater than outer radious","CCSSphericCameraMode.h"); }
			if( innerSphereRadious > relativePositionToCameraTarget.length() 
				|| outerSphereRadious < relativePositionToCameraTarget.length())
			{ 
				throw new Ogre::Exception(1002,"relativePositionToCameraTarget param value not valid.","CCSSphericCameraMode.h"); 
			}
		}

        virtual ~SphericCameraMode(){};

        virtual bool init();

        virtual void update(const float &timeSinceLastFrame);

        virtual void instantUpdate();

        // Specific methods

		/**
		 * @brief Reset the camera position to its initial position.
		 *
		 * @param instant true for doing it instantaniously; false for doing it smoothly.
		 */
		void reset(bool instant = true);

		/**
		 * @param outerSphereRadious cannot be smaller than current inner radious nor the length of relativePositionToCameraTarget.
		 */
		void setOuterSphereRadious(float outerSphereRadious);
		float getOuterSphereRadious(){ return mOuterSphereRadious; }

		/**
		 * @param innerSphereRadious cannot be greater than current outer radious nor the length of relativePositionToCameraTarget.
		 */
		void setInnerSphereRadious(float innerSphereRadious);
		float getInnerSphereRadious(){ return mInnerSphereRadious; }

		/**
		 * @brief Displacement of the center of the sphere with respect to the fixed axis
		 */
		void setHeightOffset(float value){ mOffset = mFixedAxis.normalisedCopy() * value; }
		void setOffset(Ogre::Vector3 offset){ mOffset = offset; }
		Ogre::Vector3 getOffset(){ return mOffset; }

		/**
		 * @param seconds zero for disabling.
		 */
		void setAutoResetTime(float seconds){ mAutoResetTime = seconds; }
		float getAutoResetTime(){ return mAutoResetTime; }
		
    protected:

        Ogre::Vector3 mFixedAxis;
		Ogre::Vector3 mOffset;
		float mOuterSphereRadious;
		float mInnerSphereRadious;
        float mResetDistance;
        float mResetRotationFactor;
		Ogre::Vector3 mRelativePositionToCameraTarget;
		float mAutoResetTime;
		float mTimeSinceLastChange;
		bool mResseting;
		Ogre::Radian mLastRessetingDiff;
		Ogre::Vector3 mLastTargetPosition;
		Ogre::Quaternion mLastTargetOrientation;
	};

}

#endif
