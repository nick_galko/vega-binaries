#pragma once
#include "iCameraBase.h"

namespace vega
{
	class ENGINE_API CameraFree :public iCameraBase{
	public:
		CameraFree(bool _active=false);
		virtual~CameraFree();
		void setYaw(float _angle);
		void setPitch(float _angle);
		virtual void InjectMouseUp(const int id);
		virtual void InjectMouseDown(const int  id);
		virtual void InjectMouseWheel(const int evt){}
		virtual void InjectMouseMove(const Ogre::Vector2& evt/*const OIS::MouseEvent& evt*/);
		virtual void InjectKeyDown(const int evt);
		virtual void InjectKeyUp(const int evt);
		virtual void ManualStop();
		virtual void Update(float);
	private:
		class CameraControlSystem*mCameraCS;
		class FreeCameraMode* camMode9;
	};
}