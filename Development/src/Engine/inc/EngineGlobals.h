#pragma once


namespace vega
{
	class Render;
	class iGame;
	struct iInterp;
	class StaticPluginLoader;
	class Window;
	class EnginePlugins;
	class Input;
	class Console;
	class AudioManager;
	class GUI;
	class SceneManager;
	class Physics;
	class Updater;

	struct ENGINE_API EngineGlobals :public CoreSystems
	{
		EngineGlobals();
		virtual ~EngineGlobals();
		virtual void Initialize();
		void StartupOgre();
		void Update(float _time);
		virtual void Release();
		FORCEINLINE bool isLaunched(){ return mShutDown; }
		// !@Engine Shoutdown
		void EngineShoutdown();
		void Go(void);
		void SetGame(iGame*_game);

		void RunScript(const char*_filename, bool _debug = false);

		//OGRE
		// !@Creating Listeners for Update cicle
		void CreateFrameListener(void);
		// !@Update Cicle
		bool Update(const Ogre::FrameEvent& evt);
		// !@Creation Ogre 
		bool Setup(void);
		// !@Configure Engine
		bool Configure(void);
		// !@Choosing SceneManager from loaded addon
		void ChooseSceneManager(void);
		// !@Creating Fly and Standart Camera
		void CreateCamera(void);
		// !@Creating Main Viewport
		void CreateViewports(void);
		// !@Adding All Zip's,user dir's and directories(using masks)
		void SetupResources();
		// !@Loading Resources
		void LoadResources(void);
		// !@Creating classes for struct engine
		void PreInitSystems(void);
		// !@Initiliasing systems
		void StartupSystems();
		// !@Precache
		void PrecacheResources(std::string _mResourcesCfg);
		// !Set Pause
		void SetPause(bool _a);
		// !Run Console Command
		void RunConsoleCommand(const char*_str);
		// !Attempt Config Params
		void AttemptConfig();
	public:
		Window*				              window;				// Ogre window listener
		EnginePlugins* 		              plugins;				// plugins
		Input*				              input;				// input
		Console*			              console;
		Render*                           render;				// render
		iAudioDevice*                     audio;			    // audio
		GUI*			                  gui;					// GUI
		SceneManager*		              sceneManager;			// Level Loader
		iBasePhysics*                     physics;		        // Physics
		iGame*                            game;					// Game
		StaticPluginLoader*               mOgrePluginLoader;
		Updater*                          updater;
	private:
		// !@For loading dll-modules from Modules.ini 
		void _LoadEngineModules();
	private:
		bool mShutDown;
	};
	ENGINE_API EngineGlobals*GetEngine();
}