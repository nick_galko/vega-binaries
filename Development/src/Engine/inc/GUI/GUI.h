#pragma once

namespace vega
{
	class GUI
	{
	public:
		GUI();
		void Initialize();
		bool frameRenderingQueued(float _evt);
	private:
#ifndef __ANDROID__
		MyGUI::OgrePlatform* mPlatform;
		MyGUI::Gui * mGUI;
#endif
	};
}