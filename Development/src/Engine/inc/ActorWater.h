#pragma once


namespace vega
{
	class Hydrax;
	struct EngineGlobals;

	class ENGINE_API ActorWater :private Actor
	{
	public:
		ActorWater();
		~ActorWater();
		void AddWave(Ogre::Vector2 dir = Ogre::Vector2(0, 0), float A = 0.0f, float T = 0.0f, float p = 0.0f);
		void LoadCfg(const char _name);
		void Update(float evt);
		__inline Hydrax*GetHydrax(){ return water; }
	private:
		void _Init(const std::string &_profile);
	private:
		Hydrax*water;
		EngineGlobals*mG;
	};
}