#pragma once


#include "xml/XMLRead.h"

namespace vega
{
	class NodeProperty
	{
	public:
		std::string mNodeName;
		std::string mPropertyNm;
		std::string mValueName;
		std::string mTypeName;

		NodeProperty(const std::string &node, const std::string &propertyName, const std::string &value, const std::string &type)
			: mNodeName(node), mPropertyNm(propertyName), mValueName(value), mTypeName(type) {}
	};

	class LevelLoader:private XMLRead
	{
	public:


		Ogre::TerrainGlobalOptions *mTerrainGlobalOptions;

		LevelLoader();
		virtual ~LevelLoader();

		void parseDotScene(const Ogre::String &SceneName, Ogre::SceneNode *pAttachNode = NULL, const Ogre::String &sPrependNode = "");
		Ogre::String getProperty(const Ogre::String &ndNm, const Ogre::String &prop);

		Ogre::TerrainGroup* getTerrainGroup() { return mTerrainGroup; }

		std::vector<NodeProperty> nodeProperties;
		std::vector<Ogre::String> staticObjects;
		std::vector<Ogre::String> dynamicObjects;

	private:
		struct XMLData		{
			XMLData();
			Ogre::Vector3 mPosition;
			Ogre::Vector3 mLinearVelocity;
			Ogre::Vector3 mScale;
			Ogre::Quaternion mRotation;
			Ogre::String meshFile;
			Ogre::String materialFile;
			float mCollisionModel;
			float mMass;
		};
		XMLData mXML;
		void processScene(rapidxml::xml_node<>* XMLRoot);

		void processNodes(rapidxml::xml_node<>* XMLNode);
		void processScripts(rapidxml::xml_node<>* XMLNode);
		void processEnvironment(rapidxml::xml_node<>* XMLNode);
		void processTerrain(rapidxml::xml_node<>* XMLNode);
		void processTerrainPage(rapidxml::xml_node<>* XMLNode);
		void processBlendmaps(rapidxml::xml_node<>* XMLNode);
		void ProcessLight(rapidxml::xml_node<>* XMLNode, Ogre::SceneNode *pParent = 0);
		void processCamera(rapidxml::xml_node<>* XMLNode, Ogre::SceneNode *pParent = 0);

		void processNode(rapidxml::xml_node<>* XMLNode, Ogre::SceneNode *pParent = 0);
		void processLookTarget(rapidxml::xml_node<>* XMLNode, Ogre::SceneNode *pParent);
		void processTrackTarget(rapidxml::xml_node<>* XMLNode, Ogre::SceneNode *pParent);
		void processEntity(rapidxml::xml_node<>* XMLNode, Ogre::SceneNode *pParent);
		void processParticleSystem(rapidxml::xml_node<>* XMLNode, Ogre::SceneNode *pParent);
		void processBillboardSet(rapidxml::xml_node<>* XMLNode, Ogre::SceneNode *pParent);
		void processPlane(rapidxml::xml_node<>* XMLNode, Ogre::SceneNode *pParent);

		void processFog(rapidxml::xml_node<>* XMLNode);
		void processSkyBox(rapidxml::xml_node<>* XMLNode);
		void processSkyDome(rapidxml::xml_node<>* XMLNode);
		void processSkyPlane(rapidxml::xml_node<>* XMLNode);
		void processClipping(rapidxml::xml_node<>* XMLNode);

		void processLightRange(rapidxml::xml_node<>* XMLNode, Ogre::Light *pLight);
		void processLightAttenuation(rapidxml::xml_node<>* XMLNode, Ogre::Light *pLight);

		void AssetParse(rapidxml::xml_node<>* XMLNode);


		Ogre::SceneManager *mSceneMgr;
		Ogre::SceneNode *mAttachNode;
		Ogre::String m_sGroupName;
		Ogre::String m_sPrependNode;
		Ogre::TerrainGroup* mTerrainGroup;
		Ogre::Vector3 mTerrainPosition;
		Ogre::Vector3 mLightDirection;
	};
}