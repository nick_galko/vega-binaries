#pragma once
#ifndef XMLREAD_H
#define XMLREAD_H

// Depends
// XML parser
#include "../../../external/rapidxml-1.13/rapidxml.hpp"

//-----------------------------------------------------------------------------
// Forward declarations.
//-----------------------------------------------------------------------------

namespace Ogre
{
	class SceneManager;
	class SceneNode;
	class TerrainGroup;
	class TerrainGlobalOptions;
}

namespace vega
{
	class XMLRead
	{
	protected:
		Ogre::String getAttrib(rapidxml::xml_node<>* XMLNode, const Ogre::String &parameter, const Ogre::String &defaultValue = "");
		float getAttribReal(rapidxml::xml_node<>* XMLNode, const Ogre::String &parameter, float defaultValue = 0);
		bool getAttribBool(rapidxml::xml_node<>* XMLNode, const Ogre::String &parameter, bool defaultValue = false);

		Ogre::Vector3 parseVector3(rapidxml::xml_node<>* XMLNode);
		Ogre::Quaternion parseQuaternion(rapidxml::xml_node<>* XMLNode);
		Ogre::ColourValue parseColour(rapidxml::xml_node<>* XMLNode);

	};
}

#endif