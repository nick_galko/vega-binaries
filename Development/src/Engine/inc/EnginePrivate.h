#pragma once
#ifndef ENGINEPRIVATE_H
#define ENGINEPRIVATE_H

#ifndef __ANDROID__
#ifdef ENGINE_EXPORTS
#define ENGINE_API __declspec(dllexport)
#else
#define ENGINE_API __declspec(dllimport)
#endif
#else
#define ENGINE_API 
#endif

//-----------------------------------------------------------------------------
// Visual Studio Warnings
//-----------------------------------------------------------------------------
#pragma warning(disable: 4267) /// convert type1->type2
#pragma warning(disable: 4390)
#pragma warning(disable: 4305)
//-----------------------------------------------------------------------------
// Engine public includes.
//-----------------------------------------------------------------------------
//Render.dll � ��� ��� ������� ������ �����������,� ������� ��� ������� Core
#include "../../Render/inc/RenderPrivate.h"


//external
#ifndef __ANDROID__
#include "MyGUI_OgrePlatform.h"
#include "MyGUI.h"
#endif

//Main Engine
#include "EngineGlobals.h"
#include "Window.h"
#include "EnginePlugins.h"
#include "EngineBase.h"
#include "Console.h"
#include "Render.h"
#include "SceneManager.h"
#include "Input.h"
#include "GUI/GUI.h"
//Entity
#include "world/Actor.h"
#include "ActorMesh.h"
#include "ActorVegetation.h"
#include "ActorWater.h"
#include "ActorLight.h"

#endif