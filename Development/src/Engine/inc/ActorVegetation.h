// !Vegetations Class(Grass,Tree)
#pragma once

namespace vega
{
	class ENGINE_API ActorVegetation:private Actor
	{
	public:
		ActorVegetation(const std::string &_fileName,const Ogre::Vector3& _position=Ogre::Vector3(0,0,0), 
			const Ogre::Radian& _rot = Ogre::Radian(0), float _scale = 1.0f,const std::string &_material="DefVegetationMat");
		void setCastShadows(bool _status);
	private:
		//	!This is for creation Vegetation from OgreMesh
		void _createMeshVegetation(const std::string &_fileName,const std::string &_material,const Ogre::Vector3& _position,
			const Ogre::Radian& _rot, float _scale);
	private:
		Ogre::MeshPtr pmEntity;
		bool collisionStatus;
	};
}