#pragma once
#ifndef EMMITTER_P_H
#define EMMITTER_P_H

namespace vega
{
	class ENGINE_API EmitterParticle:public Actor
	{
	public:
		EmitterParticle(const char *name,const char *_temp);
		~EmitterParticle();
		//   Attaching to Node(SceneNode)
		void attach(Actor *_mParent);
	private:

		Ogre::ParticleSystem* ps;
	};
}
#endif