#ifndef CONSOLE_H
#define CONSOLE_H

#ifndef __ANDROID__
#include <list>
#include <vector>
#include "GUI/BaseLayout/BaseLayout.h"

namespace vega
{
typedef MyGUI::delegates::CDelegate2<const MyGUI::UString&, const MyGUI::UString&> CommandDelegate;

	namespace formates
	{
		template<typename T> FORCEINLINE std::string format()
		{
			return MyGUI::utility::toString("[ ", (std::numeric_limits<T>::min)(), " | ", (std::numeric_limits<T>::max)(), " ]");
		}
		template<> FORCEINLINE std::string format<bool>()
		{
			return "[ true | false ]";
		}
		template<> FORCEINLINE std::string format<float>()
		{
			return MyGUI::utility::toString("[ ", -(std::numeric_limits<float>::max)(), " | ", (std::numeric_limits<float>::max)(), " ]");
		}
		template<> FORCEINLINE std::string format<double>()
		{
			return MyGUI::utility::toString("[ ", -(std::numeric_limits<double>::max)(), " | ", (std::numeric_limits<double>::max)(), " ]");
		}
	}

	class Console :
		public wraps::BaseLayout,
		public Ogre::LogListener
	{
	public:
		Console();

		void PrintTextToConsole(const MyGUI::UString& _line);
		void PrintTextToConsole(const MyGUI::UString& _reason, const MyGUI::UString& _key, const MyGUI::UString& _value);

		void CleanConsole();
		void Initialize();

		/** Method : add command.\n
			@example Add_console_command
			@code
				RegisterConsoleDelegate("your_command_1","description", MyGUI::newDelegate(your_func));
				registerConsoleDelegate("your_command_2","description", MyGUI::newDelegate(your_static_method));
				registerConsoleDelegate("your_command_3","description", MyGUI::newDelegate(your_class_ptr, &your_class_name::your_method_name));
			@endcode

			signature your method : void method(const MyGUI::UString & _key, const MyGUI::UString & _value)
		*/
		void RegisterConsoleDelegate(const MyGUI::UString& _command,const MyGUI::UString& _des, CommandDelegate::IDelegate* _delegate);

		/** Remove command. */
		void UnregisterConsoleDelegate(const MyGUI::UString& _command);

		/** Event : Unknown command.\n
			signature : void method(const MyGUI::UString & _key, const MyGUI::UString & _value)
		*/
		CommandDelegate eventConsoleUnknowCommand;

		const MyGUI::UString& GetConsoleStringCurrent() const;
		const MyGUI::UString& GetConsoleStringError() const;
		const MyGUI::UString& GetConsoleStringSuccess() const;
		const MyGUI::UString& GetConsoleStringUnknow() const;
		const MyGUI::UString& GetConsoleStringFormat() const;

		bool GetVisible();
		void SetVisible(bool _visible);

		template <typename T>
		bool IsAction(T& _result, const MyGUI::UString& _key, const MyGUI::UString& _value, const MyGUI::UString& _format = "")
		{
			if (_value.empty())
			{
				addToConsole(getConsoleStringCurrent(), _key, MyGUI::utility::toString(_result));
			}
			else
			{
				if (!MyGUI::utility::parseComplex(_value, _result))
				{
					addToConsole(getConsoleStringError(), _key, _value);
					addToConsole(getConsoleStringFormat(), _key, _format.empty() ? formates::format<T>() : _format);
				}
				else
				{
					addToConsole(getConsoleStringSuccess(), _key, _value);
					return true;
				}
			}
			return false;
		}

		void GetListCommands();
	private:
		void NotifyWindowButtonPressed(MyGUI::Window* _sender, const std::string& _button);

		void NotifyMouseButtonClick(MyGUI::Widget* _sender);
		void NotifyComboAccept(MyGUI::ComboBox* _sender, size_t _index);
		void NotifyButtonPressed(MyGUI::Widget* _sender, MyGUI::KeyCode _key, MyGUI::Char _char);

		void RegisterBaseCommands();
		void messageLogged(const Ogre::String &, Ogre::LogMessageLevel, bool, const Ogre::String &, bool &);
	private:
		MyGUI::EditBox* mListHistory;
		MyGUI::ComboBox* mComboCommand;
		MyGUI::Button* mButtonSubmit;

		typedef std::map<MyGUI::UString, CommandDelegate> MapDelegate;
		MapDelegate mDelegates;
		typedef std::map<MyGUI::UString, MyGUI::UString> DescriptionsMap;
		DescriptionsMap mDescriptions;

		MyGUI::UString mStringCurrent;
		MyGUI::UString mStringError;
		MyGUI::UString mStringSuccess;
		MyGUI::UString mStringUnknow;
		MyGUI::UString mStringFormat;

		// ���� ������� ����� ������������
		bool mAutocomleted;
	};

};
#endif
#endif