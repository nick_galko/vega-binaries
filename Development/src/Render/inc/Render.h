//������������� ����� �������
//����� ����� ������������\�������������� Deffered Rendering,����������� � �.�
#pragma once

namespace vega
{
	class Externals;
	class CSMGpuConstants;
	class OwnShadowCameraSetup;
	class DeferredShadingSystem;
	class GlobalIlluminationManager;
	class VideoPlayer;

	class RENDER_API Render
	{
	public:
		Render( CoreSystems* _manager);
		~Render();

		void Initialize();
		void Update(float _evt);


		void PostEffectSetStatus(std::string _name, bool _status);
		bool PlayVideo(std::string _name);
		VideoPlayer* GetPlayer();
		void EnableDisableAA(bool _s);
	private:
		void _InitPostEffects();
		void _CreatePostEffects();
		void _EnablePostEffects();
		//Cascaded Shadow Mapping	
		void _CreateOwnSMCamera();
		void _CreateConstants();
		void _CreateDeferredShading();

		//Loading Shaders
		void _LoadShaders();
	public:
		Ogre::Root *mRoot;
		Ogre::SceneManager* mSceneMgr;
		Ogre::RenderWindow* mWindow;
		Ogre::Camera*mCamera;
		Ogre::Viewport*mViewport;

		Externals*externals;
		CSMGpuConstants*mGpuConstants;
		OwnShadowCameraSetup* mShadowCamera;
		EngineConfig*   mEngineConfig;
	private:
		DeferredShadingSystem *mSystem;
		std::vector<std::string>mCompositorNames;
	};
}