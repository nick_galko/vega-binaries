#ifndef OWNSHADOW_CAMERA_SETUP_H
#define OWNSHADOW_CAMERA_SETUP_H

#ifndef SHADOW_CAMERA_SETUP_STABLE_CSM_H
#include "ShadowCameraSetupStableCSM.h"
#endif

namespace vega
{
	class OwnShadowCameraSetup : public StableCSMShadowCameraSetup
	{
	public:
		OwnShadowCameraSetup(CSMGpuConstants* constants);
		~OwnShadowCameraSetup();
	};

} // namespace vega

#endif // SHADOW_CAMERA_SETUP_STABLE_CSM_H
