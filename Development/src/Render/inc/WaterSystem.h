#pragma once
#include "renderdllex.h"

namespace vega
{
	class RENDER_API WaterSystem
	{
	public:
		explicit WaterSystem(Render*_render);
		void Init(const std::string &_profile="Default.wsf");
		void Update(float evt);
	private:
		Render*render;
	};
}