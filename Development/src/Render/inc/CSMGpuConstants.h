#ifndef CSM_GPU_CONSTANTS_H
#define CSM_GPU_CONSTANTS_H


namespace vega
{
	class CSMGpuConstants
	{
	public:
		CSMGpuConstants(size_t cascadeCount);
		void updateCascade(const Ogre::Camera &texCam, size_t index);

	private:
		Ogre::GpuSharedParametersPtr mParamsScaleBias;
		Ogre::GpuSharedParametersPtr mParamsShadowMatrix;

		Ogre::Matrix4 mFirstCascadeViewMatrix;
		float mFirstCascadeCamWidth;
		float mViewRange;
		Ogre::Matrix4 hack;
	};

} // namespace vega

#endif // CSM_GPU_CONSTANTS_H
