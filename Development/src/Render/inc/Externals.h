#pragma once
#include "renderdllex.h"

namespace vega
{
	class WaterSystem;
	class RENDER_API Externals
	{
	public:
		explicit Externals(Render*_render);
		~Externals();

		// LWater
		WaterSystem* CreateWater(const std::string &_profile);

		void Update(float evt);

		WaterSystem*waterEx;
		class VideoPlayer* mVideoPlayer;
	private:
		Render*mRenderFactory;
	};
}