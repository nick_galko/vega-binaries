#include "RenderPrivate.h"

#include "VideoPlayer.h"

namespace vega
{
	Externals::Externals(Render*_render)
		:waterEx(nullptr),
		mVideoPlayer(nullptr),
		mRenderFactory(_render)
	{
		mVideoPlayer = new VideoPlayer(_render);
	}

	Externals::~Externals()
	{
		SAFE_DELETE(waterEx);
		SAFE_DELETE(mVideoPlayer);
	}
		
	void Externals::Update(float evt)
	{
		if (mVideoPlayer)
			mVideoPlayer->Update();
	}

}