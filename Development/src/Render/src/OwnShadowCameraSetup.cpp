#include "RenderPrivate.h"
#include "ShadowCameraSetupStableCSM.h"
#include "OwnShadowCameraSetup.h"

using namespace Ogre;

namespace vega
{

	OwnShadowCameraSetup::OwnShadowCameraSetup(CSMGpuConstants* constants) 
		:StableCSMShadowCameraSetup(constants)
	{
		
	}
	//---------------------------------------------------------------------
	OwnShadowCameraSetup::~OwnShadowCameraSetup()
	{
	}
}