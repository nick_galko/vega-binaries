#include "RenderPrivate.h"
#include "CSMGpuConstants.h"
#include "OwnShadowCameraSetup.h"
#include "VideoPlayer.h"
#include "PostEffects/PostEffects.h"

#include "DeferredAndGI/DeferredShading.h"
#include "DeferredAndGI/GeomUtils.h"
using namespace Ogre;

namespace vega
{

	//-------------------------------------------------------------------------------------
	Render::Render(CoreSystems* _manager)
		: mRoot(_manager->mGRoot)
		, mCamera(_manager->mGCamera)
		, mWindow(_manager->mGWindow)
		, mSceneMgr(_manager->mGSceneMgr)
		, mViewport(_manager->mGViewport)
		, mGpuConstants(nullptr)
		, mShadowCamera(nullptr)
		, mSystem(nullptr)
		, mEngineConfig(_manager->mEngineConfig)
	{
		Debug("[render]Render::Render");
	
#ifdef __EXPEREMENTAL__
		mSystem = new DeferredShadingSystem(mViewport, mSceneMgr, mCamera);
#endif
		//������� �������������� ������ � �������
		externals = new Externals(this);
	}

	//-------------------------------------------------------------------------------------
	void Render::_CreateDeferredShading(){
#ifdef __EXPEREMENTAL__
		Debug("Render::_CreateDeferredShading()");
		mSystem->initialize();
#endif
	}

	//-------------------------------------------------------------------------------------
	void Render::_CreateOwnSMCamera()
	{
#ifdef __EXPEREMENTAL__
		Debug("Render::_CreateOwnSMCamera()");
		// Scene manager shadow settings
		mSceneMgr->setShadowTechnique(SHADOWTYPE_TEXTURE_MODULATIVE_INTEGRATED);
		mSceneMgr->setShadowCasterRenderBackFaces(false);
		static int textureCount = 4;
		mSceneMgr->setShadowTextureCount(textureCount);
		mSceneMgr->setShadowTextureCountPerLightType(Ogre::Light::LT_DIRECTIONAL, textureCount);
		mSceneMgr->setShadowTextureCountPerLightType(Ogre::Light::LT_SPOTLIGHT, textureCount);
		mSceneMgr->setShadowTextureCountPerLightType(Ogre::Light::LT_POINT, 1);

		for (int i = 0; i < textureCount; i++)
			mSceneMgr->setShadowTextureConfig(i, 1024, 1024, Ogre::PF_FLOAT32_RGBA,0, 2);//Use a value of "2" to use a different depth buffer pool and avoid sharing this with the Backbuffer's
		static float farClip = 5000.0f;

		// Create the CSM shadow setup
		mShadowCamera = new OwnShadowCameraSetup(mGpuConstants);


		float lambda = 0.93f; // lower lamdba means more uniform, higher lambda means more logarithmic
		float firstSplitDist = mCamera->getNearClipDistance() + 500.0f;
		if (!firstSplitDist){
			Debug("mCamera->getNearClipDistance()=0,set 50.0f");
			firstSplitDist = 50.0f;
		}
		mShadowCamera->calculateSplitPoints(textureCount, firstSplitDist, farClip, lambda);

		StableCSMShadowCameraSetup::SplitPointList points = mShadowCamera->getSplitPoints();
		// Apply settings
		mShadowCamera->setSplitPoints(points);
		float splitPadding = 1.0f;
		mShadowCamera->setSplitPadding(splitPadding);

		//�����������
		mSceneMgr->setShadowCameraSetup(ShadowCameraSetupPtr(mShadowCamera));
#endif
	}
	/**
	*/
	bool Render::PlayVideo(std::string _name)	{
		return externals->mVideoPlayer->playVideo(_name, true);
	}
	//-------------------------------------------------------------------------------------
	void Render::EnableDisableAA(bool _s){
		int r = mEngineConfig->mAntiAlliasing;
		switch (r){
		case 1:
			PostEffectSetStatus("SMAA", _s);
			Debug("[AA]_EnableDisableAA SMAA-Render %i", _s);
			break;
		case 2:
			PostEffectSetStatus("SSAA", _s);
			Debug("[AA]_EnableDisableAA SSAA-Render %i", _s);
			break;
		case 3:
			PostEffectSetStatus("FXAA", _s);
			Debug("[AA]_EnableDisableAA FXAA-Render %i", _s);
			break;
		}
	}
	//-------------------------------------------------------------------------------------
	VideoPlayer* Render::GetPlayer() {
		return externals->mVideoPlayer;
	}
	//-------------------------------------------------------------------------------------
	void Render::_LoadShaders()
	{
		Debug("Render::_LoadShaders()");
		Ogre::ResourceGroupManager*ptr = Ogre::ResourceGroupManager::getSingletonPtr();
		if (!ptr)
			std::exception("[Render::_LoadShaders]is not exist ResourceGroupManager!");
		//��������� �������
		//Shaders Zip's
		Ogre::StringVectorPtr gameArchives = ptr->findResourceNames("Shaders", "*.shaders", false);
		for (unsigned int i = 0; i < gameArchives->size(); i++)
		{
			ptr->addResourceLocation("..\\Engine\\Shaders\\" + (*gameArchives)[i], "Zip", "Shaders", true);
			ptr->addResourceLocation("..\\Engine\\D3D11Shaders\\" + (*gameArchives)[i], "Zip", "Shaders", true);
		}
		ptr->addResourceLocation("..\\Engine\\D3D11Shaders\\", "FileSystem", "Shaders", true);

		ptr->initialiseResourceGroup("Shaders");

		//DEBUG
#ifdef _DEBUG
			gameArchives = Ogre::ResourceGroupManager::getSingleton().listResourceNames("Shaders");
			for (unsigned int i = 0; i < gameArchives->size(); i++)
				Debug("Shader File %s,number %i of shader archives", ((*gameArchives)[i]).c_str(), i);
#endif
	}
	//-------------------------------------------------------------------------------------
	void Render::Initialize()
	{
		Debug("Render::Initialize()");
		_CreateConstants();
		//Cascaded Shadow Mapping	
		_CreateOwnSMCamera();
		//��������������� ��� �������������� ���������,������ �������
		_LoadShaders();

		_CreateDeferredShading();
		static bool firstStart = true;
		if (firstStart)
		{
			_InitPostEffects();
			firstStart = false;
		}
	}
	//-------------------------------------------------------------------------------------
	void Render::Update(float _evt)	{
		externals->Update(_evt);
	}
	//-------------------------------------------------------------------------------------
	Render::~Render()	{
		SAFE_DELETE(mSystem);
		SAFE_DELETE(mGpuConstants);
	}
	//-------------------------------------------------------------------------------------
	void Render::_CreateConstants(){
		Debug("Render::_CreateConstants()");
		// Setup shadow GPU parameters before we load the resource groups so materials can use the parameters
		mGpuConstants = new CSMGpuConstants(4);
	}
	//-------------------------------------------------------------------------------------
	void Render::_InitPostEffects()	{
		/// Create a couple of hard coded postfilter effects as an example of how to do it but the preferred method is to use compositor scripts.
		_CreatePostEffects();
		_EnablePostEffects();
	}
	//-------------------------------------------------------------------------------------
	void Render::PostEffectSetStatus(std::string _name, bool _status)	{
#ifdef __EXPEREMENTAL__
		if (_name == "SSAO"){
			mSystem->setSSAO(_status);
			Debug("[PostEffects]Enabled SSAO");
		}
		else
		#endif
		if (_name == "HDR")
		{
			if (_status)
				CompositorManager::getSingleton().addCompositor(mViewport, _name,0);
			else
				CompositorManager::getSingleton().removeCompositor(mViewport, _name);
			CompositorManager::getSingleton().setCompositorEnabled(mViewport, _name, _status);
		}
		else{
			if (_status)
				CompositorManager::getSingleton().addCompositor(mViewport, _name);
			else
				CompositorManager::getSingleton().removeCompositor(mViewport, _name);
			CompositorManager::getSingleton().setCompositorEnabled(mViewport, _name, _status);
		}
	}
	//-------------------------------------------------------------------------------------
	void Render::_EnablePostEffects()	{
		int r = 0;
		r = mEngineConfig->SSAO;
		if (r)
			PostEffectSetStatus("SSAO", r);
		EnableDisableAA(true);
		r = mEngineConfig->HDR;
		if (r)
			PostEffectSetStatus("HDR", r);
	}
	//-------------------------------------------------------------------------------------
	void Render::_CreatePostEffects()
	{
		Ogre::CompositorManager& compMgr = Ogre::CompositorManager::getSingleton();
		compMgr.registerCompositorLogic("GaussianBlur", new GaussianBlurLogic);
		compMgr.registerCompositorLogic("HDR", new HDRLogic);
		compMgr.registerCompositorLogic("HeatVision", new HeatVisionLogic);
	}
}