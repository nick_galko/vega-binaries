#pragma once

#ifdef _DLIB
#ifdef PLATFORM_EXPORT
#define PLEXPORT __declspec(dllexport)
#else
#define PLEXPORT _declspec(dllimport)
#endif
#else
#define PLEXPORT 
#endif