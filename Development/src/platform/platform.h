//=========== (C) Copyright 2009, Nick Galko. All rights reserved. ===========
/// Desc: Platform code
///
/// Author: Nick Galko
/// Modifications:
//====================================================================================
#ifndef __PLATFORM_H_
#define __PLATFORM_H_

// Initial platform/compiler-related stuff to set.
#define ENGINE_PLATFORM_WINDOWS 1
#define ENGINE_PLATFORM_LINUX	2
#define ENGINE_PLATFORM_APPLE   3

#define ENGINE_COMPILER_MSVC    1
#define ENGINE_COMPILER_GNUC    2
#define ENGINE_COMPILER_ICC     3


/* Finds the compiler type and version.
*/
#if defined( _MSC_VER )
#   define ENGINE_COMPILER ENGINE_COMPILER_MSVC
#   define ENGINE_COMP_VER _MSC_VER

#elif defined( __GNUC__ )
#   define ENGINE_COMPILER ENGINE_COMPILER_GNUC
#   define ENGINE_COMP_VER (((__GNUC__)*100)+__GNUC_MINOR__)

#else
#   pragma error "No known compiler. Abort! Abort!"

#endif

/* See if we can use __forceinline or if we need to use __inline instead */
#if ENGINE_COMPILER == ENGINE_COMPILER_MSVC 
#       define FORCEINLINE __forceinline
#else
#   define FORCEINLINE __inline
#endif

/* Finds the current platform */

#if defined( __WIN32__ ) || defined( _WIN32 )
#   define ENGINE_PLATFORM ENGINE_PLATFORM_WINDOWS

#elif defined( __APPLE_CC__)
#   define ENGINE_PLATFORM ENGINE_PLATFORM_APPLE

#else
#   define ENGINE_PLATFORM ENGINE_PLATFORM_LINUX
#endif

// For generating compiler warnings - should work on any compiler
// As a side note, if you start your message with 'Warning: ', the MSVC
// IDE actually does catch a warning :)
#define ENGINE_QUOTE_INPLACE(x) # x
#define ENGINE_QUOTE(x) ENGINE_QUOTE_INPLACE(x)
#define ENGINE_WARN( x )  message( __FILE__ "(" QUOTE( __LINE__ ) ") : " x "\n" )

//----------------------------------------------------------------------------
// Windows Settings
#if ENGINE_PLATFORM == ENGINE_PLATFORM_WINDOWS
#include "PlatformWinDefines.h"
#endif
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
// Linux/Apple Settings
#if ENGINE_PLATFORM == ENGINE_PLATFORM_LINUX || ENGINE_PLATFORM == ENGINE_PLATFORM_APPLE


#endif

//For apple, we always have a custom config.h file
#if ENGINE_PLATFORM == ENGINE_PLATFORM_APPLE
#endif

//----------------------------------------------------------------------------

#endif
