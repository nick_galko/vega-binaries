#pragma once

namespace vega
{
	/**
	*/
	//Forward Declaration for Bullet
	class DynamicsWorld;
	struct BaseActorInterface;
	/**
	*/
	struct IBody{
		virtual ~IBody(){}
		virtual IBody* createPrimitiveSphereCollision(BaseActorInterface *_actor, DynamicsWorld *world) = 0;;
		virtual IBody* createConvex(BaseActorInterface *_actor, DynamicsWorld *world) = 0;;
		virtual IBody* createPrimitiveCapsuleCollision(BaseActorInterface *_actor, DynamicsWorld *world) = 0;;
		virtual IBody* createStaticGeometry(BaseActorInterface *_actor, DynamicsWorld *world) = 0;;
		virtual IBody* createPrimitiveBoxCollision(BaseActorInterface *_actor, DynamicsWorld *world) = 0;
		virtual IBody* createRagdoll(BaseActorInterface *_actor) = 0;
	};
}