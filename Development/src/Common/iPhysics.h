#pragma once



namespace vega
{
#if 1
	struct IBody;
	struct BaseActorInterface;
#endif
	class iBasePhysics
	{
	public:
		virtual bool initialize()=0;
		/**
		Updates PhysSystem engine
		*/
		virtual bool update(float time)=0;
		virtual void destroy()=0;
		virtual void cleanScene()=0;
		virtual ~iBasePhysics(){};
		virtual IBody* addObject(BaseActorInterface *_actor) = 0;
	public:
		class iPhysicsBaseBodys *iPhBody;
	};
};