#ifndef OGREINCS_H
#define OGREINCS_H

#include "Ogre.h"
#include "OgreErrorDialog.h"

#ifdef USE_BOOST
#ifndef BOOST_ALL_NO_LIB
#define BOOST_ALL_NO_LIB
#endif
#endif

//Define it,only for fast building Physics/Audio wrappers
#include "OIS.h"
#include "OISEvents.h"
#include "OISInputManager.h"
#include "OISKeyboard.h"
#include "OISMouse.h"

#include "Components\Terrain\include\OgreTerrain.h"
#include "Components\Terrain\include\OgreTerrainGroup.h"
#include "Components\Terrain\include\OgreTerrainMaterialGeneratorA.h"
#endif