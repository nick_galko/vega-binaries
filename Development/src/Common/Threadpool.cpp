#include "IEngine.h"
#include "ThreadPool.h"

namespace vega
{
	void Worker::operator()()
	{
#ifdef USE_BOOST 
		pool.service.run();
#else
		std::function<void()> task;
		while (true)
		{
			{   // acquire lock
				std::unique_lock<std::mutex>
					lock(pool.queue_mutex);

				// look for a work item
				while (!pool.stop && pool.tasks.empty())
				{ // if there are none wait for notification
					pool.condition.wait(lock);
				}

				if (pool.stop) // exit if the pool is stopped
					return;

				// get the task from the queue
				task = pool.tasks.front();
				pool.tasks.pop_front();

			}   // release lock

			// execute the task
			task();
		}
#endif
	}

	// the constructor just launches some amount of workers
	ThreadPool::ThreadPool(size_t threads)
		:
#ifdef USE_BOOST 
		working(service)
#else
		stop(false)
#endif
	{
#ifdef USE_BOOST  
			for (size_t i = 0; i < threads; ++i)
				workers.push_back(std::unique_ptr<boost::thread>(new boost::thread(Worker(*this))));
#else
			for (size_t i = 0; i < threads; ++i)
				workers.push_back(std::thread(Worker(*this)));
#endif
	}

	// the destructor joins all threads
	ThreadPool::~ThreadPool()
	{
#ifdef USE_BOOST 
		service.stop();
		for (size_t i = 0; i < workers.size(); ++i)
			workers[i]->join();
#else
		// stop all threads
		stop = true;
		condition.notify_all();

		// join them
		for (size_t i = 0; i < workers.size(); ++i)
			workers[i].join();
#endif
	}	
}