#pragma once

namespace vega
{
	struct SubSystemsManager
	{
		SubSystemsManager();
		virtual ~SubSystemsManager();
		//In public for plugins acess
		Ogre::SceneManager* mGSceneMgr;
		Ogre::Root *mGRoot;

		Ogre::Camera* mGCamera;				// Static Camera controller	
		Ogre::Viewport* mGViewport;			// Create one viewport, entire window
		Ogre::RenderWindow* mGWindow;
	};
};