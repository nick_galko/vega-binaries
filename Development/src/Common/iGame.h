#pragma once



namespace vega
{
	class iGame
	{
	public:
		virtual void			Initialize()=0;
		virtual void			CreateScene()=0;
		virtual void			Update(float _time)=0;
	};
};