#include "IEngine.h"

namespace vega
{
	SubSystemsManager::SubSystemsManager()
		:mGSceneMgr(nullptr)
		,mGRoot(nullptr)
		,mGCamera(nullptr)
		,mGWindow(nullptr)
		,mGViewport(nullptr)
	{}
	/**
	*/
	SubSystemsManager::~SubSystemsManager()
	{
	}
}