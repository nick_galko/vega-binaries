#ifndef _THREADPOOL_H
#define _THREADPOOL_H
#pragma once

namespace vega
{
	class ThreadPool;

	// our worker thread objects
	class Worker {
	public:
		Worker(ThreadPool &s) : pool(s) { }
		void operator()();
	private:
		ThreadPool &pool;
	};

	// the actual thread pool
	class ThreadPool {
	public:
		ThreadPool(size_t);
		// add new work item to the pool
		template<class F>
		void enqueue(F f)
		{
#ifdef USE_BOOST 
			service.post(f);
#else
			{ // acquire lock
				std::unique_lock<std::mutex> lock(queue_mutex);

				// add the task
				tasks.push_back(std::function<void()>(f));
			} // release lock

			// wake up one thread
			condition.notify_one();
#endif
		}
		~ThreadPool();
	private:
		friend class Worker;
#ifdef USE_BOOST 
		// need to keep track of threads so we can join them
		std::vector< std::unique_ptr<boost::thread> > workers;

		// the io_service we are wrapping
		boost::asio::io_service service;
		boost::asio::io_service::work working;
#else
		// need to keep track of threads so we can join them
		std::vector< std::thread > workers;

		// the task queue
		std::deque< std::function<void()> > tasks;

		// synchronization
		std::mutex queue_mutex;
		std::condition_variable condition;
		bool stop;
#endif
	};
}
#endif