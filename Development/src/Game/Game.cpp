#include "GamePrivate.h"
//API
#include "APIEngineCommands.h"
//Core
#include "CoreSystems.h"
//Render
#define RENDER_API __declspec(dllimport)
#include "Render.h"
//Engine
#include "EngineGlobals.h"
#include "world\Actor.h"
#include "ActorLight.h"
#include "ActorMesh.h"
#include "ActorSkyBox.h"
#include "ActorDynamicSky.h"
#include "ActorWater.h"
#include "EmitterSound.h"
#include "EmitterParticle.h"
#include "VideoPlayer.h"
#include "Scripting.h"
#include "SceneManager.h"

namespace vega
{
	Game::Game()
		:vplayer(nullptr),
		sound(nullptr)
	{}

	Game::~Game()
	{
		SAFE_DELETE(sound);
	}
	
	void Game::Initialize(){}
	
	void Game::CreateScene()	{		
		using namespace Ogre;
		vplayer = GetEngine()->render->GetPlayer();
		API::PlayVideo("logo.bik", false);

		ActorMesh*mesh = nullptr;
		//mesh = new ActorMesh("cast1.mesh", "cast1", "cast1");
		//mesh = new ActorMesh("pilon.mesh", "pilon", "pilon");

	/*	mesh = new ActorMesh("robot.mesh", "marine", "DefMat", ActorMesh::ACTORMESH_COLLISION_RAGGDOL, 10.0f, Ogre::Vector3(-55.0f, 500.0f, 0.0f),
			Ogre::Quaternion(1, 0, 0, 0), Vector3(1.0f, 1.0f, 1.0f), Vector3(0.0f, 0.0f, 0.0f));
*/
		
		// Floor plane
		mesh = new ActorMesh("floor.mesh", "floor", "StonehengeWall", ActorMesh::ACTORMESH_COLLISION_STATIC, 1000.0f, Ogre::Vector3(-55.0f, -25.0f, 0.0f),
			Ogre::Quaternion(1, 0, 0, 0), Vector3(35.0f, 35.0f, 35.0f), Vector3(0.0f, 0.0f, 0.0f));
		mesh->setCastShadows(false);
		////Backgorund Music
		sound = new EmitterSound("music", "stounh.wav", true);
		if (API::IsPlayingVideo())
			sound->pause();

		//GetEngine()->sceneManager->SetAmbientLight(0.2, 0.2, 0.2);
		API::RunScript("Engine_main.lua",false);
	}

	void Game::_enableStohMusic(){
	if (!vplayer->isPlaying())
		if (!sound->isPlaying())
			sound->play();
	}

	void Game::Update(float _time){
		_enableStohMusic();
	}
}