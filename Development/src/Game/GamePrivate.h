#pragma once
#ifndef GAMEPRIVATE_H
#define GAMEPRIVATE_H

#ifndef __ANDROID__
#define ENGINE_API __declspec(dllimport)
#ifdef GAME_EXPORTS
#define GAME_API __declspec(dllexport)
#else
#define GAME_API __declspec(dllimport)
#endif
#else
#define ENGINE_API 
#define GAME_API
#endif

// Engine Interfaces
#include "../Common/IEngine.h"

// Engine.dll
#include "../Engine/inc/EngineBase.h"

// Core.dll Log writting
#include "../Core/inc/log.h"
#include "../Core/inc/buildconfig.h"

//GameDll
#include "Game.h"
#include "GameApp.h"

#endif//GAMEPRIVATE_H