#pragma once

#ifdef __ANDROID__
#define CORE_API 
#else
#ifdef CORE_EXPORTS
#define CORE_API __declspec(dllexport)
#else
#define CORE_API __declspec(dllimport)
#endif
#endif

#define SAFE_DELETE(p)  { if (p) { delete (p); (p)=0; } } 

#define _STR(x) #x
#define STR(x) _STR(x)
#ifdef __ANDROID__
#define TODO(x) 
#else
#define TODO(x) __pragma(message("TODO: "_STR(x) " :: " __FILE__ "@"STR(__LINE__)))
#endif
#ifdef __ANDROID__
#define BUG(x) 
#else
#define BUG(x) __pragma(message("BUG: "_STR(x) " :: " __FILE__ "@"STR(__LINE__)))
#endif