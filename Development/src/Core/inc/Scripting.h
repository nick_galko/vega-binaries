#pragma once

struct lua_State;

namespace vega
{
	struct CORE_API Scripting
	{
	public:
		Scripting();

		~Scripting();

		::lua_State* getLua() const;
		bool RunScript(const char*_filename, bool debug);
	private:
		bool doString(const char * code) const;
		void bindLogFunctions();
		void bindMathFunctions();
		void bindCoreFunctions();
	private:
		::lua_State * luaState;
	};
}