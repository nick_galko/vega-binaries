#pragma once

namespace vega
{
	class CORE_API VFile
	{
	public:
		/*
		**/
		VFile(){};
		/*
		**/
		~VFile(){};
		/*
		**/
		const char* LoadFileHowCChar(const char *_mFilename);
		/*
		**/
		size_t LoadFileAndGetSize(const char *_mFilename);
		/*
		**/
		const char* GetHowCChar() const;			
		/*
		**/
		size_t GetSize();
	private:
		Ogre::DataStreamPtr pStream;
	};
}