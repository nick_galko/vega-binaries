/* Copyright (C) 2010-2014, Nick Galko. All rights reserved.
*
* File:    BuildConfig.h
* Desc:    Build Config defines
*/
#ifndef BUILDCONFIG_H
#define BUILDCONFIG_H

#ifndef USE_DLL_GAME
#define USE_DLL_GAME 1							//we will load game how dll,using LoadLibrary
#endif

#ifndef _DEVELOP
#define _DEVELOP	 1					       // DEVELOPER Build
#endif
//���������� ����,������������ ��� ������������ ������������
#ifndef _PRIVATEBUILD
//#define _PRIVATEBUILD 1
#endif

/*-------------------------------------------Features-------------------------------------------------*/
#ifndef USE_SKYX
#define USE_SKYX 1
#endif

#ifndef COMPONENT_USE_FFMPEG
#ifndef __ANDROID__
#define COMPONENT_USE_FFMPEG 1
#endif
#endif

#ifndef USE_STEAMWORKS
#ifndef __ANDROID__
#define USE_STEAMWORKS 1
#endif
#endif


#ifndef USE_D3D9_RENDER
#ifndef __ANDROID__
#define USE_D3D9_RENDER 1
#endif
#endif

#ifndef USE_D3D11_RENDER
#ifndef __ANDROID__
//#define USE_D3D11_RENDER 1
#endif
#endif

#ifndef USE_OGL_RENDER
#ifndef __ANDROID__
//#define USE_OGL_RENDER 1
#endif
#endif
/*-------------------------------------�������� ��������� ������-------------------------------------*/
#endif//BUILDCONFIG_H