#pragma once
#ifndef COREPRIVATE_H
#define COREPRIVATE_H

//-----------------------------------------------------------------------------
// Standard includes
//-----------------------------------------------------------------------------

// Engine Interfaces
#include "../../Common/IEngine.h"

#include "buildconfig.h"
#include "Core.h"
#include "DynLib.h"
#include "log.h"
#include "Shared.h"
#include "Config.h"
#include "FileSystem.h"
#include "EngineConfig.h"
#include "CoreSystems.h"
#include "VFile.h"
#include "VMathTranslations.h"
#include "Scripting.h"
#endif//Include Guard