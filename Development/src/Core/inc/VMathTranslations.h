#pragma once
#ifndef __V_MATH_TRANSLATIONS__H__
#define __V_MATH_TRANSLATIONS__H__

#include "OgreVector3.h"
#include "OgreColourValue.h"


__inline Ogre::ColourValue VectorToColour(const Ogre::Vector3& v)
{
	return Ogre::ColourValue(v.x, v.y, v.z);
}

__inline Ogre::Vector3 ColourToVector(const Ogre::ColourValue& c)
{
	return Ogre::Vector3(c.r, c.g, c.b);
}
#endif