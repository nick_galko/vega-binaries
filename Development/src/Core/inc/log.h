/* Copyright (C) 2010-2012, Nick Galko. All rights reserved.
*
* File:    Log.h
* Desc:    Log Functions
*/
#pragma once

#include "Core.h"


//�� ���� ���������� ��� � ��������� �����,�.� ����� ������������� ������ ������ ����� ������,� ����� �������� ������� 
//����� ���������� �� ������ ��������� ����
/// Report warning without terminating program (stops program until user responds).
CORE_API void Warning(const char *fmt, ...);

/// Report error and terminate program. Returns S_OK to shut up functions. Will never really return.
CORE_API void ErrorFunction(bool _toLog,const char *fmt, const char *_file, int _line, ...);
#ifndef Error
#ifndef __ANDROID
#define Error(x) ErrorFunction(true,x,__FILE__,__LINE__)
#else
#define Error(x) 
#endif
#endif

/// Serious warnings, always show MB
CORE_API void SeriousWarning(bool _show, const char *fmt, ...);
/// Report a message to the user for debug-only builds
CORE_API void Debug(const char *fmt, ...);
/// Writing in Log
CORE_API void LogPrintf(const char *fmt, ...);
/// LL_LOW = 1,LL_NORMAL = 2,LL_BOREME = 3
CORE_API void SetLoggingLevel(int _level);
///
CORE_API int GetLoggingLevel();