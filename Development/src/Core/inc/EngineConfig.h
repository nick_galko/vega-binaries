#pragma once

namespace vega
{
	struct EngineConfig
	{
		EngineConfig() :GlobalIllumination(false), SSAO(false), HDR(false), DOF(false),
			GodRays(false), PrecacheResources(false),
			ShadowFarDistance(100), ShadowTextureSize(100),
			FarClipDistance(100),
			mAntiAlliasing(AA_NONE), mRenderAPI(RENDER_NONE),
			mCompany("Vega Group"), mProjectName("DemoProject")
		{}

		//Render
		bool GlobalIllumination,SSAO,HDR,DOF,GodRays;

		float ShadowFarDistance, ShadowTextureSize,FarClipDistance;
		//Core
		bool PrecacheResources;
		std::string mCompany, mProjectName;

		enum AntiAlliasing
		{
			AA_NONE = 0,
			AA_SMAA = 1,
			AA_SSAA = 2,
			AA_FXAA = 3
		};
		AntiAlliasing mAntiAlliasing;

		enum RenderAPI{
			RENDER_NONE = -1,
			RENDER_OGL,
			RENDER_DX9,
			RENDER_DX11,
			RENDER_COUNT
		};
		RenderAPI mRenderAPI;
	};
}