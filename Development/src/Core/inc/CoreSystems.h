#pragma once


namespace vega
{
	class CollisionTools;
	class Config;
	struct EngineConfig;
	class FileSystem;
	class ScriptSerializerManager;
	class Scripting;
	class ThreadPool;

	class CORE_API CoreSystems:public SubSystemsManager
	{
	public:
		CoreSystems();
		~CoreSystems();
		
		//Sub-systems
		ThreadPool*mThreadpool;
		Config *mConfig;
		EngineConfig *mEngineConfig;
		FileSystem* mFS;
		CollisionTools* mRaycast;
		Scripting* mScript;
		enum EngineState{
			ES_LOADING,
			ES_PAUSE,
			ES_PLAY
		};
		EngineState mEngineState;
	protected:
		void WriteInfoAboutBuild();
		void Release();
	private:
		void Initialize();
		void GetDataFromSettingsFile();
	private:
		ScriptSerializerManager*mShaderCache;
	};
}