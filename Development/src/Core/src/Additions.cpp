#include "CorePrivate.h"

//#ifndef _DEVELOP
//#define USE_BREAKPAD 1
//#endif

#ifdef USE_BREAKPAD
#if ENGINE_PLATFORM == ENGINE_PLATFORM_WINDOWS
#include "..\..\..\external\breakpad\client\windows\handler\exception_handler.h"
#pragma comment(lib,"BREAKPAD_exception_handler.lib")
#pragma comment(lib,"BREAKPAD_common.lib")
#pragma comment(lib,"BREAKPAD_crash_generation_client.lib")
#endif
#endif


namespace vega
{
#ifdef USE_STEAMWORKS
	/**
	SteamWorks
	*/
	bool InitSteamWorks();
	void DestroySteamWorks();
#endif
	/**
	*/
#ifdef USE_BREAKPAD
	static bool dumpCallback(const wchar_t* dump_path,
		const wchar_t* minidump_id,
		void* context, EXCEPTION_POINTERS *exinfo, MDRawAssertionInfo *assertion,
		bool succeeded)
	{
		return succeeded;
	}
#endif

	/**
	*/
	bool InitAdditions()
	{
#ifdef USE_BREAKPAD
		google_breakpad::ExceptionHandler eh(L".", NULL, dumpCallback, NULL, true);
#endif

#ifdef USE_STEAMWORKS
		if(!InitSteamWorks())
			return false;
#endif
		return true;
	}

	/**
	*/
	void DestroyAdditions(){
#ifdef USE_STEAMWORKS
		DestroySteamWorks();
#endif
	}
}