#include "CorePrivate.h"

//����� ������ ����� � ���� ������
std::string ToStr(const char *fmt, ...)
{
	static char		msg[8000];

	va_list		argptr;
	va_start (argptr,fmt);
	vsprintf (msg,fmt,argptr);
	va_end (argptr);

	return msg;
}