#include "CorePrivate.h"

#if ENGINE_PLATFORM == ENGINE_PLATFORM_NACL

#include <sys/types.h>
#include <ppapi\cpp\file_system.h>
#include <ppapi\cpp\file_ref.h>
#include <ppapi\cpp\file_io.h>

namespace vega
{
    //---------------------------------------------------------------------
	void FileSystem::getConfigPaths()
	{
		// use application path as first config search path
		mConfigPaths.push_back("");
	}
    //---------------------------------------------------------------------
	void FileSystem::prepareUserHome(const std::string& subdir)
	{
	}
    //---------------------------------------------------------------------
	bool FileSystem::fileExists(const std::string& path) const
	{
        return false;
    }
}
#endif