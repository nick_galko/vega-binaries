#include "CorePrivate.h"
#include "Scripting.h"
#include "ThreadPool.h"
#include "Common.h"

namespace vega
{
	//-------------------------------------------------------------------------------------
	void ComputeBuildId();
	void DestroyAdditions();
	bool InitAdditions();
	//-------------------------------------------------------------------------------------
	CoreSystems::CoreSystems()
		: mThreadpool(nullptr)
		, mFS(nullptr)
		, mConfig(nullptr)
		, mEngineConfig(nullptr)
		, mShaderCache(nullptr)
		, mRaycast(nullptr)
		, mScript(nullptr)
		, mEngineState(ES_LOADING)
	{
		ComputeBuildId();
		BUG("Steam is not correct works")
#ifndef _DEVELOP
		//if (!InitAdditions())
		//	exit(0);
#else
		//InitAdditions();
#endif
		Initialize();
	}
	//-------------------------------------------------------------------------------------
	void CoreSystems::GetDataFromSettingsFile()
	{
		mEngineConfig->GlobalIllumination = mConfig->getValueAsBool("Render/R_GI", false);
		mEngineConfig->SSAO = mConfig->getValueAsBool("Render/R_SSAO", false);
		mEngineConfig->HDR = mConfig->getValueAsBool("Render/R_HDR", false);
		mEngineConfig->DOF = mConfig->getValueAsBool("Render/R_DOF", false);
		mEngineConfig->mAntiAlliasing = EngineConfig::AntiAlliasing(mConfig->getValueAsInt("Render/R_AA"));
		mEngineConfig->mRenderAPI = EngineConfig::RenderAPI(mConfig->getValueAsInt("Render/R_API"));
		mEngineConfig->GodRays = mConfig->getValueAsBool("Render/R_GodRays", false);

		mEngineConfig->PrecacheResources = mConfig->getValueAsBool("Resources/PreCache", false);

		mEngineConfig->ShadowFarDistance = mConfig->getValueAsFloat("Render/ShadowFarDistance", 150);
		mEngineConfig->ShadowTextureSize = mConfig->getValueAsFloat("Render/ShadowTextureSize", 512);
		mEngineConfig->FarClipDistance = mConfig->getValueAsFloat("Render/FarClipDistance", 512);
		mEngineConfig->mCompany = mConfig->getValueAsString("Licensing/CompanyName", "Vega Group");
		mEngineConfig->mProjectName = mConfig->getValueAsString("Licensing/ProjectName", "DemoProject");
	}
	//-------------------------------------------------------------------------------------
	void CoreSystems::Initialize()
	{
		// create a thread pool of 4 worker threads
		mThreadpool = new ThreadPool(4);
		mConfig = new Config();

		mEngineConfig = new EngineConfig();

		mScript = new Scripting();
		GetDataFromSettingsFile();

		mFS = new FileSystem(mEngineConfig);
		if (!mFS)
			return;
	}
	//-------------------------------------------------------------------------------------
	CoreSystems::~CoreSystems()	{
		Release();
	}
	//-------------------------------------------------------------------------------------
	void CoreSystems::Release(){
		LogPrintf("CoreSystems::Release");
		SAFE_DELETE(mFS);
		SAFE_DELETE(mScript);
		SAFE_DELETE(mEngineConfig);
		SAFE_DELETE(mConfig);
		SAFE_DELETE(mThreadpool);
		BUG("Steam is not correct works")
		//		DestroyAdditions();
	}
	//-------------------------------------------------------------------------------------
	void CoreSystems::WriteInfoAboutBuild(){
		LogPrintf("--------------------------------------------------------------");
		LogPrintf("'VCore' build: %d, %s", build_id, build_date);
		LogPrintf("--------------------------------------------------------------");
	}
}