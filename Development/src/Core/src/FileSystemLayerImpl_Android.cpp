#include "CorePrivate.h"

#if ENGINE_PLATFORM == ENGINE_PLATFORM_ANDROID

namespace vega
{
	void FileSystem::getConfigPaths()
	{
	}
    //---------------------------------------------------------------------
	void FileSystem::prepareUserHome(const std::string& subdir)
	{
	}
    //---------------------------------------------------------------------
	bool FileSystem::fileExists(const std::string& path) const
	{
		return access(path.c_str(), 00) == 0;
	}
    //---------------------------------------------------------------------
	bool FileSystem::createDirectory(const std::string& path)
	{
		return false;
	}
}
#endif