#pragma once

namespace vega
{
	// computing build id
	extern LPCSTR	build_date;
	extern int build_id;
}