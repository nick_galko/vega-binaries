#include "CorePrivate.h"
#include "OgreStaticPluginLoader.h"

#pragma comment(lib,"Plugin_OctreeSceneManager")
#pragma comment(lib,"Plugin_CgProgramManager")
#pragma comment(lib,"Plugin_ParticleFX")
// Static plugin headers
#  include "OgreCgPlugin.h"
#  include "OgreOctreePlugin.h"
#  include "OgreParticleFXPlugin.h"
#ifdef __ANDROID__
#ifdef OGRE_BUILD_RENDERSYSTEM_GLES2
#  include "OgreGLES2Plugin.h"
#endif

#elif _WIN32
#ifdef USE_D3D11_RENDER
#ifdef OGRE_BUILD_RENDERSYSTEM_D3D11
#  include "OgreD3D11Plugin.h"
#pragma comment(lib,"RenderSystem_Direct3D11")
#endif
#endif

#ifdef USE_D3D9_RENDER
#ifdef OGRE_BUILD_RENDERSYSTEM_D3D9
#  include "OgreD3D9Plugin.h"
#pragma comment(lib,"RenderSystem_Direct3D9")
#endif
#endif

#ifdef USE_OGL_RENDER
#ifdef OGRE_BUILD_RENDERSYSTEM_GL
#  include "OgreGLPlugin.h"
#pragma comment(lib,"RenderSystem_OpenGL")
#endif
#endif
#else
#ifdef OGRE_BUILD_RENDERSYSTEM_GL
#  include "OgreGLPlugin.h"
#pragma comment(lib,"RenderSystem_OpenGL")
#endif
#endif
namespace vega
{
	StaticPluginLoader::StaticPluginLoader(Ogre::Root*_mRoot)
		:mCgPlugin(nullptr),
		mOctreePlugin(nullptr),
		mParticleFXPlugin(nullptr),
		mOctreeZonePlugin(nullptr),
		mGLPlugin(nullptr),
		mGLES2Plugin(nullptr),
		mD3D9Plugin(nullptr),
		mD3D11Plugin(nullptr),
		mRoot(_mRoot) {}
	/** Load all the enabled plugins against the passed in root object. */
	void StaticPluginLoader::load()
	{
		using namespace Ogre;
#ifdef USE_OGL_RENDER
		mGLPlugin = new GLPlugin();
		mRoot->installPlugin(mGLPlugin);
#endif
#ifdef __ANDROID__
#ifdef OGRE_BUILD_RENDERSYSTEM_GLES2
		mGLES2Plugin = new GLES2Plugin();
		mRoot->installPlugin(mGLES2Plugin);
#endif
#endif


#if _WIN32
#ifdef USE_D3D11_RENDER
		mD3D11Plugin = new D3D11Plugin();
		mRoot->installPlugin(mD3D11Plugin);
#endif
#ifdef USE_D3D9_RENDER
		mD3D9Plugin = new D3D9Plugin();
		mRoot->installPlugin(mD3D9Plugin);
#endif
#endif
		mCgPlugin = new CgPlugin();
		mRoot->installPlugin(mCgPlugin);

		mOctreePlugin = new OctreePlugin();
		mRoot->installPlugin(mOctreePlugin);

		mParticleFXPlugin = new ParticleFXPlugin();
		mRoot->installPlugin(mParticleFXPlugin);
	}

	void StaticPluginLoader::unload()
	{
		// don't unload plugins, since Root will have done that. Destroy here.
		SAFE_DELETE(mParticleFXPlugin);
		SAFE_DELETE(mOctreePlugin);
		SAFE_DELETE(mCgPlugin);
#if _WIN32
#ifdef USE_D3D11_RENDER
		SAFE_DELETE(mD3D11Plugin);
#endif
#ifdef USE_D3D9_RENDER
		SAFE_DELETE(mD3D9Plugin);
#endif
#endif
#ifdef USE_OGL_RENDER
		SAFE_DELETE(mGLPlugin);
#endif

#ifdef __ANDROID__
#ifdef OGRE_BUILD_RENDERSYSTEM_GLES2
		SAFE_DELETE(mGLES2Plugin);
#endif
#endif

	}

}