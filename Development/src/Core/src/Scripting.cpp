#include "CorePrivate.h"
#include "lua\lua.hpp"
#include "Scripting.h"
#include "WrappedScriptFunctions.h"
#include "luabind\luabind.hpp"

namespace vega
{
	Scripting::Scripting() {
		this->luaState = luaL_newstate();
		luaL_openlibs(this->luaState);
		// Connect LuaBind to this lua state
		luabind::open(this->luaState);
		bindLogFunctions();
		bindMathFunctions();
		bindCoreFunctions();
	}

	Scripting::~Scripting() {
		lua_close(this->luaState);
	}

	lua_State * Scripting::getLua() const {
		return this->luaState;
	}

	bool Scripting::doString(const char * code) const {
		Debug("Loaded script: %s \n", code);
		luaL_loadstring(this->luaState, code);
		//crash,but faster 
		if (lua_pcall(luaState, 0, LUA_MULTRET, 0) != 0)
			Warning("[LUA]error running function: %s", lua_tostring(luaState, -1));
		int result = (int)lua_tointeger(luaState, -1);
		lua_pop(luaState, 1);

		return result;
	}

	TODO("����������� ������,����� ���� �� ����������")
		bool Scripting::RunScript(const char*_filename, bool debug){
			VFile file;
			return doString(file.LoadFileHowCChar(_filename));
	}

	void Scripting::bindLogFunctions() {
		lua_register(luaState, "Debug", ScriptDebug);
		lua_register(luaState, "LogPrintf", ScriptLogPrintf);
		lua_register(luaState, "Warning", ScriptWarning);
		lua_register(luaState, "SeriousWarning", ScriptSeriousWarning);
		using namespace luabind;
		module(luaState)
			[
				def("SetLoggingLevel", &SetLoggingLevel),
				def("GetLoggingLevel", &GetLoggingLevel)
			];
	}

	void Scripting::bindMathFunctions() {
		TODO("Vector2,Matrix");
		using namespace Ogre;
		using namespace luabind;
		module(luaState)
			[
				class_<Vector3>("Vector3")
				.def_readwrite("x", &Vector3::x)
				.def_readwrite("y", &Vector3::y)
				.def_readwrite("z", &Vector3::z)
				.def(constructor<>())
				.def(constructor<Vector3&>())
				.def(constructor<Real, Real, Real>())
				.def("absDotProduct", &Vector3::absDotProduct)
				.def("crossProduct", &Vector3::crossProduct)
				.def("directionEquals", &Vector3::directionEquals)
				.def("distance", &Vector3::distance)
				.def("dotProduct", &Vector3::dotProduct)
				.def("getRotationTo", &Vector3::getRotationTo)
				.def("isZeroLength", &Vector3::isZeroLength)
				.def("length", &Vector3::length)
				.def("makeCeil", &Vector3::makeCeil)
				.def("makeFloor", &Vector3::makeFloor)
				.def("midPoint", &Vector3::midPoint)
				.def("normalise", &Vector3::normalise)
				.def("nornaliseCopy", &Vector3::normalisedCopy)
				.def("perpendicular", &Vector3::perpendicular)
				.def("positionCloses", &Vector3::positionCloses)
				.def("positionEquals", &Vector3::positionEquals)
				.def("randomDeviant", &Vector3::randomDeviant)
				.def("reflect", &Vector3::reflect)
				.def("squaredDistance", &Vector3::squaredDistance)
				.def("squaredLength", &Vector3::squaredLength)
				//ColourValue
				, class_<ColourValue>("ColourValue")
				.def(constructor<>())
				.def(constructor<Real, Real, Real, Real>())
				.def(constructor<Real, Real, Real>())
				.def_readwrite("r", &ColourValue::r)
				.def_readwrite("g", &ColourValue::g)
				.def_readwrite("b", &ColourValue::b)
				.def_readwrite("a", &ColourValue::a)
				.def("saturate", &ColourValue::saturate)
			];
	}


	void Scripting::bindCoreFunctions() {
		using namespace luabind;
		using namespace vega;
		module(luaState)
			[
				//VFile
				class_ <VFile>("VFile")
				.def(luabind::constructor<>())
				.def("LoadFileHowCChar", (&VFile::LoadFileHowCChar))
				.def("LoadFileAndGetSize", (&VFile::LoadFileAndGetSize))
				.def("GetHowCChar", (&VFile::GetHowCChar))
				.def("GetSize", (&VFile::GetSize))
				,
				//EngineConfig
				class_<EngineConfig>("EngineConfig")
				.def(luabind::constructor<>())
				.def_readwrite("GlobalIllumination", &EngineConfig::GlobalIllumination)
				.def_readwrite("SSAO", &EngineConfig::SSAO)
				.def_readwrite("HDR", &EngineConfig::HDR)
				.def_readwrite("DOF", &EngineConfig::DOF)
				.def_readwrite("ShadowFarDistance", &EngineConfig::ShadowFarDistance)
				.def_readwrite("ShadowTextureSize", &EngineConfig::ShadowTextureSize)
				.def_readwrite("FarClipDistance", &EngineConfig::FarClipDistance)
				.def_readwrite("PrecacheResources", &EngineConfig::PrecacheResources)
				.def_readwrite("mAntiAlliasing", &EngineConfig::mAntiAlliasing)
				.def_readwrite("mRenderAPI", &EngineConfig::mRenderAPI)
				.enum_("AntiAlliasing")
				[
					value("AA_NONE", EngineConfig::AntiAlliasing::AA_NONE),
					value("AA_SMAA", EngineConfig::AntiAlliasing::AA_SMAA),
					value("AA_SSAA", EngineConfig::AntiAlliasing::AA_SSAA),
					value("AA_FXAA", EngineConfig::AntiAlliasing::AA_FXAA)
				]
				.enum_("RenderAPI")
					[
						value("RENDER_NONE", EngineConfig::RenderAPI::RENDER_NONE),
						value("RENDER_OGL", EngineConfig::RenderAPI::RENDER_OGL),
						value("RENDER_DX9", EngineConfig::RenderAPI::RENDER_DX9),
						value("RENDER_DX11", EngineConfig::RenderAPI::RENDER_DX11),
						value("RENDER_COUNT", EngineConfig::RenderAPI::RENDER_COUNT)
					]
			];
	}
}