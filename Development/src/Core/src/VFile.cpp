#include "CorePrivate.h"

using namespace Ogre;

namespace vega
{
	/*
	**/
	const char* VFile::LoadFileHowCChar(const char *_mFilename)
	{
		pStream = ResourceGroupManager::getSingletonPtr()->openResource( _mFilename, Ogre::ResourceGroupManager::AUTODETECT_RESOURCE_GROUP_NAME.c_str()); // open the file 
		if (!&pStream){
			Error(("[VFILE]Failed Open file %s", _mFilename));
			return "";
		}
		return (pStream->getAsString()).c_str();
	}

	/*
	**/
	size_t VFile::LoadFileAndGetSize(const char *_mFilename)
	{
		pStream = ResourceGroupManager::getSingletonPtr()->openResource(_mFilename, Ogre::ResourceGroupManager::AUTODETECT_RESOURCE_GROUP_NAME.c_str()); // open the file 
		if (!&pStream){
			Error(("[VFILE]Failed Open file %s", _mFilename));
			return 0;
		}
		return pStream->getAsString().size();
	}
	/*
	**/
	const char* VFile::GetHowCChar() const
	{
		if (!pStream.isNull())
			return (pStream->getAsString()).c_str();
		else
			return "";
	}
	/*
	**/
	size_t VFile::GetSize()
	{
		if (!pStream.isNull())
			return pStream->getAsString().size();
		else
			return 0;
	}
}
