#include "CorePrivate.h"


//-----------------------------------------------------------------------------
CORE_API void ErrorFunction(bool _toLog, const char *fmt, const char *_file, int _line, ...)
{
	char		msg[1024];

	va_list		argptr;
	va_start(argptr, fmt);
	vsprintf(msg, fmt, argptr);
	va_end(argptr);

	std::string str = ("%s\n IN:%s, LINE:%s", fmt, _file, (char*)(_line));
	if (_toLog)
		LogPrintf(str.c_str());
#if WIN32
	MessageBox(0, str.c_str(), "[ERROR]", 0);
#else
	Ogre::ErrorDialog* dlg = new Ogre::ErrorDialog(); 

	dlg->display(str.c_str()); 
	delete dlg; 
#endif
}