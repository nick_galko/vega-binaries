#ifndef __TQTTOOIS_H__
#define __TQTTOOIS_H__

int TranslateOISToQt(int _id)
{
	if (_id == OIS::MB_Left)
		return Qt::LeftButton;
	else if (_id == OIS::MB_Right)
		return Qt::RightButton;
	else if (_id == OIS::MB_Middle)
		return Qt::MiddleButton;
	else
		return _id;
}

int TranslateQtToOIS(int _id)
{
	if (_id == Qt::LeftButton)
		return OIS::MB_Left;
	else if (_id == Qt::RightButton)
		return OIS::MB_Right;
	else if (_id == Qt::MiddleButton)
		return OIS::MB_Middle;
	//Keys
	//W
	else if (_id == Qt::Key_W)
		return OIS::KC_W;
	//S
	else if (_id == Qt::Key_S)
		return OIS::KC_S;
	//A
	else if (_id == Qt::Key_A)
		return OIS::KC_A;
	//D
	else if (_id == Qt::Key_D)
		return OIS::KC_D;
	else
		return _id;
}
#endif