/********************************************************************************
** Form generated from reading UI file 'MainWindow.ui'
**
** Created by: Qt User Interface Compiler version 5.2.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDockWidget>
#include <QtWidgets/QFrame>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QToolBox>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *actionExit;
    QAction *actionAbout;
    QAction *actionNew;
    QAction *actionOpen;
    QAction *actionSave;
    QWidget *centralWidget;
    QHBoxLayout *horizontalLayout;
    QFrame *Render;
    QMenuBar *menuBar;
    QMenu *menuApplication;
    QDockWidget *dockWidget;
    QWidget *dockWidgetContents;
    QGridLayout *gridLayout;
    QToolBox *toolBox;
    QWidget *page;
    QWidget *page_2;
    QToolBar *toolBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(1038, 716);
        MainWindow->setStyleSheet(QLatin1String("*\n"
"{\n"
"	background-color: rgb(50, 50, 50);\n"
"}\n"
"\n"
"QMenuBar\n"
"{\n"
"	color: rgb(230, 230, 230);\n"
"	background-color:Qlineargradient(spread:pad, x1:0.5, y1:0, x2:0.5, y2:1, stop:0.00568182 rgba(120, 120, 120, 255), stop:1 rgba(50, 50, 50, 255));\n"
"}\n"
"\n"
" QToolBox::tab \n"
"{\n"
"	background-color:Qlineargradient(spread:pad, x1:0.5, y1:0, x2:0.5, y2:1, stop:0.00568182 rgba(120, 120, 120, 255), stop:1 rgba(50, 50, 50, 255));\n"
"    border-radius: 5px;\n"
"    color: darkgray;\n"
" }\n"
"\n"
" QToolBox::tab:hover\n"
"{\n"
"	background-color:Qlineargradient(spread:pad, x1:0.5, y1:0, x2:0.5, y2:1, stop:0.00568182 rgba(150, 150, 150, 255), stop:1 rgba(70, 70, 70, 255));\n"
" }\n"
"\n"
"QToolBox::tab:selected\n"
"{\n"
"	color: white;\n"
"	font: italic;\n"
"}\n"
"\n"
"QMenuBar::item \n"
"{\n"
"	 spacing: 3px; \n"
"	 padding: 4px 4px;\n"
"	 background: transparent;\n"
"	 border-radius: 2px;\n"
"}\n"
"\n"
"QMenuBar::item:pressed \n"
"{\n"
"	\n"
"	background-color:Qlineargradient(spread:pad, x1:0.5"
                        ", y1:0, x2:0.5, y2:1, stop:0.00568182 rgba(32, 32, 32, 255), stop:1 rgba(63, 63, 63, 255));\n"
"	color: #FFF;\n"
"}\n"
"\n"
"QMenu\n"
"{\n"
"	background: rgb(80, 80, 80);\n"
"}\n"
"\n"
"QMenu::item\n"
"{\n"
"	color: #FFF;\n"
"	padding: 2px 20px 2px 20px;\n"
"}\n"
"\n"
"QMenu::item:selected\n"
"{\n"
"	font-size: 1.7em;\n"
"}\n"
"\n"
"QMenu::item:selected\n"
"{\n"
"	color: #FFF;\n"
"	background-color:Qlineargradient(spread:pad, x1:0.5, y1:0, x2:0.5, y2:1, stop:0.00568182 rgba(64, 64, 64, 255), stop:1 rgba(32, 32, 32, 255));\n"
"}\n"
"\n"
"QToolButton \n"
"{\n"
"	background-color:Qlineargradient(spread:pad, x1:0.5, y1:1, x2:0.5, y2:0, stop:0 rgba(66, 66, 66, 255), stop:0.463277 rgba(101, 101, 101, 255), stop:0.5 rgba(135, 135, 135, 255), stop:0.977273 rgba(197, 197, 197, 255), stop:1 rgba(255, 255, 255, 255));\n"
"	border-radius: 3px;\n"
"	padding: 1px;\n"
"	margin: 5px;\n"
"	color: #CCC;\n"
"}\n"
"\n"
"QToolButton:hover\n"
"{\n"
"	background-color:Qlineargradient(spread:pad, x1:0.5, y1:1, x2:0.5, y2:0, stop:0 rg"
                        "ba(86, 86, 86, 255), stop:0.463277 rgba(121, 121, 121, 255), stop:0.5 rgba(155, 155, 155, 255), stop:0.977273 rgba(227, 227, 227, 255), stop:1 rgba(255, 255, 255, 255));\n"
"	color: #fff;\n"
"}\n"
"\n"
"QToolButton:pressed\n"
"{\n"
"	background-color:Qlineargradient(spread:pad, x1:0.5, y1:1, x2:0.5, y2:0, stop:0 rgba(26, 26, 26, 255), stop:0.463277 rgba(61, 61, 61, 255), stop:0.5 rgba(95, 95, 95, 255), stop:0.977273 rgba(157, 157, 157, 255), stop:1 rgba(255, 255, 255, 255));\n"
"	color: #333;\n"
"}\n"
"\n"
"QMainWindow::separator \n"
"{\n"
"    width: 2px;\n"
"    height: 2px;\n"
"}\n"
"\n"
"QMainWindow::separator:hover \n"
"{\n"
"    background: #DC143C;\n"
"}\n"
"\n"
"QToolBar\n"
"{\n"
"	background-color:Qlineargradient(spread:pad, x1:0.5, y1:1, x2:0.5, y2:0, stop:0 rgba(46, 46, 46, 255), stop:0.463277 rgba(81, 81, 81, 255), stop:0.5 rgba(115, 115, 115, 255), stop:0.977273 rgba(177, 177, 177, 255));\n"
"    spacing: 3px; /* spacing between items in the tool bar */\n"
"}\n"
"\n"
"QFrame ,QLabel, QToolTip \n"
""
                        "{\n"
"    border-radius: 4px;\n"
"    padding: 2px;\n"
"}\n"
"QDockWidget \n"
"{\n"
"	color: #FFF;\n"
"}\n"
"\n"
"QDockWidget::title\n"
"{\n"
"    text-align: left;\n"
"	background-color:Qlineargradient(spread:pad, x1:0.5, y1:0, x2:0.5, y2:1, stop:0.00568182 rgba(120, 120, 120, 255), stop:1 rgba(50, 50, 50, 255));\n"
"	padding: 5px;\n"
"    padding-left: 5px;\n"
"}\n"
"\n"
"QMdiArea\n"
"{\n"
"	border: 1px solid #dc143c;\n"
"}\n"
"\n"
"QProgressBar \n"
"{\n"
"    border: 2px solid grey;\n"
"    border-radius: 5px;\n"
"	text-align: center;\n"
"	color: #FFF;\n"
"	background-color:Qlineargradient(spread:pad, x1:0.5, y1:1, x2:0.5, y2:0, stop:0 rgba(47, 47, 47, 255), stop:0.514124 rgba(73, 73, 73, 255), stop:0.536723 rgba(100, 100, 100, 255), stop:1 rgba(100, 100, 100, 255));\n"
"}\n"
"\n"
"QProgressBar::chunk \n"
"{\n"
"	\n"
"	background-color:Qlineargradient(spread:pad, x1:0.5, y1:1, x2:0.5, y2:0, stop:0 rgba(47, 0, 0, 255), stop:0.514124 rgba(185, 16, 50, 255), stop:0.536723 rgba(220, 76, 105, 255), stop:1 rgba(2"
                        "20, 83, 111, 255));\n"
"    width: 5px;\n"
"	margin-right: 2px;\n"
"}\n"
"\n"
"QPlainTextEdit, QTextEdit\n"
"{\n"
"	background-color:Qlineargradient(spread:pad, x1:0.5, y1:0, x2:0.5, y2:1, stop:0 rgba(193, 193, 193, 255), stop:1 rgba(236, 236, 236, 255));\n"
"	border: 1px solid rgb(120, 120, 120);\n"
"	border-radius: 5px;\n"
"	color: rgb(79, 79, 79);\n"
"}\n"
"\n"
"QGraphicsView\n"
"{\n"
"	background-color: rgb(150, 150, 150);\n"
"}"));
        actionExit = new QAction(MainWindow);
        actionExit->setObjectName(QStringLiteral("actionExit"));
        actionAbout = new QAction(MainWindow);
        actionAbout->setObjectName(QStringLiteral("actionAbout"));
        actionNew = new QAction(MainWindow);
        actionNew->setObjectName(QStringLiteral("actionNew"));
        actionNew->setCheckable(true);
        QIcon icon;
        icon.addFile(QStringLiteral("../Editor/icons/assets/file.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionNew->setIcon(icon);
        actionOpen = new QAction(MainWindow);
        actionOpen->setObjectName(QStringLiteral("actionOpen"));
        actionOpen->setCheckable(true);
        QIcon icon1;
        icon1.addFile(QStringLiteral("../Editor/icons/menu/open.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionOpen->setIcon(icon1);
        actionSave = new QAction(MainWindow);
        actionSave->setObjectName(QStringLiteral("actionSave"));
        actionSave->setCheckable(true);
        QIcon icon2;
        icon2.addFile(QStringLiteral("../Editor/icons/menu/save.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionSave->setIcon(icon2);
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        horizontalLayout = new QHBoxLayout(centralWidget);
        horizontalLayout->setSpacing(6);
        horizontalLayout->setContentsMargins(11, 11, 11, 11);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        Render = new QFrame(centralWidget);
        Render->setObjectName(QStringLiteral("Render"));
        Render->setStyleSheet(QStringLiteral("background-color: rgb(0, 0, 0);"));
        Render->setFrameShape(QFrame::StyledPanel);
        Render->setFrameShadow(QFrame::Raised);

        horizontalLayout->addWidget(Render);

        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 1038, 24));
        menuApplication = new QMenu(menuBar);
        menuApplication->setObjectName(QStringLiteral("menuApplication"));
        MainWindow->setMenuBar(menuBar);
        dockWidget = new QDockWidget(MainWindow);
        dockWidget->setObjectName(QStringLiteral("dockWidget"));
        dockWidget->setMinimumSize(QSize(400, 172));
        dockWidgetContents = new QWidget();
        dockWidgetContents->setObjectName(QStringLiteral("dockWidgetContents"));
        gridLayout = new QGridLayout(dockWidgetContents);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        toolBox = new QToolBox(dockWidgetContents);
        toolBox->setObjectName(QStringLiteral("toolBox"));
        page = new QWidget();
        page->setObjectName(QStringLiteral("page"));
        page->setGeometry(QRect(0, 0, 374, 550));
        toolBox->addItem(page, QStringLiteral("Page 1"));
        page_2 = new QWidget();
        page_2->setObjectName(QStringLiteral("page_2"));
        page_2->setGeometry(QRect(0, 0, 374, 550));
        toolBox->addItem(page_2, QStringLiteral("Page 2"));

        gridLayout->addWidget(toolBox, 0, 0, 1, 1);

        dockWidget->setWidget(dockWidgetContents);
        MainWindow->addDockWidget(static_cast<Qt::DockWidgetArea>(2), dockWidget);
        toolBar = new QToolBar(MainWindow);
        toolBar->setObjectName(QStringLiteral("toolBar"));
        MainWindow->addToolBar(Qt::TopToolBarArea, toolBar);

        menuBar->addAction(menuApplication->menuAction());
        menuApplication->addAction(actionNew);
        menuApplication->addAction(actionOpen);
        menuApplication->addAction(actionSave);
        menuApplication->addSeparator();
        menuApplication->addAction(actionExit);
        toolBar->addAction(actionNew);
        toolBar->addAction(actionOpen);
        toolBar->addAction(actionSave);

        retranslateUi(MainWindow);
        QObject::connect(actionOpen, SIGNAL(triggered()), MainWindow, SLOT(show()));
        QObject::connect(actionNew, SIGNAL(triggered()), MainWindow, SLOT(show()));

        toolBox->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "VegaEd", 0));
        actionExit->setText(QApplication::translate("MainWindow", "Exit", 0));
        actionExit->setShortcut(QApplication::translate("MainWindow", "Ctrl+Q", 0));
        actionAbout->setText(QApplication::translate("MainWindow", "About", 0));
        actionNew->setText(QApplication::translate("MainWindow", "New", 0));
        actionOpen->setText(QApplication::translate("MainWindow", "Open", 0));
        actionOpen->setShortcut(QApplication::translate("MainWindow", "Ctrl+O", 0));
        actionSave->setText(QApplication::translate("MainWindow", "Save", 0));
        actionSave->setShortcut(QApplication::translate("MainWindow", "Ctrl+S", 0));
        menuApplication->setTitle(QApplication::translate("MainWindow", "File", 0));
        dockWidget->setWindowTitle(QApplication::translate("MainWindow", "Second Window", 0));
        toolBox->setItemText(toolBox->indexOf(page), QApplication::translate("MainWindow", "Page 1", 0));
        toolBox->setItemText(toolBox->indexOf(page_2), QApplication::translate("MainWindow", "Page 2", 0));
        toolBar->setWindowTitle(QApplication::translate("MainWindow", "toolBar", 0));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
