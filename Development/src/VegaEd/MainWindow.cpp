/*
 * ----------------------------------------------------------------------------
 * "THE BEER-WARE LICENSE" (Revision 42):
 * Ilija Boshkov <Mind Calamity> wrote this file. As long as you retain this notice you
 * can do whatever you want with this stuff. If we meet some day, and you think
 * this stuff is worth it, you can buy me a beer in return Poul-Henning Kamp
 * ----------------------------------------------------------------------------
 */

#include "MainWindow.h"
#include "ui_mainwindow.h"
#include "EnginePrivate.h"

using namespace vega;

bool yes = false;
MainWindow::MainWindow(QWidget *parent)
: QMainWindow(parent)
, ui(new Ui::MainWindow)
, isClosing(false)
, mRoot(0)
{
	ui->setupUi(this);
	ogre = new VegaEd::Widget(ui->Render);
	yes = true;
	EnableEngine();//Activate
}

MainWindow::~MainWindow()
{
	SAFE_DELETE(ogre);
	delete ui;
}

void MainWindow::paintEvent(QPaintEvent *e) { }

void MainWindow::on_actionExit_triggered() {
	close();
}


void MainWindow::on_actionSave_triggered()
{
	QString filename = QFileDialog::getSaveFileName(this, tr("Select file"));
}

void MainWindow::on_actionOpen_triggered()
{
	QString fileName = QFileDialog::getOpenFileName(this, tr("Select file"),
		tr("Vega World ( *.vworld "));

	if (fileName.isEmpty())
		return;

	GetEngine()->sceneManager->LoadLevel(fileName.toStdString().c_str(), true);
	//Nick	mUriList.append(fileNames);
}

void MainWindow::keyPressEvent(QKeyEvent *e)
{
	bool alt = e->modifiers().testFlag(Qt::AltModifier);
	if (e->key() == Qt::Key_F11 && alt)
	{
		if (this->isFullScreen())
			this->showNormal();
		else
			this->showFullScreen();
	}
}

void MainWindow::resizeEvent(QResizeEvent *e)
{
	std::cout << "Resize Event " << e->size().width() << ", " << e->size().height() << std::endl;
	std::cout << "Sending to ogre\n";
	ogre->resize(ui->Render->size());
}

void MainWindow::EnableEngine()
{
	std::cout << "Initializing Engine\n";
	GetEngine()->StartupOgre();
	mRoot = GetEngine()->mGRoot;
	if (mRoot->restoreConfig() || mRoot->showConfigDialog())
	{
		mRoot->initialise(false);
	}

	mTimer = new Ogre::Timer();
	mTimer->reset();

	ogre->go(mRoot);
	ogre->resize(ui->Render->size());
	ogre->repaint();
}

void MainWindow::renderOgre()
{
	if (this->isMinimized())
		return;

	if (mRoot)
	{
		ogre->updateOgre();
		ogre->repaint();
		mRoot->renderOneFrame();
	}
}

void MainWindow::closeEvent(QCloseEvent *e) {
	isClosing = true;
}
