#include "QtInputManager.h"
#include <QObject>
#include <QDebug>

QtInputManager QtInputManager::mInstance;

QtInputManager::QtInputManager()
{
}

void QtInputManager::AddKeyListener(QtKeyListener* listener)
{
    mKeyListeners.append(listener);
}

void QtInputManager::AddMouseListener(QtMouseListener* listener)
{
    mMouseListeners.append(listener);
}

void QtInputManager::RemoveKeyListener(QtKeyListener* listener)
{
    mRemovedKeyListeners.append(listener);
}

void QtInputManager::RemoveMouseListener(QtMouseListener* listener)
{
    mRemovedMouseListeners.append(listener);
}
void QtInputManager::keyPressEvent(QKeyEvent *event)
{
    foreach(QtKeyListener* l,mRemovedKeyListeners)
    {
        mKeyListeners.removeOne(l);
    }
    mRemovedKeyListeners.clear();
    foreach(QtKeyListener* l,mKeyListeners)
    {
        l->keyPressEvent(event);
    }
}

void QtInputManager::keyReleaseEvent(QKeyEvent *event)
{
    foreach(QtKeyListener* l,mRemovedKeyListeners)
    {
        mKeyListeners.removeOne(l);
    }
    mRemovedKeyListeners.clear();
    foreach(QtKeyListener* l,mKeyListeners)
    {
        l->keyReleaseEvent(event);
    }
}

void QtInputManager::mousePressEvent(QMouseEvent *event)
{
    foreach(QtMouseListener* l,mRemovedMouseListeners)
    {
        mMouseListeners.removeOne(l);
    }
    mRemovedMouseListeners.clear();
    foreach(QtMouseListener* l,mMouseListeners)
    {
        l->mousePressEvent(event);
    }
}

void QtInputManager::mouseReleaseEvent(QMouseEvent *event)
{
    foreach(QtMouseListener* l,mRemovedMouseListeners)
    {
        mMouseListeners.removeOne(l);
    }
    mRemovedMouseListeners.clear();
    foreach(QtMouseListener* l,mMouseListeners)
    {
        l->mouseReleaseEvent(event);
    }
}

void QtInputManager::mouseMoveEvent(QMouseEvent *event)
{
    foreach(QtMouseListener* l,mRemovedMouseListeners)
    {
        mMouseListeners.removeOne(l);
    }
    mRemovedMouseListeners.clear();
    foreach(QtMouseListener* l,mMouseListeners)
    {
        l->mouseMoveEvent(event);
    }
}

void QtInputManager::wheelEvent(QWheelEvent *event)
{
    foreach(QtMouseListener* l,mRemovedMouseListeners)
    {
        mMouseListeners.removeOne(l);
    }
    mRemovedMouseListeners.clear();
    foreach(QtMouseListener* l,mMouseListeners)
    {
        l->wheelEvent(event);
    }
}
