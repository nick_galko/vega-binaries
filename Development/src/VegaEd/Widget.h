/*
 * ----------------------------------------------------------------------------
 * "THE BEER-WARE LICENSE" (Revision 42):
 * Ilija Boshkov <Mind Calamity> wrote this file. As long as you retain this notice you
 * can do whatever you want with this stuff. If we meet some day, and you think
 * this stuff is worth it, you can buy me a beer in return Poul-Henning Kamp
 * ----------------------------------------------------------------------------
 */

#ifndef Widget_H
#define Widget_H

#include "IEngine.h"
#include <Prereqs.h>

namespace Ogre{
	class Root;
	class SceneManager;
	class Camera;
	class RenderWindow;
}

#define QTLIB	0
#define OISLIB	1

#define INPUTLIB QTLIB

namespace vega
{
	class iCameraBase;
}
namespace VegaEd
{
	class CameraMan;//Kill
	class OverlayWidget : public QWidget
	{
		Q_OBJECT

	public:
		OverlayWidget(QWidget *parent = 0) : QWidget(parent)
		{
			msgstr = QApplication::translate("Widget", "Initializing OGRE...");
			setContextMenuPolicy(Qt::PreventContextMenu);
		}

		virtual ~OverlayWidget() {}

		void setMessageString(QString msg)
		{
			if (msgstr != msg)
			{
				msgstr = msg;
				this->repaint();
			}
		}

	protected:
		QString msgstr;

		void paintEvent(QPaintEvent* evt);
	};

	class Widget : public QWidget
	{
		Q_OBJECT
	public:
		Widget(QWidget *parent);
		virtual ~Widget(void);
		virtual void	go(Ogre::Root* root, Ogre::SceneManager* mgr = NULL);
		QPaintEngine*	paintEngine() const; // Turn off QTs paint engine for the Ogre widget.
		Ogre::SceneManager* getSceneManager() { return mSceneMgr; }
		Ogre::String	mFPS;
		bool mSystemInitialized;
		void updateOgre(/*float timeSinceLastFrame*/);
	protected:
		bool setup();
		void chooseSceneManager(void);
		void createCamera(void);
		void createViewports(void);
		void setupResources(void);
		void loadResources(void);

		Ogre::Root*	    mRoot;
		Ogre::Camera*	    mCamera;
		Ogre::SceneManager* mSceneMgr;
		Ogre::RenderWindow* mWindow;
		Ogre::Real	    mTimeSinceLastFrame;
		vega::iCameraBase*   mCameraMan;
		bool mShutDown;
		bool mPassedSM;

	private:
		void _EditCameraInit();
		void _InjectMouseWheel(const QWheelEvent* evt);
		void paintEvent(QPaintEvent *e);
		void resizeEvent(QResizeEvent *e);
		void mouseMoveEvent(QMouseEvent *e);
		void mousePressEvent(QMouseEvent *e);
		void mouseReleaseEvent(QMouseEvent *e);
		void wheelEvent(QWheelEvent *e);
		void keyPressEvent(QKeyEvent *e);
		void keyReleaseEvent(QKeyEvent *e);
		Ogre::Vector2		    mAbsolute;
		Ogre::Vector2		    mRelative;
		VegaEd::OverlayWidget*	    mOverlayWidget;
	};
}
#endif // Widget_H

