/*
 * ----------------------------------------------------------------------------
 * "THE BEER-WARE LICENSE" (Revision 42):
 * Ilija Boshkov <Mind Calamity> wrote this file. As long as you retain this notice you
 * can do whatever you want with this stuff. If we meet some day, and you think
 * this stuff is worth it, you can buy me a beer in return Poul-Henning Kamp
 * ----------------------------------------------------------------------------
 */

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <Widget.h>

namespace Ui {
	class MainWindow;
}

namespace vega{
	class EngineBase;
}

class MainWindow : public QMainWindow
{
	Q_OBJECT

public:
	explicit MainWindow(QWidget *parent = 0);
	~MainWindow();
	bool isClosing;
	void renderOgre();
	VegaEd::Widget renderWidget() { return ogre; }

	private slots:
	void on_actionExit_triggered();

	void on_actionOpen_triggered();
	void on_actionSave_triggered();

	void EnableEngine();

private:
	Ui::MainWindow *ui;
	Ogre::Root*	    mRoot;
	VegaEd::Widget*  ogre;
	Ogre::Timer*    mTimer;

	Ogre::String	mResourcesCfg;
	Ogre::String	mPluginsCfg;

	void resizeEvent(QResizeEvent *e);
	void keyPressEvent(QKeyEvent *e);
	void paintEvent(QPaintEvent *e);
	void closeEvent(QCloseEvent *e);
};

#endif // MAINWINDOW_H
