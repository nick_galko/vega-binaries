#include "Widget.h"
#include "EnginePrivate.h"
#include "Updater.h"
#include "Camera/ICameraBase.h"
#include "CameraMan.h"
#include "EditorCamera.h"
#include "ActorDynamicSky.h"
#include "TranslateQtToOIS.h"
using namespace vega;

namespace VegaEd{
	//----------------------------------------------------------------------------------------
	void OverlayWidget::paintEvent(QPaintEvent* evt)
	{
		QPainter painter(this);
		painter.setClipRect(0, 0, width(), height());
		painter.setBrush(QBrush(QColor(0, 0, 0)));
		painter.fillRect(QRectF(0, 0, width(), height()), QColor(0, 0, 0));
		painter.setPen(QColor(210, 210, 210));
		painter.drawText(QRectF(0, 0, width(), height()), msgstr, QTextOption(Qt::AlignVCenter | Qt::AlignHCenter));
	}
	//----------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------
	Widget::Widget(QWidget *parent)
		: QWidget(parent),
		mRoot(0),
		mCamera(0),
		mSceneMgr(0),
		mWindow(0),
		mShutDown(false),
		mPassedSM(false),
		mSystemInitialized(false)
	{
		setAttribute(Qt::WA_OpaquePaintEvent);
		setAttribute(Qt::WA_PaintOnScreen);
		setMinimumSize(240, 240);
		resize(800, 600);
		setFocusPolicy(Qt::StrongFocus);
		setMouseTracking(true);
		mOverlayWidget = new OverlayWidget(this);
		QVBoxLayout *layout = new QVBoxLayout();
		layout->setMargin(0);
		layout->addWidget(mOverlayWidget);
		setLayout(layout);
	}

	//-------------------------------------------------------------------------------------
	Widget::~Widget(void)
	{}

	//-------------------------------------------------------------------------------------
	void Widget::chooseSceneManager(void)
	{
		mOverlayWidget->setMessageString("Creating Scene Manager");
		GetEngine()->ChooseSceneManager();
	}
	//-------------------------------------------------------------------------------------
	void Widget::createCamera(void)
	{
		mOverlayWidget->setMessageString("Creating Camera");
		GetEngine()->CreateCamera();
		mCamera = GetEngine()->mGCamera;
	}
	//-------------------------------------------------------------------------------------
	QPaintEngine* Widget::paintEngine() const
	{
		// We don't want another paint engine to get in the way for our Ogre based paint engine.
		// So we return nothing.
		return NULL;
	}
	//-------------------------------------------------------------------------------------
	void Widget::createViewports(void)
	{
		if (!mSystemInitialized)
			GetEngine()->CreateViewports();
	}
	//-------------------------------------------------------------------------------------
	void Widget::setupResources(void)
	{
		if (!mSystemInitialized)
			GetEngine()->SetupResources();
	}
	//-------------------------------------------------------------------------------------
	void Widget::paintEvent(QPaintEvent *e)
	{}
	//-------------------------------------------------------------------------------------
	void Widget::resizeEvent(QResizeEvent *e)
	{
		if (e->isAccepted())
		{
			const QSize &newSize = e->size();
			if (mRoot && mCamera)
			{
				mWindow->resize(newSize.width(), newSize.height());
				mWindow->windowMovedOrResized();

				Ogre::Real aspectRatio = Ogre::Real(newSize.width()) / Ogre::Real(newSize.height());
				mCamera->setAspectRatio(aspectRatio);
			}
		}
	}
	//-------------------------------------------------------------------------------------
	void Widget::mouseMoveEvent(QMouseEvent *e)
	{
		if (mSystemInitialized)
		{
			Ogre::Vector2 oldPos = mAbsolute;
			mAbsolute = Ogre::Vector2(e->pos().x(), e->pos().y());
			mRelative = mAbsolute - oldPos;
			//Nick			mCameraMan->InjectMouseMove(mRelative);
		}
	}

	//-------------------------------------------------------------------------------------
	void Widget::mousePressEvent(QMouseEvent *e)
	{
		if (mSystemInitialized)
			mCameraMan->InjectMouseDown(TranslateQtToOIS(e->button()));
	}

	//-------------------------------------------------------------------------------------
	void Widget::mouseReleaseEvent(QMouseEvent *e)
	{
		if (mSystemInitialized)
			mCameraMan->InjectMouseUp(TranslateQtToOIS(e->button()));
	}
	//-------------------------------------------------------------------------------------
	void Widget::wheelEvent(QWheelEvent *e)
	{
		if (mSystemInitialized)
			_InjectMouseWheel(e);
	}

	//-------------------------------------------------------------------------------------
	void Widget::keyPressEvent(QKeyEvent *e)
	{
		if (mSystemInitialized)
			mCameraMan->InjectKeyDown(TranslateQtToOIS(e->key()));
	}

	//-------------------------------------------------------------------------------------
	void Widget::keyReleaseEvent(QKeyEvent *e)
	{
		if (mSystemInitialized)
			mCameraMan->InjectKeyUp(TranslateQtToOIS(e->key()));
	}
	//-------------------------------------------------------------------------------------
	void Widget::updateOgre()
	{
		float timeSinceLastFrame = GetEngine()->updater->mUpdatedTime;
		//mCameraMan->frameRenderingQueued(timeSinceLastFrame);
		mTimeSinceLastFrame = timeSinceLastFrame;
		repaint();
		if (this->size() != this->parentWidget()->size())
			resize(this->parentWidget()->size());
	}


	//-------------------------------------------------------------------------------------
	void Widget::loadResources(void) {
		GetEngine()->LoadResources();
	}

	//-------------------------------------------------------------------------------------
	void Widget::go(Ogre::Root* root, Ogre::SceneManager* mgr)
	{
		if (!mSystemInitialized)
		{

			mRoot = root;
			if (mgr)
			{
				mSceneMgr = mgr;
				mPassedSM = true;
			}
			if (!setup())
				return;
		}
	}
	//-------------------------------------------------------------------------------------
	bool Widget::setup(void)
	{
		if (!mSystemInitialized)
		{
			EngineGlobals*engine = GetEngine();
			engine->SetupResources();

			// Create a new parameters list according to compiled OS
			size_t hand = (size_t)((HWND)this->winId());
			Ogre::String widgetHandle = Ogre::StringConverter::toString(hand);
			Ogre::NameValuePairList  viewConfig;
			viewConfig["externalWindowHandle"] = widgetHandle;
			// The line below utilizes the root's timer in order to generate a unique name for each widget instance
			mWindow = mRoot->createRenderWindow(Ogre::StringConverter::toString(mRoot->getTimer()->getMicroseconds()),
				width(), height(), false, &viewConfig);
			engine->mGWindow = mWindow;


			engine->ChooseSceneManager();
			createCamera();
			engine->CreateViewports();
			// Set default mipmap level (NB some APIs ignore this)
			Ogre::TextureManager::getSingleton().setDefaultNumMipmaps(5);
			/**
			Enable Systems
			*/
			engine->PreInitSystems();
			engine->StartupSystems();
			////NickAdded:
			//engine->CreateFrameListener();

			engine->window->Listener();
			//Crash 
			//engine->input->createFrameListener(hand);
			GetEngine()->mGRoot->addFrameListener(engine->updater);
		}
		_EditCameraInit();
		mSystemInitialized = true;
		mOverlayWidget->hide();
		return true;
	}
	//-------------------------------------------------------------------------------------
	void Widget::_EditCameraInit() {
		mCameraMan = GetEngine()->sceneManager->GetActiveCamera();
		if (!mCameraMan)
			mCameraMan = new CameraMan(GetEngine()->mGCamera);//EditorCamera(true);

		new ActorDynamicSky();
		ActorMesh*mesh = new ActorMesh("floor.mesh", "floor", "StonehengeWall", ActorMesh::ACTORMESH_COLLISION_STATIC, 1000.0f, Ogre::Vector3(-55.0f, -25.0f, 0.0f),
			Ogre::Quaternion(1, 0, 0, 0), Ogre::Vector3(35.0f, 35.0f, 35.0f), Ogre::Vector3(0.0f, 0.0f, 0.0f));
			mesh->setCastShadows(false);
			
	}
	//-------------------------------------------------------------------------------------
	void Widget::_InjectMouseWheel(const QWheelEvent* evt)	{
		mCameraMan->InjectMouseWheel(evt->delta());
	}
}