#ifndef QTINPUTMANAGER_H
#define QTINPUTMANAGER_H

#include "QtKeyListener.h"
#include "QtMouseListener.h"

class QtInputManager
{
public:
    static QtInputManager& getIntance(){return mInstance;}
    static QtInputManager* getIntancePtr(){return &mInstance;}

    void AddKeyListener(QtKeyListener* listener);
    void AddMouseListener(QtMouseListener* listener);
    void RemoveKeyListener(QtKeyListener* listener);
    void RemoveMouseListener(QtMouseListener* listener);

    void keyPressEvent(QKeyEvent *event);
    void keyReleaseEvent(QKeyEvent *event);

    void mousePressEvent(QMouseEvent *event);
    void mouseReleaseEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
    void wheelEvent(QWheelEvent *event);
private:
    QtInputManager();
    static QtInputManager mInstance;
    QList<QtKeyListener*> mKeyListeners;
    QList<QtMouseListener*> mMouseListeners;

    QList<QtKeyListener*> mRemovedKeyListeners;
    QList<QtMouseListener*> mRemovedMouseListeners;
};

#endif // QTINPUTMANAGER_H
