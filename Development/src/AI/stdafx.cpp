#include "AiPrivate.h"

#ifndef AI_EXPORTS
#define AI_EXPORTS
#endif

#ifndef AI_LIB
#define AI_LIB 1
#endif

#pragma comment(lib,"Common.lib")
#pragma comment(lib,"Core.lib")