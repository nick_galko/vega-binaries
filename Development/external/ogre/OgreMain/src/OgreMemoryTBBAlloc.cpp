#include "OgreStableHeaders.h"
#include "OgrePrerequisites.h"
#include "OgreMemoryTBBAlloc.h"
#include "OgrePlatformInformation.h"
#include "OgreMemoryTracker.h"

#if OGRE_MEMORY_ALLOCATOR == OGRE_MEMORY_ALLOCATOR_TBB

// Intel Threading building Blocks multithreaded memory allocator
#include <tbb/scalable_allocator.h>

namespace Ogre
{

	//---------------------------------------------------------------------
	void* TBBAllocImpl::allocBytes(size_t count, 
		const char* file, int line, const char* func)
	{
		void* ptr = scalable_malloc(count);
	#if OGRE_MEMORY_TRACKER
		// this alloc policy doesn't do pools (yet, ned can do it)
		MemoryTracker::get()._recordAlloc(ptr, count, 0, file, line, func);
	#endif
		return ptr;
	}
	//---------------------------------------------------------------------
	void TBBAllocImpl::deallocBytes(void* ptr)
	{
		// deal with null
		if (!ptr)
			return;
#if OGRE_MEMORY_TRACKER
		MemoryTracker::get()._recordDealloc(ptr);
#endif
		scalable_free(ptr);
	}
	//---------------------------------------------------------------------
	void* TBBAllocImpl::allocBytesAligned(size_t align, size_t count, 
		const char* file, int line, const char* func)
	{
		// default to platform SIMD alignment if none specified
		void* ptr =  align ? scalable_aligned_malloc(count, align)
			: scalable_aligned_malloc(count, OGRE_SIMD_ALIGNMENT);
#if OGRE_MEMORY_TRACKER
		// this alloc policy doesn't do pools (yet, ned can do it)
		MemoryTracker::get()._recordAlloc(ptr, count, 0, file, line, func);
#endif
		return ptr;
	}
	//---------------------------------------------------------------------
	void TBBAllocImpl::deallocBytesAligned(size_t align, void* ptr)
	{
		// deal with null
		if (!ptr)
			return;
#if OGRE_MEMORY_TRACKER
		// this alloc policy doesn't do pools (yet, ned can do it)
		MemoryTracker::get()._recordDealloc(ptr);
#endif
		scalable_aligned_free(ptr);
	}


};


#endif

