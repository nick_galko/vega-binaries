﻿/**
 * Copyright 1998-2011 Epic Games, Inc. All Rights Reserved.
 */

using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Resources;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("Launcher")]
[assembly: AssemblyDescription("For handling Vega Development Kit editor launching")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Vega Group, Ltd.")]
[assembly: AssemblyProduct("VegaEngine")]
[assembly: AssemblyCopyright("Copyright 2013 Vega Group, Ltd. All Rights Reserved.")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]
[assembly: ComVisible(false)]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
[assembly: AssemblyVersion("1.0.*")]
[assembly: NeutralResourcesLanguageAttribute( "" )]

