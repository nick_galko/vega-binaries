material Common/SphereMappedRustySteel
{
	technique
	{
		pass
		{
			texture_unit
			{
				texture RustySteel.jpg
			}

			texture_unit
			{
				texture spheremap.png
				colour_op_ex add src_texture src_current
				colour_op_multipass_fallback one one
				env_map spherical
			}
		}
	}
}

material Common/DynamicCubeMap
{
	technique
	{
		pass
		{
			texture_unit
			{
				// will be filled in at runtime
				cubic_texture dyncubemap combinedUVW
				tex_address_mode clamp
				env_map cubic_reflection
			}
		}
	}
}



material Common/Flare
{
	technique
	{
		pass
		{
			lighting off
			scene_blend add
			depth_write off
			diffuse vertexcolour

			texture_unit
			{
				texture flare.png
			}
		}
	}
}
material Common/Flare2
{
	technique
	{
		pass
		{
			lighting off
			scene_blend add
			depth_write off
			diffuse vertexcolour

			texture_unit
			{
				texture flaretrail.png
			}
		}
	}
}
material Common/Flare3
{
	technique
	{
		pass
		{
			lighting off
			scene_blend alpha_blend
			depth_write off
			diffuse vertexcolour

			texture_unit
			{
				texture flare_alpha.dds
			}
		}
	}
}
material Common/FlarePointSprite
{
	technique
	{
		pass
		{
			lighting off
			scene_blend add
			depth_write off
			diffuse vertexcolour

			point_sprites on
			point_size 2
			point_size_attenuation on

			texture_unit
			{
				texture flare.png
			}
		}
	}
}


// Test hardware morph animation (no normals)
material Common/HardwareMorphAnimation
{
	technique
	{
		pass
		{
			
			vertex_program_ref HardwareMorphAnimation
			{
				// all default
			}

			texture_unit
			{
				colour_op_ex source1 src_current src_current
			}
		
		}
	}
}

// Test hardware pose animation (no normals)
material Common/HardwarePoseAnimation
{
	technique
	{
		pass
		{
			
			vertex_program_ref HardwarePoseAnimation
			{
				// all default
			}
			texture_unit
			{
				tex_coord_set 0
				colour_op_ex source1 src_current src_current
			}
		
		}
	}
}


// Test hardware morph animation (with normals)
material Common/HardwareMorphAnimationWithNormals
{
	technique
	{
		pass
		{
			
			vertex_program_ref HardwareMorphAnimationWithNormals
			{
				// all default
			}

			texture_unit
			{
				colour_op_ex source1 src_current src_current
			}
		
		}
	}
}

// Test hardware pose animation (no normals)
material Common/HardwarePoseAnimationWithNormals
{
	technique
	{
		pass
		{
			
			vertex_program_ref HardwarePoseAnimationWithNormals
			{
				// all default
			}
			texture_unit
			{
				tex_coord_set 0
				colour_op_ex source1 src_current src_current
			}
		
		}
	}
}

fragment_program Common/ShowUV_pCg cg
{
	source Basic.cg
	entry_point showuv_p
	profiles ps_4_0 ps_2_0 arbfp1
}

fragment_program Common/ShowUV_pGLSLES glsles
{
	source ShowUV.glsles
	profiles glsles
}

fragment_program Common/ShowUV_p unified
{
	delegate Common/ShowUV_pGLSLES
	delegate Common/ShowUV_pCg
}

fragment_program Common/ShowUVdir3D_pCg cg
{
	source Basic.cg
	entry_point showuvdir3d_p
	profiles ps_4_0 ps_2_0 arbfp1
}

fragment_program Common/ShowUVdir3D_pGLSLES glsles
{
	source ShowUVdir3D.glsles
	profiles glsles
}

fragment_program Common/ShowUVdir3D_p unified
{
	delegate Common/ShowUVdir3D_pGLSLES
	delegate Common/ShowUVdir3D_pCg
}

vertex_program Common/ShowTangents_vCg cg
{
	source Basic.cg
	entry_point basicPassthroughTangent_v
	profiles vs_2_0 arbvp1
	default_params
	{
		param_named_auto worldViewProj worldviewproj_matrix
	}
}

vertex_program Common/ShowTangents_vGLSLES glsles
{
	source ShowTangents.glsles
	profiles glsles
	default_params
	{
		param_named_auto worldViewProj worldviewproj_matrix
	}
}

vertex_program Common/ShowTangents_v unified
{
	delegate Common/ShowTangents_vGLSLES
	delegate Common/ShowTangents_vCg
}

vertex_program Common/ShowNormals_vCg cg
{
	source Basic.cg
	entry_point basicPassthroughNormal_v
	profiles vs_2_0 arbvp1
	default_params
	{
		param_named_auto worldViewProj worldviewproj_matrix
	}
}

vertex_program Common/ShowNormals_vGLSLES glsles
{
	source ShowNormals.glsles
	profiles glsles
	default_params
	{
		param_named_auto worldViewProj worldviewproj_matrix
	}
}

vertex_program Common/ShowNormals_v unified
{
	delegate Common/ShowNormals_vGLSLES
	delegate Common/ShowNormals_vCg
}

material Common/ShowUV
{
	technique
	{
		pass
		{
			vertex_program_ref BasicVertexPrograms/AmbientOneTextureWithUV
			{
				param_named_auto worldViewProj worldviewproj_matrix
			}
			fragment_program_ref Common/ShowUV_p
			{
			}
		}
	}

	technique
	{
		pass
		{
			vertex_program_ref BasicVertexPrograms/AmbientOneTextureUnified
			{
			}
			fragment_program_ref Common/ShowUV_p
			{
			}
		}
	}
}
material Common/ShowTangents
{
	technique
	{
		pass
		{
			vertex_program_ref Common/ShowTangents_v
			{
			}
			fragment_program_ref Common/ShowUVdir3D_p
			{
			}
		}
	}
}
material Common/ShowNormals
{
	technique
	{
		pass
		{
			vertex_program_ref Common/ShowNormals_v
			{
			}
			fragment_program_ref Common/ShowUVdir3D_p
			{
			}
		}
	}
}


